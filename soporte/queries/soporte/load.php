<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
include('../../../include/coneccion.php');
include('../../../include/funciones.php');

$fechaToday = date('Y-m-d H:i:s');
$z = 1;
$id_logIn  = idUsuario();
$tipo_usuario = tipoUsuario();

if ($tipo_usuario < 3) {
    $query="SELECT id_soporte, id_articulo, id_redactor, asunto, contenido, tickects_date, status FROM soporte_tickects where status = 0 order by tickects_date DESC ";    
}else{
    $query="SELECT id_soporte, id_articulo, id_redactor, asunto, contenido, tickects_date, status FROM soporte_tickects Where status < 2  and id_redactor = '$id_logIn' order by tickects_date DESC ";    
}

$res=$conn->query($query);
if($res->num_rows > 0){
    while($fila=$res->fetch_array()){
        $id_soporte      = $fila['id_soporte'];
        $id_redactor     = $fila['id_redactor'];
        $id_articulo     = $fila['id_articulo'];
        if ($id_articulo != 0) {
            $ref   = refId($fila['id_articulo']);    
            $ref_text = "REF: ".$ref;
        }else{
            $ref_text = "";
        }
        
        $redactor        = usuarioId($fila['id_redactor']);
        $contenido       = $fila['contenido'];
        $asunto          = $fila['asunto'];
        $tickects_date   = $fila['tickects_date'];
        $status          = $fila['status'];
        $color = $status + 1;
       
            $completo = '<div class="panel panel-'.$status_ordenes_color[$color].' card-view panel-refresh" >
                            <div class="refresh-container2">
                                <div class="la-anim-1"></div>
                            </div>
                            <div class="panel-heading">
                                <div class="pull-left">';
                                if ($tipo_usuario == 1) {
                                    $completo .= '<h6 class="panel-title txt-dark inline-block"><span style="color:green">'.$redactor.' <i class="zmdi zmdi-star-circle"></i> '.$asunto.' - '.$ref_text.'</span>  <span style="color:blue;font-size:14px;"> <i class="zmdi zmdi-timer"></i> '.ConvFecha($tickects_date).' </span> </h6>';
                                }else{

                                    $completo .= '<h6 class="panel-title txt-dark inline-block"><span style="color:black">'.$asunto.' - '.$ref_text.' </span>  <span style="color:blue;font-size:14px;">'.ConvFecha($tickects_date).' </span> </h6>';
                                }
                                $completo .='
                                </div>
                                <div class="pull-right" >
                                <a class="pull-left inline-block mr-15"  data-toggle="collapse" href="#collapse_'.$id_soporte.'" aria-expanded="true" >
                                        <i class="zmdi zmdi-chevron-down" style="color:#333;"></i>
                                        <i class="zmdi zmdi-chevron-up" onclick="cargar('.$id_soporte.');" style="color:#333;"></i>
                                    </a>
                                    
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div  id="collapse_'.$id_soporte.'" class="panel-wrapper collapse">
                            </div>
                        </div>';
      
                    $script = '';

            $productos[$id_soporte]= $completo.$script;

        $z++;
    
    }
}

if(empty($productos)){
    echo "<tr><td colspan=7 class='text-center text-muted'>NO SE ENCONTRARON RESULTADOS..</td></tr> ";
}else{

    foreach($productos as $producto)
    {
        echo $producto;
    }
}
?>
