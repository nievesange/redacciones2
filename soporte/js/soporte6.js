function loadOrden(){
    //loaderIn();
    var id = 1;
    var url="soporte/queries/soporte/load.php";
    $.post(url,{id:id},
    function (response){
        $("#listarOrdenes").html(response);
    });
}
loadOrden();

function completarPago(id){
    var url="soporte/queries/soporte/pagar.php";
    $.post(url,{id:id},
    function (response){
        if(response == 1){
            toastr['success']('', 'PAGO REALIZADO');
            loadOrden();                    
            }else if(response == 3){
                toastr["warning"]("", "NO SE HA PODIDO REALIZAR EL PAGO");
            }else{
                toastr['error']('', 'NO SE HA PODIDO REALIZAR EL PAGO');
            }
    });
}

function cargar(id){
    loaderIn();
    var url="soporte/queries/soporte/load_datos.php";
    $.post(url,{id:id},
    function (response){
        $("#collapse_"+id).html(response);
        loaderOut();
    });
}


function preguntar() {
    var pregunta = tinymce.get("descripcion").getContent();
    var id  = $("#soporte_id").val();
    if (pregunta != "") {
        var url="soporte/queries/soporte/comentarios.php";
        $.post(url,{id:id,pregunta:pregunta},function(data){
            if(data == 1){
                toastr['success']('', 'RESPUESTA PUBLICADA');
                tinymce.get("descripcion").setContent('');
                setTimeout(function(){
                    loadOrden();
                    atrasRedactores();
                },500)
                                
            }else if(data == 3){
                toastr["warning"]("", "NO SE HA PODIDO PUBLICAR TU RESPUESTA");
                location.reload();
            }else{
                toastr['error']('', 'NO SE HA PODIDO GUARDAR');
                location.reload();
            }
        });
    }else{
        toastr['warning']('', 'DEBES INGRESAR UN CONTENIDO EN LA RESPUESTA');
        $("#pregunta").focus();
    }
    
}

function cerrar(id){
    swal({
        title: 'ESTA SEGURO?',
        text: 'ESTA SEGURO QUE DESEA CERRAR ESTE TICKECTS',
        type: 'success',
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'SI, CERRAR!',
        cancelButtonText: 'NO',
        closeOnConfirm: true,
        closeOnCancel: true
    },function(isConfirm){
        if (isConfirm) {
            var url="soporte/queries/soporte/cerrar.php";
            $.post(url,{id:id},
            function (response){
                loadOrden();
            });
        }
    });
}

function tickectPublico(){
    $('#modalSoporte2').modal('show');
}

function tickectPublicar() {
    var pregunta = tinymce.get("descripcion").getContent();
    var titulo = $("#titulo").val();
    var id  = 0;
    if (pregunta != "" && titulo != "") {
        var url="soporte/queries/soporte/publicar_tickects.php";
        $.post(url,{id:id,pregunta:pregunta,titulo:titulo},function(data){
            if(data == 1){
                toastr['success']('', 'TICKECTS ENVIADO A TODOS LOS REDACTORES');
                tinymce.get("descripcion").setContent('');
                $("#titulo").val("");
                setTimeout(function(){
                    loadOrden();
                    atrasRedactores();
                },500)
                                
            }else if(data == 3){
                toastr["warning"]("", "NO SE HA PODIDO GUARDAR");
                location.reload();
            }else{
                toastr['error']('', 'NO SE HA PODIDO GUARDAR');
                location.reload();
            }
        });
    }else{
        toastr['warning']('', 'DEBES INGRESAR UN CONTENIDO DE LA PREGUNTA CON SU TITULO');
    }
    
}

function buscarSoporte() {
    var redactor = $("#redactor").val();
    if (redactor === 0 || redactor == "0") {
        toastr["warning"]("", "DEBE SELECCIONAR ÚN REDACTOR PARA REALIZAR LA BÚSQUEDA");
    } else {
        var url="soporte/queries/soporte/load2.php";
        $.post(url,{redactor:redactor},
        function (response){
            $("#listarOrdenes").html(response);
        });
    }
}
function recargar(){
    loadOrden();
    $("#redactor").val("0");
}


$('#TooltipDemo').click(function(){
    atrasRedactores2();
});

function responder(id) {
   
    $(".panel_li").hide();
    $("#solo_mensaje").show();
    $(".bot").hide();
    $("#bot_mensaje").show();

    $("#soporte_id").val(id);
    $('#TooltipDemo').trigger('click');
    $("#TooltipDemo").show('slow');
}

function tickectModal(id) {
    $(".panel_li").hide();
    $("#solo_tick").show();
    $(".bot").hide();
    $("#bot_tick").show();

    $("#soporte_id").val(id);
    $('#TooltipDemo').trigger('click');
    $("#TooltipDemo").show('slow');
}

function atrasRedactores(){
    $('#TooltipDemo').trigger('click');
    $("#TooltipDemo").hide('slow');
    $("#titulo").val("");
    tinymce.get("descripcion").setContent('');

}

function atrasRedactores2(){
    $("#TooltipDemo").hide('slow');
    $("#titulo").val("");
    tinymce.get("descripcion").setContent('');
}

function enviarNota() {
    var pregunta = tinymce.get("descripcion").getContent();
    var url="redactores/queries/redactores/notas2.php";
    $.post(url,{pregunta:pregunta},
    function (response){
        if(response == 1){
                toastr['success']('', 'SE HA ENVIADO LA NOTA A TODOS LOS REDACTORES');
                setTimeout(function(){
                    atrasRedactores();
                },300)
                                
            }else{
                toastr['error']('', 'NO SE HA PODIDO ENVIAR LA NOTA');
                setTimeout(function(){
                    atrasRedactores()
                },300)
            }
    });
}

function nota() {
    $(".panel_li").hide();
    $("#solo_nota").show();
    $("#mensaje_nota").show();
    $(".bot").hide();
    $("#bot_nota").show();

    $('#TooltipDemo').trigger('click');
    $("#TooltipDemo").show('slow');
}