<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
include('../../../include/coneccion.php');
include('../../../include/funciones.php');

$fechaToday = date('Y-m-d H:i:s');
$z = 1;
$id_logIn  = idUsuario();
$tipo_usuario = tipoUsuario();

if ($tipo_usuario == 1) {
    $query="SELECT id_retiro, id_redactor, monto_retiro, fecha_retiro FROM redactor_retiro Where  status_retiro = 0 order by fecha_retiro DESC ";    
}else{
    $query="SELECT id_retiro, id_redactor ,monto_retiro, fecha_retiro FROM redactor_retiro Where status_retiro = 0 and id_redactor = '$id_logIn' order by fecha_retiro DESC ";    
}

$res=$conn->query($query);
if($res->num_rows > 0){
    while($fila=$res->fetch_array()){
        $id_retiro       = $fila['id_retiro'];
        $id_redactor     = $fila['id_redactor'];
        $redactor        = usuarioId($fila['id_redactor']);
        $monto_retiro    = $fila['monto_retiro'];
        $fecha_retiro    = $fila['fecha_retiro'];
       
            $completo = '<div class="panel panel-'.$status_ordenes_color[1].' card-view panel-refresh" >
                            <div class="refresh-container2">
                                <div class="la-anim-1"></div>
                            </div>
                            <div class="panel-heading">
                                <div class="pull-left">';
                                if ($tipo_usuario == 1) {
                                    $completo .= '<h6 class="panel-title txt-dark inline-block"><span style="color:green">'.$redactor.' - </span>  <span style="color:blue">'.ConvFecha($fecha_retiro).' - </span>  '.$monto_retiro.' $</h6>';
                                }else{

                                    $completo .= '<h6 class="panel-title txt-dark inline-block"><span style="color:green"> '.$monto_retiro.'  $ - </span>  <span style="color:blue">'.ConvFecha($fecha_retiro).' - </span> </h6>';
                                }
                                $completo .='
                                </div>
                                <div class="pull-right" >';
                                if ($tipo_usuario == 1) {
                                    $completo .= '<a href="javascript:void(0);" onclick="completarPago('.$id_retiro.');" class="btn btn-success">COMPLETAR</a>';
                                }else{
                                    $completo .= '<a href="javascript:void(0);" class="btn btn-warning">PENDIENTE</a>';
                                }
                                $completo .='
                                    
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div  id="collapse_'.$id_retiro.'" class="panel-wrapper collapse">
                                
                            </div>
                        </div>';
      
                    $script = '';

            $productos[$id_retiro]= $completo.$script;

        $z++;
    
    }
}

if(empty($productos)){
    echo "<tr><td colspan=7 class='text-center text-muted'>NO SE ENCONTRARON RESULTADOS..</td></tr> ";
}else{

    foreach($productos as $producto)
    {
        echo $producto;
    }
}
?>
