<style type="text/css">
    .app-inner-layout__wrapper{
        min-height:50vh !important;
    }
    body{
        overflow-y: hidden;
    }
</style>
<div class="app-main__outer">
    <div class="app-main__inner p-0" id="pantalla">
        <div class="app-inner-layout chat-layout">
            <div class="app-page-title" style="padding-bottom: : 20px; margin-bottom: 10px; padding-top: 30px; padding-bottom: 0px;">
                <div class="page-title-wrapper">
                    <div class="page-title-heading" style="padding-top: 10px; padding-bottom: 10px; padding-left: 10px;">
                        <div class="page-title-icon">
                            <i class="pe-7s-menu icon-gradient bg-ripe-malin">
                        </i>
                        </div>
                        <div>CHAT
                        <div class="page-title-subheading">CONTACTA A TODOS DIRECTAMENTE</div>   
                        </div>
                    </div>   
                </div>
            </div>       
            <div class="app-inner-layout__wrapper">
                <div class="app-inner-layout__content card" id="pantalla1" style="height: 500px;">
                    <div class="table-responsive">
                        <div class="app-inner-layout__top-pane" style="background-color: gainsboro;">
                            <div class="pane-left">
                                <div class="mobile-app-menu-btn">
                                    <button type="button" class="hamburger hamburger--elastic" id="closed_movil">
                                        <span class="hamburger-box">
                                            <span class="hamburger-inner"></span>
                                        </span>
                                    </button>
                                </div>
                                <div class="avatar-icon-wrapper mr-2 avatar-icon-xl">
                                    <div class="avatar-icon">
                                        <img width="42" id="img" class="card-img-top img-perfil" src="#" alt="" style="" >
                                    </div>
                                </div>
                                <h6 class="mb-0 text-nowrap" id="usuario_sel"></h6>
                                  <div class="mb-0 text-nowrap" id="id_Urep"></div>
                                    <div class="opacity-7" ></div>
                            </div>
                        </div>
                        <div class="chat-wrapper scroll-area-lg" id="vista" style="height: 300px;">
                            <div class="main-card mb-3 card" id="pant" style="height: auto;">
                                <div id="listarOrdenes" style="height: 300px;"></div>  
                            </div>            
                        </div>
                        <div class="app-inner-layout__bottom-pane d-block text-center">
                            <form method="POST" action="chat.php">
                                <div class="mb-0 position-relative row form-group">
                                    <div class="col-xl-9 col-md-9 col-sm-9" style="width: 70%">
                                        <input type="text" class="form-control" style="padding-right: 0px;" name="mensaje" id="mensaje">
                                    </div>
                                    <div class="col-xl-3 col-md-3 col-sm-3" style="width:28%;padding-left: 0px;">
                                        <button class="mb-2 mr-2 btn-icon btn-shadow btn-outline-2x btn btn-outline-info" type="button" name="enviar" onclick="guardarMensaje()" title="Enviar" style="width: 60px; height: 40px;" >
                                            <i class="fa fa-play" style="padding-left: 8px;"> </i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                 <div class="app-inner-layout__sidebar card" id="pantalla2" style="height: 500px;overflow-y: auto;">
                    <div class="app-inner-layout__sidebar-header">
                        <ul class="nav flex-column">
                            <li class="pt-4 pl-3 pr-3 pb-3 nav-item">
                                <div class="input-group">
                                     <input placeholder="Buscar..." type="text" class="form-control" id="buscar" name="buscar"> <a href="javascript:void(0)" onclick="limpiar();">  <span style="display: none;cursor: pointer;"  id="limpiar"><i class="pe-7s-close icon-gradient bg-ripe-malin" style="font-size: 35px;" ></i></span></a>
                                                         
                                </div>
                            </li>
                        </ul>
                    </div>    
                    <div class="col-lg-6" style="margin-top: 5px;" id="conec">
                        <ul class="nav flex-column" id="usuarios_conectados"></ul>
                        <div class="app-inner-layout__sidebar-footer pb-3">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src='chat/js/chat17.js?v=0.0026'></script>   
<script src="add/vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>

