<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
include('../../../include/coneccion.php');
include('../../../include/funciones.php');

$fechaToday = date('Y-m-d H:i:s');
$fecha_date = date('Y-m-d');
$z = 1;

$query="SELECT id_redactor, monto_acumulado, monto_total, ultima_conexion, ultimo_pago FROM perfil_redactor where id_redactor > 2 order by ultima_conexion DESC";
$res=$conn->query($query);
if($res->num_rows>0){
    while($fila=$res->fetch_array()){
        $id_redactor      = $fila['id_redactor'];
        $status_redactor = statusUsuario($id_redactor);
        $tipo_usuario = idTipoUsuario($id_redactor);
        if ($tipo_usuario == 3) {
            $ultima_conexion  = $fila['ultima_conexion'];
            $last_page      = $fila['ultimo_pago'];
            if (!$last_page or $last_page == null or $last_page == "") {
                $last_page = "2019-05-01 00:00:00";
            }
            $redactor         = usuarioId($id_redactor);
            $correo2          = correoId($id_redactor);
            $pago             = $fila['monto_total'];
            $textos_entrega   = 0;
            $nivel = nivel($id_redactor);

            $query4 = "SELECT articulo_palabras from articulos where (articulo_status = 1 or articulo_status = 2) and articulo_redactor = '$id_redactor' and articulo_entrega > '$last_page'";
            $res4 = $conn->query($query4);
            $cant_palabras = 0;
            if($res4->num_rows > 0){
                while($fila4=$res4->fetch_array()){
                    $cant_palabras = $cant_palabras + $fila4['articulo_palabras'];
                }
                $suma = ($cant_palabras/100)*$pago;
                $suma = number_format($suma, 2, '.', '');
            }else{
                $suma = 0;
            }

            if ($correo == "") {
               $correo = $correo2; 
            }

            if ($status_redactor == 0) {
                $status = "<span onclick='changeStatusAdmin(1,".$id_redactor.")'>INACTIVO</span>";
                $color  = 'warning';
            }else{
                $status = "<span onclick='changeStatusAdmin(0,".$id_redactor.")'>ACTIVO</span>";
                $color  = 'success';
            }
            $query2="SELECT articulo_ref, articulo_palabras FROM articulos where articulo_redactor = '$id_redactor' and ( articulo_status = 0 or articulo_status = 3 ) order by articulo_date desc";
            $res2=$conn->query($query2);
            $acumule = 0;
            $ref = 1;
            $dato = "";
                
            if($res2->num_rows>0){
                
                while($fila2=$res2->fetch_array()){
                    $refer  = $fila2['articulo_ref'];
                    $palabras  = $fila2['articulo_palabras'];
                    $acumule = $acumule + $palabras;
                    $dato .= '<b style="color:red">'.$refer."</b>: <b style='color:green'>".$palabras." PALABRAS</b><br>";
                }
            }else{
                $ref = 0;
            }

            $query3="SELECT cod,tel,horas,img,style FROM perfil_usuario where id_usuario = '$id_redactor' limit 1";
            $res3=$conn->query($query3);
            $info = "";
            $img = "avatar.jpg";
            if($res3->num_rows>0){
                while($fila3=$res3->fetch_array()){
                    $cod  = $fila3['cod'];
                    $tel  = $fila3['tel'];
                    $img  = $fila3['img'];
                    $style  = $fila3['style'];
                    
                    $horas  = $fila3['horas'];
                    if ($horas == "") {
                        $horas = "NO DEFINIDO";
                    }
                    if ($tel == "" or $cod=="") {
                        $tel == "NO DEFINIDO";
                    }
                    $dato .= '<b style="color:red">'.$refer."</b>: <b style='color:green'>".$palabras." PALABRAS</b><br>";
                    $info .= '<b>TELÉFONO: </b>('.$cod.') '.$tel.'<br>';
                    $info .= '<b>CORREO PAY: </b>'.$correo.'<br>';
                    $info .= '<b>PALABRAS/24H: </b>'.$horas.'<br>';
                }
            }else{
                $info .= '<b>TELÉFONO: </b>NO DEFINIDO<br>';
                $info .= '<b>CORREO PAY: </b>'.$correo.'<br>';
                $info .= '<b>PALABRAS/24H: </b>NO DEFINIDO<br>';
            }

            $fecha_conexion = explode(" ", $ultima_conexion);
            //$date = $res[0];
            if ($ultima_conexion == "") {
                $conexion = "EL REDACTOR NO SE HA CONECTADO";
            }else{
                if ($fecha_conexion[0] == $fecha_date) {
                    $conexion = "HOY";
                }else{
                    $conexion = ConvFecha($ultima_conexion);
                }
            }
            if ($ref === 0 or $ref === "") {
                $entrega = "<b style='color:red'>NO ESTÁ REDACTANDO</b>";
            }else{
                $entrega = $dato;
            }
                $redactando = CountSomething('articulos',"articulo_redactor = '".$id_redactor."' AND articulo_status = 0");
                $completo = '<div class="col-md-12 col-lg-6 col-xl-4" style="height: 640px;">
                            <div class="scroll-area-lg" style="height: 600px;">
                              <div class="card-shadow-primary card-border mb-3 card">
                                <div class="dropdown-menu-header">
                                    <div class="dropdown-menu-header-inner bg-dark">
                                        <div class="menu-header-content">
                                            <div class="avatar-icon-wrapper mb-3 avatar-icon-xl">
                                                <div class="avatar-icon"><img
                                                        src="images/'.$img.'"
                                                        alt="" style="'.$style.'"></div>
                                            </div>
                                            <div><h6 class="widget-heading">'.ucwords($redactor).'</h6><b class="menu-header-subtitle" style="    font-size: 11px;">ÚLTIMA VEZ: '.$conexion.'</b></div>
                                            <div class="menu-header-btn-pane pt-1">
                                                <button data-toggle="tooltip" data-placement="top" data-original-title="ESTADO DEL REDACTOR" type="button" class="btn btn-'.$color.' btn-xs">'.$status.'</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="p-3"><h6 class="text-muted text-uppercase font-size-md opacity-5 font-weight-normal">DETALLES</h6>
                                    <ul class="rm-list-borders list-group list-group-flush">
                                        <li class="list-group-item">
                                            <div class="widget-content p-0">
                                                <div class="widget-content-wrapper">
                                                    <div class="widget-content-left">
                                                        <div class="widget-heading">ACUMULADO DESDE</div>
                                                        <div class="widget-subheading" style="opacity:1;">'.lastPageId($id_redactor).'</div>
                                                    </div>
                                                    <div class="widget-content-right">
                                                        <div class="font-size-xlg text-muted">
                                                            <input type="hidden" id="sumax_'.$id_redactor.'" value="'.$suma.'">
                                                            <span>'.$suma.' $</span>
                                                            </small>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="list-group-item">
                                            <div class="widget-content p-0">
                                                <div class="widget-content-wrapper">
                                                    <div class="widget-content-left">
                                                        <div class="widget-heading">NIVEL DE REDACCIÓN:</div>
                                                    </div>
                                                    <div class="widget-content-right">
                                                        <div class="font-size-xlg text-muted">
                                                            <span>'.$nivel.'</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="list-group-item">
                                            <div class="widget-content p-0">
                                                <div class="widget-content-wrapper">
                                                    <div class="widget-content-left">
                                                        <div class="widget-heading">REDACTANDO</div>
                                                        <div class="widget-subheading">'.$entrega.'</div>
                                                    </div>
                                                    <div class="widget-content-right">
                                                        <div class="font-size-xlg text-muted">
                                                            <span>'.$acumule.'('.$redactando.')</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="list-group-item">
                                            <div class="widget-content p-0">
                                                <div class="widget-content-wrapper">
                                                    <div class="widget-content-left">
                                                        <div class="widget-heading">INFORMACIÓN</div>
                                                        <div class="widget-subheading">'.$info.'</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="text-center d-block card-footer" style="padding-right:0px; padding-left:0px;">
                                    <button onclick="mensaje('.$id_redactor.')"  data-placement="top" data-original-title="DEJAR UN MENSAJE" type="button" class="btn btn-primary btn-xs"><i class="icon text-success ion-android-mail"></i></button>
                                    <button onclick="nota('.$id_redactor.')"  data-placement="top" data-original-title="DEJAR UN MENSAJE" type="button" class="btn btn-warning btn-xs"><i class="icon text-success ion-android-desktop"></i></button>
                                </div>
                            </div>
                             </div>
                        </div>';
          
                $script = "";

                $productos[$id_redactor]= $completo.$script;
            $z++;
        }

    }
}

if(empty($productos))
{
    echo "<tr><td colspan=7 class='text-center text-muted'>NO SE ENCONTRARON RESULTADOS</td></tr>";
}else{

    foreach($productos as $producto)
    {
        echo $producto;
    }
}
?>
