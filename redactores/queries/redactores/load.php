<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
include('../../../include/coneccion.php');
include('../../../include/funciones.php');

$fechaToday = date('Y-m-d H:i:s');
$fecha_date = date('Y-m-d');
$z = 1;

$query="SELECT id_redactor, monto_total, ultima_conexion, ultimo_pago FROM perfil_redactor where id_redactor > 2 order by ultima_conexion DESC";
$res=$conn->query($query);
if($res->num_rows>0){
    while($fila=$res->fetch_array()){
        $id_redactor      = $fila['id_redactor'];
        $tipo_usuario = idTipoUsuario($id_redactor);
        if ($tipo_usuario == 3) {
            $ultima_conexion  = $fila['ultima_conexion'];
            $redactor         = usuarioId($id_redactor);
            $correo2           = correoId($id_redactor);
            $pago      = $fila['monto_total'];
            $textos_entrega   = 0;

            $last_page      = $fila['ultimo_pago'];
            if (!$last_page or $last_page == null or $last_page == "" ) {
                $last_page = "2019-05-01 00:00:00";
            }
            $nivel = nivel($id_redactor);
           $query4 = "SELECT articulo_palabras from articulos where (articulo_status = 1 or articulo_status = 2) and articulo_redactor = '$id_redactor' and articulo_entrega > '$last_page'";
            $res4 = $conn->query($query4);
            $cant_palabras = 0;
            if($res4->num_rows > 0){
                while($fila4=$res4->fetch_array()){
                    $cant_palabras = $cant_palabras + $fila4['articulo_palabras'];
                }
                $suma = ($cant_palabras/100)*$pago;
                $suma = number_format($suma, 2, '.', '');
            }else{
                $suma = 0;
            }

            if ($correo == "") {
               $correo = $correo2; 
            }

            $query2="SELECT articulo_ref, articulo_palabras FROM articulos where articulo_redactor = '$id_redactor' and ( articulo_status = 0 or articulo_status = 3 ) order by articulo_date desc";
            $res2=$conn->query($query2);
            if($res2->num_rows>0){
                $ref = 1;
                $dato = "";
                while($fila2=$res2->fetch_array()){
                    $refer  = $fila2['articulo_ref'];
                    $palabras  = $fila2['articulo_palabras'];
                    $dato .= '<b style="color:red">'.$refer."</b>: <b style='color:green'>".$palabras." PALABRAS</b><br>";
                }
            }else{
                $ref = 0;
            }
            $fecha_conexion = explode(" ", $ultima_conexion);
            //$date = $res[0];
            if ($ultima_conexion == "") {
                $conexion = "NO SE HA CONECTADO";
            }else{
                if ($fecha_conexion[0] == $fecha_date) {
                    $conexion = "HOY";
                }else{
                    $conexion = ConvFecha($ultima_conexion);
                }
            }
            if ($ref === 0 or $ref === "") {
                $entrega = "<b style='color:red'>NO ESTÁ REDACTANDO</b>";
            }else{
                $entrega = $dato;
            }
                $redactando = CountSomething('textos',"textos_autor = '".$id_redactor."' AND textos_status = 1");
                $completo = '<tr>
                                <td style="text-align: center;"><b >'.ucwords($redactor).'</b></td>
                                <td style="text-align: center;">'.$conexion.'</td>
                                <td style="text-align: center;">'.$entrega.'</td>
                                <td style="text-align: center;"><span id="suma_'.$id_redactor.'">'.$suma.'</span> $</td>
                                <td style="text-align: center;">'.$nivel.'</td>
                                <td class="text-center" style="text-align: center;width:1px; " >
                                    <div class="btn-toolbar">
                                        <div class="btn-group">
                                            <button onclick="mensaje('.$id_redactor.');" data-toggle="tooltip" data-placement="top" data-original-title="DEJAR UN MENSAJE" type="button" class="btn btn-warning btn-xs"><i class="icon text-success ion-android-mail"></i></button>
                                        </div>
                                    </div>
                                </td>
                            </tr>';
          
                $script = "";

                $productos[$id_redactor]= $completo.$script;
            $z++;
        }

    }
}

if(empty($productos))
{
    echo "<tr><td colspan=7 class='text-center text-muted'>NO SE ENCONTRARON RESULTADOS</td></tr>";
}else{

    foreach($productos as $producto)
    {
        echo $producto;
    }
}
?>
