function loadRedactores(){
    var id=1;
    var url="redactores/queries/redactores/load.php";
    $.post(url,{id:id},
    function (response){
        //json = eval('('+response+')');
        $("#redactores").html(response);
    });
    var url4="redactores/queries/redactores/load2.php";
    $.post(url4,{id:id},
    function (response){
        //json = eval('('+response+')');
        $("#listarOrdenes").html(response);
    });

}
loadRedactores();

function atras() {
    $('#modalPago').modal('hide');
    $('#modalSoporte').modal('hide');
    $("#suma").val("");
    $("#id_redactor").val("");
}

function pagar(){
	var id = $("#redactor_id").val();
	var suma = $("#sumax_"+id).val();
    var result = inputNumeros2(suma);
	if (suma != "" && result != 1) {
		var titulo = "PAGO POR "+suma+"$";
		preguntar(id,suma,titulo,1);
	}else{
        toastr['warning']('', 'DEBES INGRESAR UN MONTO A PAGAR CORRECTO EJ: 2.2');
        $("#suma").focus();
    }
}

function mostrar(type) {
    if (type==1) {
        $("#table").hide('slow');
        $("#listarOrdenes").show('slow');
    } else {
        $("#listarOrdenes").hide('slow');
        $("#table").show('slow');
    }
}

function enviarNota() {
    var id       = $("#redactor_id").val();
    var pregunta = tinymce.get("descripcion").getContent();
    var url="redactores/queries/redactores/notas.php";
    $.post(url,{id:id,pregunta:pregunta},
    function (response){
        if(response == 1){
                toastr['success']('', 'SE HA ENVIADO LA NOTA AL PANEL DE REDACTOR');
                $("#id_redactor").val("");
                $("#pregunta").val("");
                setTimeout(function(){
                    loadRedactores();
                    atrasRedactores()
                    var url="redactores/queries/redactores/cargar_nota.php";
                    $.post(url,{id:id},
                    function (response){
                        if (response == 1 || response == 'NO HAY NOTAS EN EL PANEL DE USUARIO') {
                            $("#ver_nota").hide('slow');
                        } else {
                            $("#ver_nota").html(response);
                        }
                    });
                },300)
                                
            }else{
                toastr['error']('', 'NO SE HA PODIDO ENVIAR LA NOTA');
                setTimeout(function(){
                    loadRedactores();
                    atrasRedactores()
                },300)
            }
    });
}

function enviarSoporte(){
	var id       = $("#redactor_id").val();
	var titulo   = $("#titulo").val();
	var pregunta = tinymce.get("descripcion").getContent();
    if (titulo != "" && pregunta != "") {
        preguntar(id,pregunta,titulo,2);    
    } else {
        toastr['warning']('', 'DEBES INGRESAR EL MENSAJE CON SU TITULO');
    }
	
}

function preguntar(id,suma,titulo,tipo) {
    if (tipo == 1) {
    	var pregunta = "Ha recibido un pago por un monto de "+suma+ "$";
    }else{
    	var pregunta = suma;
    }
    if (pregunta != "" || titulo != "" ) {
        var url="redactores/queries/redactores/soporte.php";
        $.post(url,{id:id,pregunta:pregunta,tipo:tipo,suma:suma,titulo:titulo},function(data){
            if(data == 1){
                toastr['success']('', 'SE HA ENVIADO UN TICKECT AL REDACTOR');
                $("#id_redactor").val("");
                $("#sumax_"+id).val("");
                $("#titulo").val("");
                $("#pregunta").val("");
                setTimeout(function(){
                    loadRedactores();
                    atrasRedactores()
                },300)
                                
            }else if(data == 3){
                toastr["warning"]("", "NO SE HA PODIDO ENVIAR EL TICKECT");
                setTimeout(function(){
                    loadRedactores();
                    atrasRedactores()
                },300)
            }else if(data == 4){
                toastr["warning"]("", "EL MONTO A PAGAR SUPERA EL ACUMULADO");
                setTimeout(function(){
                    loadRedactores();
                    atrasRedactores()
                },300)
            }else{
                toastr['error']('', 'NO SE HA PODIDO ENVIAR EL TICKECT');
                setTimeout(function(){
                    loadRedactores();
                    atrasRedactores()
                },300)
            }
        });
    }else{
        toastr['warning']('', 'DEBES LLENAR TODOS LOS CAMPOS');
        $("#pregunta").focus();
    }
    
}


function mensaje(id) {

    $(".panel_li").hide();
    $("#solo_mensaje").show();
    $("#mensaje_nota").show();
    asignaNombre(id);
    $(".bot").hide();
    $("#bot_mensaje").show();

    $("#redactor_id").val(id);
    $('#TooltipDemo').trigger('click');
    $("#TooltipDemo").show('slow');
    $(".card-footer").css('opacity','0.1');
}

function asignaNombre(id) {
    var url="redactores/queries/redactores/asigna_nombre.php";
    $.post(url,{id:id},
    function (response){
        $(".redactor").html(response);
    });
}

$('#TooltipDemo').click(function(){
    atrasRedactores2();
});

function hacerPago(id) {
    var suma = $('#sumax_'+id).val();
    $('#suma').html(suma);

    $(".panel_li").hide();
    $("#solo_pagar").show();
    asignaNombre(id);
    $(".bot").hide();
    $("#bot_pagar").show();

    $("#redactor_id").val(id);
    $('#TooltipDemo').trigger('click');
    $("#TooltipDemo").show('slow');
    $(".card-footer").css('opacity','0.1');
}

function nota(id) {
    $(".panel_li").hide();
    $("#solo_nota").show();
    $("#mensaje_nota").show();
    asignaNombre(id);
    $(".bot").hide();
    $("#bot_nota").show();
    var url="redactores/queries/redactores/cargar_nota.php";
    $.post(url,{id:id},
    function (response){
        $("#ver_nota").html(response);
    });

    $("#redactor_id").val(id);
    $('#TooltipDemo').trigger('click');
    $("#TooltipDemo").show('slow');
    $(".card-footer").css('opacity','0.1');
}

function atrasRedactores(){
    $('#TooltipDemo').trigger('click');
    $("#TooltipDemo").hide('slow');
    $("#titulo").val("");
    tinymce.get("descripcion").setContent('');
    $(".card-footer").css('opacity','1');

}

function atrasRedactores2(){
    $("#TooltipDemo").hide('slow');
    $("#titulo").val("");
    tinymce.get("descripcion").setContent('');
    $(".card-footer").css('opacity','1');
}

function eliminarNota(id) {
    var url="home/queries/home/eliminar_nota.php";
    $.post(url,{id:id},
    function (response){
        if(response == 1){
            toastr['success']('', 'SE HA ELIMINADO LA NOTA DEL PANEL DE REDACTOR');
            setTimeout(function(){
                loadRedactores();
                atrasRedactores();
                var url="redactores/queries/redactores/cargar_nota2.php";
                $.post(url,{id:id},
                function (response){
                    $("#text_nota").html(response);
                });
            },300)
                            
        }else{
            toastr['error']('', 'NO SE HA PODIDO ELIMINAR LA NOTA');
            setTimeout(function(){
                loadRedactores();
                atrasRedactores();
            },300)
        }
    });
}

function changeStatusAdmin(status,id) {
    var url="home/queries/opciones/status_admin.php";
    $.post(url,{status:status,id:id},
    function (response){
       loadRedactores();
    });
}