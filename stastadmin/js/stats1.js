function load_graf() {
	$( "#ingresos" ).html('');

    $.ajax({
        url:"stastadmin/queries/statsadmin/ingresos.php", // getchart.php
        dataType: 'JSON',
        type: 'POST',
        data: {get_values: true},
        success: function(response) {
           config = {
			      data: response,
			      xkey: 'fecha',
			      ykeys: ['resultado'],
			      labels: ['Resultado'],
			      fillOpacity: 0.6,
			      hideHover: 'auto',
			      behaveLikeLine: true,
			      resize: true,
			      pointFillColors:['#ffffff'],
			      pointStrokeColors: ['black'],
			      lineColors:['gray','red']
			};
			config.element = 'ingresos';
			Morris.Area(config);
        }
    });


    $.ajax({
        url:"stastadmin/queries/statsadmin/pagos.php", // getchart.php
        dataType: 'JSON',
        type: 'POST',
        data: {get_values: true},
        success: function(response) {
           config = {
			      data: response,
			      xkey: 'fecha',
			      ykeys: ['resultado'],
			      labels: ['Resultado'],
			      fillOpacity: 0.6,
			      hideHover: 'auto',
			      behaveLikeLine: true,
			      resize: true,
			      pointFillColors:['green'],
			      pointStrokeColors: ['green'],
			      lineColors:['green','red']
			};
			config.element = 'pagos';
			Morris.Area(config);
        }
    });

      $.ajax({
        url:"stastadmin/queries/statsadmin/pagos_list.php", // getchart.php
        dataType: 'JSON',
        type: 'POST',
        data: {get_values: true},
        success: function(response) {
           config = {
			      data: response,
			      xkey: 'fecha',
			      ykeys: ['resultado'],
			      labels: ['Resultado'],
			      fillOpacity: 0.6,
			      hideHover: 'auto',
			      behaveLikeLine: true,
			      resize: true,
			      pointFillColors:['green'],
			      pointStrokeColors: ['green'],
			      lineColors:['green','red']
			};
			config.element = 'pagos_list';
			Morris.Area(config);
        }
    });
				
}
load_graf();

loadResult();
function loadResult() {
    var id = 1;
    var url2="stastadmin/queries/statsadmin/load_result.php";
    $.post(url2,{id:id},
    function (response0){
    	colors = ["red", "green","blue"];
        vale = ["ban", "check","edit"];
        vale2 = ["danger", "success","primary"];
        json   = eval('('+response0+')');
        var progress = json.nivel*10;
        $("#acumulado").html(json.total+" $");
        $("#palabras_hoy").html(json.palabras);
        $("#palabras_ent").html(json.redactando);
        $("#pedidos_hoy").html(json.num);      

        $("#comment").html(json.comment);
        if (json.ref) {
            $("#value").html("REF: "+json.ref+" VALORACIÓN: "+json.numero);    
        } else {
            $("#value").html("NO HAS RECIBIDO VALORACIONES");    
        }
        

        $("#lunes").html('<span class="pull-left inline-block capitalize-font" style="margin-top:2%;color:'+colors[json.lunes]+'">ORTOGRAFÍA/GRAMÁTICA</span> <button class="border-0 btn-transition btn btn-outline-'+vale2[json.lunes]+'"> <i class="fa fa-'+vale[json.lunes]+'"></i></button>');
        $("#martes").html('<span class="pull-left inline-block capitalize-font" style="margin-top:2%;color:'+colors[json.martes]+'">TIPO DE REDACCIÓN</span> <button class="border-0 btn-transition btn btn-outline-'+vale2[json.martes]+'"> <i class="fa fa-'+vale[json.martes]+'"></i></button>');
        $("#miercoles").html('<span class="pull-left inline-block capitalize-font" style="margin-top:2%;color:'+colors[json.miercoles]+'">TEXTOS CON SENTIDO</span> <button class="border-0 btn-transition btn btn-outline-'+vale2[json.miercoles]+'"> <i class="fa fa-'+vale[json.miercoles]+'"></i></button>');
        $("#jueves").html('<span class="pull-left inline-block capitalize-font" style="margin-top:2%;color:'+colors[json.jueves]+'">REDACCIÓN OPTIMIZADA</span> <button class="border-0 btn-transition btn btn-outline-'+vale2[json.jueves]+'"> <i class="fa fa-'+vale[json.jueves]+'"></i></button>');
        $("#viernes").html('<span class="pull-left inline-block capitalize-font" style="margin-top:2%;color:'+colors[json.viernes]+'">ADAPTADO A LAS ESPECIFICACIONES</span> <button class="border-0 btn-transition btn btn-outline-'+vale2[json.viernes]+'"> <i class="fa fa-'+vale[json.viernes]+'"></i></button>');
        $("#sabado").html('<span class="pull-left inline-block capitalize-font" style="margin-top:2%;color:'+colors[json.sabado]+'">SEO OPTIMIADO</span> <button class="border-0 btn-transition btn btn-outline-'+vale2[json.sabado]+'"> <i class="fa fa-'+vale[json.sabado]+'"></i></button>');
        $("#domingo").html('<span class="pull-left inline-block capitalize-font" style="margin-top:2%;color:'+colors[json.domingo]+'">VARIACIÓN KEYWORDS</span> <button class="border-0 btn-transition btn btn-outline-'+vale2[json.domingo]+'"> <i class="fa fa-'+vale[json.domingo]+'"></i></button>');
        
    });


    var url2="stastadmin/queries/statsadmin/valoraciones.php";
    $.post(url2,{id:id},
    function (response0){
        $("#valoraciones").html(response0);
    });      
    
}

function result(id, ref) {
    var url2="stastadmin/queries/statsadmin/result.php";
    $.post(url2,{id:id},
    function (response0){
        colors = ["red", "green","blue"];
        vale = ["ban", "check","edit"];
        vale2 = ["danger", "success","primary"];
        json   = eval('('+response0+')');
       
        $("#comment").html(json.comment);
        $("#value").html("REF: "+ref+" VALORACIÓN: "+json.numero);

       $("#lunes").html('<span class="pull-left inline-block capitalize-font" style="margin-top:2%;color:'+colors[json.lunes]+'">ORTOGRAFÍA/GRAMÁTICA</span> <button class="border-0 btn-transition btn btn-outline-'+vale2[json.lunes]+'"> <i class="fa fa-'+vale[json.lunes]+'"></i></button>');
        $("#martes").html('<span class="pull-left inline-block capitalize-font" style="margin-top:2%;color:'+colors[json.martes]+'">TIPO DE REDACCIÓN</span> <button class="border-0 btn-transition btn btn-outline-'+vale2[json.martes]+'"> <i class="fa fa-'+vale[json.martes]+'"></i></button>');
        $("#miercoles").html('<span class="pull-left inline-block capitalize-font" style="margin-top:2%;color:'+colors[json.miercoles]+'">TEXTOS CON SENTIDO</span> <button class="border-0 btn-transition btn btn-outline-'+vale2[json.miercoles]+'"> <i class="fa fa-'+vale[json.miercoles]+'"></i></button>');
        $("#jueves").html('<span class="pull-left inline-block capitalize-font" style="margin-top:2%;color:'+colors[json.jueves]+'">REDACCIÓN OPTIMIZADA</span> <button class="border-0 btn-transition btn btn-outline-'+vale2[json.jueves]+'"> <i class="fa fa-'+vale[json.jueves]+'"></i></button>');
        $("#viernes").html('<span class="pull-left inline-block capitalize-font" style="margin-top:2%;color:'+colors[json.viernes]+'">ADAPTADO A LAS ESPECIFICACIONES</span> <button class="border-0 btn-transition btn btn-outline-'+vale2[json.viernes]+'"> <i class="fa fa-'+vale[json.viernes]+'"></i></button>');
        $("#sabado").html('<span class="pull-left inline-block capitalize-font" style="margin-top:2%;color:'+colors[json.sabado]+'">SEO OPTIMIADO</span> <button class="border-0 btn-transition btn btn-outline-'+vale2[json.sabado]+'"> <i class="fa fa-'+vale[json.sabado]+'"></i></button>');
        $("#domingo").html('<span class="pull-left inline-block capitalize-font" style="margin-top:2%;color:'+colors[json.domingo]+'">VARIACIÓN KEYWORDS</span> <button class="border-0 btn-transition btn btn-outline-'+vale2[json.domingo]+'"> <i class="fa fa-'+vale[json.domingo]+'"></i></button>');
        
    });
}