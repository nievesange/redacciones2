/*Dropzone Init*/
$(function(){
	"use strict";
	Dropzone.options.myAwesomeDropzone = {
		addRemoveLinks: true,
		dictDefaultMessage: "Suelta aquí tu archivo para subirlo2",
		dictResponseError: 'Server not Configured',
		acceptedFiles: ".png,.jpg,.jpeg,.gif,.bmp,.zip",
	};
});

