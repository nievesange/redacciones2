
<div class="app-main__outer" style="width: 100%;flex: unset;">
        <div class="app-page-title" style="padding-bottom: : 20px; margin-bottom: 10px; padding-top: 30px; padding-bottom: 0px;">
            <div class="page-title-wrapper">
                <div class="page-title-heading" style="padding-top: 10px; padding-bottom: 10px; padding-left: 10px;">
                    <div class="page-title-icon">
                        <i class="pe-7s-diamond icon-gradient bg-warm-flame">
                        </i>
                    </div>
                    <div>HISTÓRICO DE ASIGNACIONES
                        <div class="page-title-subheading">FILTROS DE BÚSQUEDA AVANZADO DE ASIGNACIONES</div>   
                    </div>
                </div>   
            </div>
        </div>
        <div class="app-page-title" style="padding-bottom: 10px">
            <div class="page-title-wrapper">
                <div class="col-lg-3 col-md-9 col-sm-12">
                    <label><b>BUSCAR POR:</b></label><br>
                    <button class="mb-2 mr-2 btn-icon btn-shadow btn-outline-2x btn btn-outline-info" type="button" onclick="mostrar(1)" title="BUSCAR POR REF.">
                        <i class="fa fa-pencil-square-o" style="padding-left: 10px;"> </i></button>
                    <button class="mb-2 mr-2 btn-icon btn-shadow btn-outline-2x btn btn-outline-info" type="button" onclick="mostrar(2)" title="BUSCAR POR FECHA">
                        <i class="fa fa-calendar" style="padding-left: 8px;"> </i></button>

                    <?php if (tipoUsuario() < 3 ){?>
                       <button class="mb-2 mr-2 btn-icon btn-shadow btn-outline-2x btn btn-outline-info" type="button" onclick="mostrar(3)" title="BUSCAR POR REDACTOR">
                        <i class="fa fa-user" style="padding-left: 8px;"> </i></button>
                    <?php } ?>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="col-lg-12 col-md-12 col-sm-12"  id="buscar_ref">
                        <div id="buscar_re" class="col-lg-3 col-md-6 col-sm-12">
                            <label><b>REFERENCIA</b></label>
                            <input type="text" name="buscar" id="buscar" class="form-control" placeholder="ESCRIBE AQUÍ">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12" style="margin-top: 28px;">
                            <button type="button" class="mb-2 mr-2 btn-icon btn-shadow btn-outline-2x btn btn-outline-info" name="buscar_ref2" id="buscar_ref2" onclick="buscador()"> <i class="fa fa-search" style="padding-left: 8px;"> </i>
                            </button>
                        </div>   
                     </div>
                    <div id="buscar_fecha" style="margin:2%; padding-top: 0px; padding-left: 0px; margin: 0px;  display: none;">
                        <div class="col-lg-3 col-md-6 col-sm-6" style="margin:2%; padding-top: 0px; padding-left: 0px; margin: 0px; padding-right: 5px;">
                            <label><b>DESDE</b></label>
                            <input type="date" name="date" id="date" class="form-control date">
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6" style="margin:2%; padding-top: 0px; padding-left: 0px; margin: 0px; padding-right: 5px;">
                            <label><b>HASTA</b></label>
                            <input type="date" name="dat" id="dat" class="form-control date">
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <button type="button" class="mb-2 mr-2 btn-icon btn-shadow btn-outline-2x btn btn-outline-info" name="buscar_fec" id="buscar_fec" onclick="buscador_fecha()" style="margin-top: 30px; height: 35px;"><i class="fa fa-search" style="padding-right: 0px;width: 20px;height: 20px; padding-top: 5px;"> </i></button>
                        </div> 
                    </div>  
                    <?php if (tipoUsuario() < 3 ) { ?>
                    <div class="form-group">
                        <div class="col-lg-3 col-md-6 col-sm-12" id="buscar_redactor" style="margin:2%; padding-top: 0px; padding-left: 0px; margin: 0px; display: none; padding-right:5px;">
                            <label><b>REDACTOR</b></label>
                            <select id="redacte" class="form-control ">
                                <option value="0">SELECCIONAR:</option>
                                    <?php     
                                    selectUsuario("(idtipousuario = 3) and activo = 1");
                                    ?>
                            </select>
                        </div>
                         <div class="col-lg-6 col-md-12 col-sm-12" id="buscar_redac_div" style="margin:2%; padding-top: 0px; padding-left: 0px; margin: 0px; display: none;">
                            <button type="button" class="mb-2 mr-2 btn-icon btn-shadow btn-outline-2x btn btn-outline-info" name="buscar_redac" id="buscar_redac" onclick="buscador_redactor()" style="margin-top: 30px; height: 35px;"><i class="fa fa-search" style="padding-right: 0px;width: 20px;height: 20px; padding-top: 5px;"> </i></button>
                        </div>
                    </div>
                    <?php } ?>
                    <br>
                </div>
            </div>
        </div>
        <div class="app-page-title" style="padding-top: 0px; padding-bottom: 0px; margin-bottom: 0px;">
            <div class="form-group">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <label><b>FILTRAR POR ESTADO</b></label><br>
                    <div class="col-lg-2 col-md-2 col-sm-12" style="padding-right: 5px; width: 100px; padding-left: 0px; ">
                        <button class="btn btn-focus" onclick="loadStatus(0)">ASIGNADAS</button>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-12" style="padding-right: 5px; width: 108px; padding-left: 0px; ">                           
                        <button class="btn btn-primary" onclick="loadStatus(1)">ENTREGADAS</button>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-12" style="padding-right: 0px; width: 103px; padding-left: 5px; ">
                        
                        <button class="btn btn-success" onclick="loadStatus(2)">APROBADAS</button>
                    </div>
                    <div class="col-lg-3 col-md-2 col-sm-12" style="padding-right: 0px; width: 90px; padding-left: 7px; ">                    
                        <button class="btn btn-warning" onclick="loadStatus(3)">RECHAZADAS</button>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-12" style="padding-right: 10px; padding-left: 10px; ">
                        <button class="btn btn-info" onclick="loadOrden()" style="margin-left: 20px;"><i class="fa fa-refresh"></i></button>
                    </div>
                </div>
            </div>
        </div>   
    <div class="tabs-animation">
        <div id="home" class="view_user">
            <div class="row view_home" id="">                                                       
                <div class="col-md-12">
                    <div class="main-card mb-3 card">
                        <div id="listarOrdenes" style="margin:2%;"></div>  
                    </div>            
                </div>
            </div>
        </div>
    </div>
</div>     <!-- </div>-->
<script type="text/javascript" src='history/js/history9.js'></script>  
<script src="add/vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>
<script src="add/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js">
</script>                                                                   
<script src="add/vendors/toastr/toastr.min.js"></script>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                