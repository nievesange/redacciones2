function loadOrden(){
    //loaderIn();
    var id = 1;
    var url="home/queries/home/load.php";
    $.post(url,{id:id},
    function (response){
        $("#listarOrdenes").html(response);
    });
    var url1="home/queries/home/load_rechazos.php";
    $.post(url1,{id:id},
    function (response0){
        $("#listarPalabrasAll").html(response0);
    }); 

    var url2="home/queries/home/valoraciones.php";
    $.post(url2,{id:id},
    function (response0){
        $("#valoraciones").html(response0);
    });      
    
    //loaderOut();
}
loadOrden();
loadResult();
function loadResult() {
    var id = 1;
    var url2="home/queries/home/load_result.php";
    $.post(url2,{id:id},
    function (response0){
        colors = ["red", "green","blue"];
        vale = ["ban", "check","edit"];
        vale2 = ["danger", "success","primary"];
        mensaje = ["MUY MAL", "MAL","MALO","NECESITAS MEJORAR","NECESITAS MEJORAR","NECESITAS MEJORAR","NECESITAS MEJORAR","MUY BIEN","MUY BIEN","EXCELENTE","MUCHA CALIDAD"];
        json   = eval('('+response0+')');
        var progress = json.nivel*10;
        $("#total_retiro").val(json.total);
        $("#acumulado").html(json.dinero_acumulado+" $");
        $("#sin_aprobar").html(json.dinero_sa+" $");
        $("#total_dinero").html(json.dinero+" $");
        $("#palabras_hoy").html(json.palabras);
         $("#sin_revisar").html(json.sin_revisar);

        $("#nivel").html(json.nivel);
        $("#pedidos_hoy").html(json.num);
        $("#text").html(mensaje[json.nivel]);
        $("#progress").html(progress+"%");
        $("#comment").html(json.comment);
        if (json.ref) {
            $("#value").html("REF: "+json.ref+" VALORACIÓN: "+json.numero);    
        } else {
            $("#value").html("NO HAS RECIBIDO VALORACIONES");    
        }
        

       $("#lunes").html('<span class="pull-left inline-block capitalize-font" style="margin-top:2%;color:'+colors[json.lunes]+'">ORTOGRAFÍA/GRAMÁTICA</span> <button class="border-0 btn-transition btn btn-outline-'+vale2[json.lunes]+'"> <i class="fa fa-'+vale[json.lunes]+'"></i></button>');
        $("#martes").html('<span class="pull-left inline-block capitalize-font" style="margin-top:2%;color:'+colors[json.martes]+'">TIPO DE REDACCIÓN</span> <button class="border-0 btn-transition btn btn-outline-'+vale2[json.martes]+'"> <i class="fa fa-'+vale[json.martes]+'"></i></button>');
        $("#miercoles").html('<span class="pull-left inline-block capitalize-font" style="margin-top:2%;color:'+colors[json.miercoles]+'">TEXTOS CON SENTIDO</span> <button class="border-0 btn-transition btn btn-outline-'+vale2[json.miercoles]+'"> <i class="fa fa-'+vale[json.miercoles]+'"></i></button>');
        $("#jueves").html('<span class="pull-left inline-block capitalize-font" style="margin-top:2%;color:'+colors[json.jueves]+'">REDACCIÓN OPTIMIZADA</span> <button class="border-0 btn-transition btn btn-outline-'+vale2[json.jueves]+'"> <i class="fa fa-'+vale[json.jueves]+'"></i></button>');
        $("#viernes").html('<span class="pull-left inline-block capitalize-font" style="margin-top:2%;color:'+colors[json.viernes]+'">ADAPTADO A LAS ESPECIFICACIONES</span> <button class="border-0 btn-transition btn btn-outline-'+vale2[json.viernes]+'"> <i class="fa fa-'+vale[json.viernes]+'"></i></button>');
        $("#sabado").html('<span class="pull-left inline-block capitalize-font" style="margin-top:2%;color:'+colors[json.sabado]+'">SEO OPTIMIADO</span> <button class="border-0 btn-transition btn btn-outline-'+vale2[json.sabado]+'"> <i class="fa fa-'+vale[json.sabado]+'"></i></button>');
        $("#domingo").html('<span class="pull-left inline-block capitalize-font" style="margin-top:2%;color:'+colors[json.domingo]+'">VARIACIÓN KEYWORDS</span> <button class="border-0 btn-transition btn btn-outline-'+vale2[json.domingo]+'"> <i class="fa fa-'+vale[json.domingo]+'"></i></button>');
        
    });
}

function result(id, ref) {
    var url2="home/queries/home/result.php";
    $.post(url2,{id:id},
    function (response0){
        colors = ["red", "green","blue"];
        vale = ["ban", "check","edit"];
        vale2 = ["danger", "success","primary"];
        json   = eval('('+response0+')');
       
        $("#comment").html(json.comment);
        $("#value").html("REF: "+ref+" VALORACIÓN: "+json.numero);

       $("#lunes").html('<span class="pull-left inline-block capitalize-font" style="margin-top:2%;color:'+colors[json.lunes]+'">ORTOGRAFÍA/GRAMÁTICA</span> <button class="border-0 btn-transition btn btn-outline-'+vale2[json.lunes]+'"> <i class="fa fa-'+vale[json.lunes]+'"></i></button>');
        $("#martes").html('<span class="pull-left inline-block capitalize-font" style="margin-top:2%;color:'+colors[json.martes]+'">TIPO DE REDACCIÓN</span> <button class="border-0 btn-transition btn btn-outline-'+vale2[json.martes]+'"> <i class="fa fa-'+vale[json.martes]+'"></i></button>');
        $("#miercoles").html('<span class="pull-left inline-block capitalize-font" style="margin-top:2%;color:'+colors[json.miercoles]+'">TEXTOS CON SENTIDO</span> <button class="border-0 btn-transition btn btn-outline-'+vale2[json.miercoles]+'"> <i class="fa fa-'+vale[json.miercoles]+'"></i></button>');
        $("#jueves").html('<span class="pull-left inline-block capitalize-font" style="margin-top:2%;color:'+colors[json.jueves]+'">REDACCIÓN OPTIMIZADA</span> <button class="border-0 btn-transition btn btn-outline-'+vale2[json.jueves]+'"> <i class="fa fa-'+vale[json.jueves]+'"></i></button>');
        $("#viernes").html('<span class="pull-left inline-block capitalize-font" style="margin-top:2%;color:'+colors[json.viernes]+'">ADAPTADO A LAS ESPECIFICACIONES</span> <button class="border-0 btn-transition btn btn-outline-'+vale2[json.viernes]+'"> <i class="fa fa-'+vale[json.viernes]+'"></i></button>');
        $("#sabado").html('<span class="pull-left inline-block capitalize-font" style="margin-top:2%;color:'+colors[json.sabado]+'">SEO OPTIMIADO</span> <button class="border-0 btn-transition btn btn-outline-'+vale2[json.sabado]+'"> <i class="fa fa-'+vale[json.sabado]+'"></i></button>');
        $("#domingo").html('<span class="pull-left inline-block capitalize-font" style="margin-top:2%;color:'+colors[json.domingo]+'">VARIACIÓN KEYWORDS</span> <button class="border-0 btn-transition btn btn-outline-'+vale2[json.domingo]+'"> <i class="fa fa-'+vale[json.domingo]+'"></i></button>');
        
    });
}

function valorColors(valor){
    if (valor >1.9) {
        var colors = 3;
    }else if (valor > 0.9 && valor < 2) {
        var colors = 2;
    }else if (valor > 0 && valor < 1) {
        var colors = 1;
    }else if(valor == 0){
        var colors = 0;
    }
    return colors; 
}

function rechazar(id) {
    swal({
            title: 'ESTA SEGURO?',
            text: 'QUE DESEA RECHAZAR ESTA ASIGNACIÓN',
            type: 'success',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'SI, RECHAZAR!',
            cancelButtonText: 'NO',
            closeOnConfirm: true,
            closeOnCancel: true
        },function(isConfirm){
            if (isConfirm) {
                $.post("home/queries/opciones/rechazar.php",{id:id},
                function(data){
                    if(data == 1){
                        toastr["success"]("", "SE HA RECHAZADO LA ASIGNACIÓN ");
                        setTimeout(function(){
                            loadOrden();
                        },300)
                    }else if(data == 3){
                        toastr["warning"]("", "EL TITULO YA SE ENCUNTRA PUBLICADO");
                    }else{
                        toastr["error"]("", "ERROR AL RECHAZAR");
                    }
                });
            }
        });
}

function cerrar() {
    var id = $("#articulo_id").val();
    var url2 = $("#url").val();
    if (url2 != "") {
        var url="home/queries/opciones/entregar.php";
        $.post(url,{id:id,url2:url2},function(data){
            if(data == 1){
                toastr['success']('', 'REDACCIÓN GUARDADA PARA APROBACIÓN');
                setTimeout(function(){
                    loadOrden()
                    loadResult();
                    atrasRedactores();
                },200)
                                
            }else if(data == 3){
                toastr["warning"]("", "NO SE HA PODIDO GUARDAR");
                //location.reload();
            }else{
                toastr['error']('', 'NO SE HA PODIDO GUARDAR');
            }
        });
    }else{
        toastr['warning']('', 'LA URL DEL ARCHIVO ES NECESARIA');
        $("#url").focus();
    }
    
}

function preguntar() {
    var id = $("#articulo_id").val();
    var pregunta = tinymce.get("descripcion").getContent();
    var titulo = $("#titulo").val();
    if (pregunta != "" && titulo != "") {
        var url="home/queries/opciones/soporte.php";
        $.post(url,{id:id,pregunta:pregunta,titulo:titulo},function(data){
            if(data == 1){
                toastr['success']('', 'PREGUNTA ENVIADA A SOPORTE');
                $("#titulo_"+id).val("");
                setTimeout(function(){
                    atrasRedactores();
                    $('#collaps_'+id).trigger('click');
                },200)
                                
            }else if(data == 3){
                toastr["warning"]("", "NO SE HA PODIDO GUARDAR");
                location.reload();
            }else{
                toastr['error']('', 'NO SE HA PODIDO GUARDAR');
                location.reload();
            }
        });
    }else{
        toastr['warning']('', 'DEBES INGRESAR EL CONTENIDO Y TITULO DE LA PREGUNTA');
        $("#pregunta").focus();
    }
    
}




$('#TooltipDemo').click(function(){
    atrasRedactores2();
});

function entregar(id) {
    $(".panel_li").hide();
    $("#solo_entregar").show();
    $(".bot").hide();
    $("#bot_entregar").show();

    $("#articulo_id").val(id);
    $('#TooltipDemo').trigger('click');
    $("#TooltipDemo").show('slow');
}

function soporte(id) {
    $(".panel_li").hide();
    $("#solo_soporte").show();
    $("#mensaje_nota").show();
    $(".bot").hide();
    $("#bot_soporte").show();

    $("#articulo_id").val(id);
    $('#TooltipDemo').trigger('click');
    $("#TooltipDemo").show('slow');
}

function atrasRedactores(){
    $('#TooltipDemo').trigger('click');
    $("#TooltipDemo").hide('slow');
    $("#titulo").val("");
    $("#url").val("");
    tinymce.get("descripcion").setContent('');

}

function atrasRedactores2(){
    $("#TooltipDemo").hide('slow');
    $("#titulo").val("");
    tinymce.get("descripcion").setContent('');
    $("#url").val("");
}

function eliminarNota(id) {
    var url="home/queries/home/eliminar_nota.php";
    $.post(url,{id:id},
    function (response){
        if(response == 1){
            toastr['success']('', 'SE HA ELIMINADO LA NOTA DEL PANEL DE REDACTOR');
            $("#notes").hide('slow');
                            
        }else{
            toastr['error']('', 'NO SE HA PODIDO ELIMINAR LA NOTA');
        }
    });
}