<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
include('../../../include/coneccion.php');
include('../../../include/funciones.php');

$fechaToday = date('Y-m-d H:i:s');
$z = 1;
$id_logIn  = idUsuario();
$tipo_usuario = tipoUsuario();



$query="SELECT articulo_id, articulo_ref, articulo_palabras, articulo_entrega FROM articulos Where articulo_redactor = '$id_logIn' and articulo_status = 2 order by articulo_entrega ASC limit 20 ";

$res=$conn->query($query);
if($res->num_rows > 0){
    while($fila=$res->fetch_array()){
        $articulo_id           = $fila['articulo_id'];
        $articulo_ref          = $fila['articulo_ref'];
        $articulo_palabras     = $fila['articulo_palabras'];
        $articulo_entrega      = $fila['articulo_entrega'];
        $valor = $articulo_id.",'".$articulo_ref."'";

        $query2="SELECT numero,date_valoracion FROM valoraciones_redactor Where redactor_id = '$id_logIn' and articulo_id = '$articulo_id' limit 1 ";

        $res2=$conn->query($query2);
        if($res2->num_rows > 0){
            while($fila2=$res2->fetch_array()){
                $numero           = $fila2['numero'];
                $date_valoracion  = $fila2['date_valoracion'];
       
                $completo = '<li class="list-group-item">
                                            <div class="todo-indicator bg-success"></div>
                                            <div class="widget-content p-0">
                                                <div class="widget-content-wrapper">
                                                    <div class="widget-content-left flex2">
                                                        <div class="widget-heading">REF: '.$articulo_ref.' / '.$articulo_palabras.' PALABRAS</div>
                                                        <div class="widget-subheading">Revisado: '.$date_valoracion.'</div>
                                                    </div>';
                                                    if ($numero < 7) {
                                                        $completo .='
                                                    <div class="widget-content-right">
                                                        <div class="badge badge-warning mr-2">'.$numero.'</div>
                                                    </div>
                                                     <div class="widget-content-right">
                                                        <button class="border-0 btn-transition btn btn-outline-danger" onclick="result('.$valor.')">
                                                            <i class="fa fa-chevron-right"></i>
                                                        </button>
                                                    </div>';
                                                    }else{
                                                        $completo .='
                                                    <div class="widget-content-right">
                                                        <div class="badge badge-success mr-2">'.$numero.'</div>
                                                    </div>
                                                     <div class="widget-content-right">
                                                        <button class="border-0 btn-transition btn btn-outline-success" onclick="result('.$valor.')">
                                                            <i class="fa fa-chevron-right"></i>
                                                        </button>
                                                    </div>';
                                                    }
                                  
                                            $completo .='
                                                </div>
                                            </div>
                                        </li>';
      
                    $script = '';

            $productos[$articulo_id]= $completo.$script;
            }
        }

        $z++;
    
    }
}

if(empty($productos)){
    echo "<li class='list-group-item'>NO TIENE REVISIONES AÚN..</li> ";
}else{

    foreach($productos as $producto)
    {
        echo $producto;
    }
}
?>
