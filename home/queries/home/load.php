<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
include('../../../include/coneccion.php');
include('../../../include/funciones.php');

$fechaToday = date('Y-m-d H:i:s');
$z = 1;
$id_logIn  = idUsuario();
$tipo_usuario = tipoUsuario();


$query="SELECT articulo_id, articulo_ref, articulo_tiempo, articulo_descripcion, articulo_palabras, articulo_date FROM articulos Where articulo_redactor = '$id_logIn' and articulo_status = 0 order by articulo_id desc ";

$res=$conn->query($query);
if($res->num_rows > 0){
    while($fila=$res->fetch_array()){
        $articulo_id           = $fila['articulo_id'];
        $articulo_ref          = $fila['articulo_ref'];
        $articulo_tiempo       = $fila['articulo_tiempo'];
        $articulo_palabras     = $fila['articulo_palabras'];
        $articulo_date         = $fila['articulo_date'];
        $articulo_descripcion  = $fila['articulo_descripcion'];

        $dif_horas = diferenciaHoras($articulo_date,$fechaToday);
        $restante = $articulo_tiempo - $dif_horas;
        if ($restante < 1) {
            $restante = "Tiempo Agotado";
        }else{
            $restante = $restante." Horas";
        }

       
            $completo = '<div class="panel tx_'.$articulo_id.' panel-'.$status_ordenes_color[2].' card-view panel-refresh" >
                            <div class="refresh-container2">
                                <div class="la-anim-1"></div>
                            </div>
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-dark inline-block"> <span style="color:#fff"> REF: '.$articulo_ref.' - </span> '.$articulo_palabras.' Palabras <span style="color:#fff"> </span><span style="color:red;"> <i class="fa fa-clock-o"></i> '.$restante.'</span></h6>
                                </div>

                                <div class="pull-right" >
                                    <a class="pull-left inline-block mr-15"  data-toggle="collapse" href="#collapse_'.$articulo_id.'" aria-expanded="true" >
                                        <i class="zmdi zmdi-chevron-down" style="color:#333;"></i>
                                        <i class="zmdi zmdi-chevron-up" id="collaps_'.$articulo_id.'" style="color:#333;"></i>
                                    </a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div  id="collapse_'.$articulo_id.'" class="panel-wrapper collapse">
                                <div  class="panel-body">
                                    <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-wrap">
                                                    <form class="form-horizontal" role="form">
                                                        <div class="form-body">
                                                            <h6 class="txt-dark capitalize-font"><i class="zmdi zmdi-format-subject mr-10"></i>DETALLES DE LA REDACCIÓN</h6>
                                                            <hr class="light-grey-hr"/>
                                                            <div class="row" style="overflow-x: auto;">
                                                                <div class="col-md-12">
                                                                   
                                                                            '.$articulo_descripcion.'
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                    </form>
                                                    
                                                        <div class="col-md-12">
                                                            <div class="pull-right">
                                                                <button onclick="soporte('.$articulo_id.')" class="btn btn-warning pd-x-20">SOPORTE</button>
                                                                <button onclick="entregar('.$articulo_id.')" class="btn btn-success pd-x-20">ENTREGAR</button>
                                                                <button onclick="rechazar('.$articulo_id.')" class="btn btn-danger pd-x-20">RECHAZAR</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                </div>
                                            </div>
                                        </div>
                                    <div class="pull-left inline-block"><br>    
                                        <ul class="nav nav-pills" style="font-size:12px;">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>';
      
                    $script = '';

            $productos[$articulo_id]= $completo.$script;

        $z++;
    
    }
}

if(empty($productos)){
    echo "<tr><td colspan=7 class='text-center text-muted'>NO TIENE ASIGNADO NINGÚN PEDIDO..</td></tr> ";
}else{

    foreach($productos as $producto)
    {
        echo $producto;
    }
}
?>
