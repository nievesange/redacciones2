<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
include('../../../include/coneccion.php');
include('../../../include/funciones.php');

$fechaToday = date('Y-m-d H:i:s');
$fecha_date = date('Y-m-d');
$z = 1;

$query="SELECT id_redactor, monto_total, ultimo_pago FROM perfil_redactor where id_redactor > 2 order by ultima_conexion DESC";
$res=$conn->query($query);
if($res->num_rows>0){
    while($fila=$res->fetch_array()){
        $id_redactor      = $fila['id_redactor'];
        $tipo_usuario = idTipoUsuario($id_redactor);
        if ($tipo_usuario == 3) {
            $redactor  = usuarioId($id_redactor);
            $pago      =  $fila['monto_total'];

            $last_page      = $fila['ultimo_pago'];
            if (!$last_page or $last_page == null or $last_page == "" ) {
                $last_page = "2019-05-01 00:00:00";
            }
            $query4 = "SELECT articulo_palabras from articulos where (articulo_status = 1 or articulo_status = 2) and articulo_redactor = '$id_redactor' and articulo_entrega > '$last_page'";
            $res4 = $conn->query($query4);
            $cant_palabras = 0;
            if($res4->num_rows > 0){
                while($fila4=$res4->fetch_array()){
                    $cant_palabras = $cant_palabras + $fila4['articulo_palabras'];
                }
                $suma = ($cant_palabras/100)*$pago;
                $suma = number_format($suma, 2, '.', '');
            }else{
                $suma = 0;
            }
            $completo = '<tr>
                            <td style="text-align: center;"><b >'.ucwords($redactor).'</b></td>
                            <td style="text-align: center;"><span id="sumax_'.$id_redactor.'">'.$suma.'</span> $</td>
                           <td class="text-center" style="text-align: center; width:1px;" >
                                <div class="btn-toolbar">
                                    <div class="btn-group">
                                        <button onclick="hacerPago('.$id_redactor.')" data-toggle="tooltip" data-placement="top" data-original-title="PAGAR" type="button" class="btn btn-success btn-xs"><i class="fa fa-money"></i></button>
                                    </div>
                                </div>
                            </td>
                        </tr>';
          
                $script = "";

                $productos[$id_redactor]= $completo.$script;
            $z++;
        }

    }
}

if(empty($productos))
{
    echo "<tr><td colspan=7 class='text-center text-muted'>NO SE ENCONTRARON RESULTADOS</td></tr>";
}else{

    foreach($productos as $producto)
    {
        echo $producto;
    }
}
?>
