function loadPagar(){
    var id=1;
    var url="pagar/queries/pagar/load.php";
    $.post(url,{id:id},
    function (response){
        //json = eval('('+response+')');
        $("#pagar").html(response);
    });

}
loadPagar();

function atras() {
    $('#modalPago').modal('hide');
    $('#modalSoporte').modal('hide');
    $("#suma").val("");
    $("#id_redactor").val("");
}

function pagar(){
	var id = $("#redactor_id").val();
	var suma = $("#suma").html();
    var pay_select = $("#select_pay").val();
    var pay = $("#"+pay_select).html();
    var val_pay = $("#x"+pay_select).html();
    if (val_pay == "") {
        toastr["warning"]("", "NO TIENE CORREO EN ESTE TIPO DE PAGO");
    }else{
         var result = inputNumeros2(suma);
        if (suma != "" && result != 1) {
            var titulo = "PAGO POR "+suma+"$ A TRAVÉS DE: "+pay;
            preguntar(id,suma,titulo,1);
        }else{
            toastr['warning']('', 'DEBES INGRESAR UN MONTO A PAGAR CORRECTO EJ: 2.2');
            $("#suma").focus();
        }
    }
   
}


function preguntar(id,suma,titulo,tipo) {
    if (tipo == 1) {
    	var pregunta = "Ha recibido un pago por un monto de "+suma+ "$";
    }else{
    	var pregunta = suma;
    }
    if (pregunta != "" || titulo != "" ) {
        var url="redactores/queries/redactores/soporte.php";
        $.post(url,{id:id,pregunta:pregunta,tipo:tipo,suma:suma,titulo:titulo},function(data){
            if(data == 1){
                toastr['success']('', 'SE HA ENVIADO UN TICKECT AL REDACTOR');
                $("#id_redactor").val("");
                $("#sumax_"+id).val("");
                $("#titulo").val("");
                $("#pregunta").val("");
                setTimeout(function(){
                    loadPagar();
                    atraspagar()
                },300)
                                
            }else if(data == 3){
                toastr["warning"]("", "NO SE HA PODIDO ENVIAR EL TICKECT");
                setTimeout(function(){
                    loadpagar();
                    atraspagar()
                },300)
            }else if(data == 4){
                toastr["warning"]("", "EL MONTO A PAGAR SUPERA EL ACUMULADO");
                setTimeout(function(){
                    loadpagar();
                    atraspagar()
                },300)
            }else{
                toastr['error']('', 'NO SE HA PODIDO ENVIAR EL TICKECT');
                setTimeout(function(){
                    loadpagar();
                    atraspagar()
                },300)
            }
        });
    }else{
        toastr['warning']('', 'DEBES LLENAR TODOS LOS CAMPOS');
        $("#pregunta").focus();
    }
    
}

function asignaNombre(id) {
    var url="redactores/queries/redactores/asigna_nombre.php";
    $.post(url,{id:id},
    function (response){
        $(".redactor").html(response);
    });
}

$('#TooltipDemo').click(function(){
    atraspagar2();
});

function hacerPago(id) {
    var suma = $('#sumax_'+id).html();
    if (suma > 0 ) {
        $('#suma').html(suma);
        $(".panel_li").hide();
        $("#solo_pagar").show();
        asignaNombre(id);
        $("#mensaje_nota").show();
        var url="pagar/queries/pagar/pagos.php";
        $.post(url,{id:id},
        function (response){
            $("#mensaje_nota").html(response);
        });
        $(".bot").hide();
        $("#bot_pagar").show();

        $("#redactor_id").val(id);
        $('#TooltipDemo').trigger('click');
        $("#TooltipDemo").show('slow');
        $(".card-footer").css('opacity','0.1');
    }else{
        toastr["warning"]("", "NO TIENE DINERO ACUMULADO");
    }
}



function atraspagar(){
    $('#TooltipDemo').trigger('click');
    $("#TooltipDemo").hide('slow');
    $("#titulo").val("");
    $(".card-footer").css('opacity','1');

}

function atraspagar2(){
    $("#TooltipDemo").hide('slow');
    $("#titulo").val("");
    $(".card-footer").css('opacity','1');
}



// Pagos Redactor

function loadOrden(){
    //loaderIn();
    var id = 1;
    var url="pagar/queries/pagar/load_pagos.php";
    $.post(url,{id:id},
    function (response){
        $("#ultimos_pagos").html(response);
    });
    
}
loadOrden();

$(document).ready(function() {
    $("#image").change(function(e) {  
        var formData = new FormData();
        var files = $('#image')[0].files[0];
        formData.append('file',files);
        $.ajax({
            url: 'perfil/queries/perfil/upload.php',
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response == 1) {
                    alert('NO guardo.');
                } else if(response != 0){
                    $(".card-img-top").attr("src", response);
                }else {
                    alert('Formato de imagen incorrecto.');
                }
            }
        });
        return false;
    });
});
