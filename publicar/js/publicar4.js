function comenzarPedidos() {
    var ref_pedido = $("#ref_pedido").val();
    var tiempo_entrega = $("#tiempo_entrega").val(); 
    var redactor = $("#redactor").val();
    var palabras = $("#palabras").val(); 
    var result = inputNumeros(palabras);
    var content = tinymce.get("descripcion_general").getContent();
    if (ref_pedido == "") {
        toastr["error"]("", "DEBE INGRESAR LA REF DEL PEDIDO");
        $("#ref_pedido").focus();
        return false;
    }else if(palabras == ""){
        toastr["error"]("", "DEBE INGRESAR LA CANTIDAD DE PALABRAS ");
        $("#palabras").focus();
        return false;
    }else if(result == 1){
        toastr["error"]("", "SOLO PUEDE INGRESAR NÚMEROS EN LA CANTIDAD DE PALABRAS");
        $("#palabras").focus();
        return false;
    }else if(content == ""){
        toastr["error"]("", "DEBES INGRESAR LOS DETALLES DEL PEDIDO");
        $("#descripcion_general").focus();
        return false;
    }else if(redactor == "0"){
        toastr["error"]("", "DEBES ASIGNAR ESTE PEDIDO");
        $("#redactor").focus();
        return false;
    }else{
        swal({
            title: 'ESTA SEGURO?',
            text: 'QUE DESEA HACER ESTA PUBLICACIÓN',
            type: 'success',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'SI, PUBLICAR!',
            cancelButtonText: 'NO',
            closeOnConfirm: true,
            closeOnCancel: true
        },function(isConfirm){
            if (isConfirm) {
                $.post("publicar/queries/publicar/publicar.php",{ref_pedido:ref_pedido,tiempo_entrega:tiempo_entrega,palabras:palabras,redactor:redactor,content:content},
                function(data){
                    if(data == 1){
                        toastr["success"]("", "SE HA PUBLICADO CON EXITO Y APARECERÁ EN EL PANEL DEL REDACTOR SELECCIONADO");
                        setTimeout(function(){
                            limpiar()
                        },300)
                    }else if(data == 3){
                        toastr["warning"]("", "EL TITULO YA SE ENCUNTRA PUBLICADO");
                    }else{
                        toastr["error"]("", "ERROR AL PUBLICAR");
                    }
                });
            }
        });
    }
}

function limpiar(){
    tinymce.get("descripcion_general").setContent('');
    $("#redactor").val("0");
    $("#palabras").val("");
    $("#ref_pedido").val("");
    $("#tiempo_entrega").val("24"); 
}