<div class="app-main__outer" style="width: 100%;flex: unset;">
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="pe-7s-diamond icon-gradient bg-warm-flame">
                        </i>
                    </div>
                    <div>PEDIDOS QUE HAN SIDO RECHAZADOS POR ALGÚN REDACTOR
                        <div class="page-title-subheading">(POSIBILIDAD DE REASIGNAR)
                        </div>
                    </div>
                </div>   
            </div>
        </div>
        <div class="tabs-animation">
            <div id="home" class="view_user">
                <div class="row view_home" id="asigned">
                    <div class="col-md-12">
                        <div class="main-card mb-3 card">
                            <div id="listarOrdenes" style="margin:2%;"></div>  
                        </div>            
                    </div>
                </div>
            </div>
        </div>
<script type="text/javascript" src='activas/js/asignar.js'></script>  
<script src="add/vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>
<script src="add/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js">
</script>
<script src="add/vendors/toastr/toastr.min.js"></script>
