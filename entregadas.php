
     <div class="ui-theme-settings">
        <button type="button" id="TooltipDemo" class="btn-open-options btn btn-warning" style="display: none;">
            <i class="fa  fa-times fa-w-16 fa-2x"></i>
        </button>
        <button type="button" id="TooltipDemo1" class=" btn btn-success" style="bottom: 120px;">
            <i class="fa fa-floppy-o fa-w-16 fa-2x"></i>
        </button>
        <div class="theme-settings__inner">
            <div class="scrollbar-container ps">
                <div class="theme-settings__options-wrapper">
                    <input type="hidden" name="articulo_id" id="articulo_id">
                    <div class="p-3">
                        <ul class="list-group">
                            <div id="solo_rechazar" style="display: none;">
                                <li>EXPLICALE AL REDACTOR PORQUE HA SIDO RECHAZADO ESTE PEDIDO</li>
                            </div>
                            <div id="solo_aprobar">
                            <li class="list-group-item">
                                <div class="widget-content p-0">
                                    <div class="widget-content-wrapper">
                                        <div class="widget-content-left mr-3">
                                            <div class="switch has-switch switch-container-class" data-class="">
                                                <div class="switch-animate switch-on">
                                                    <input type="checkbox" name="valor" value="1" id="checkbox1" data-toggle="toggle" data-onstyle="success">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget-content-left">
                                            <div class="widget-heading">Ortografía/Gramática
                                            </div>
                                            <div class="widget-subheading">Sin errores ortográficos!
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="widget-content p-0">
                                    <div class="widget-content-wrapper">
                                        <div class="widget-content-left mr-3">
                                            <div class="switch has-switch switch-container-class" data-class="">
                                                <div class="switch-animate switch-on">
                                                    <input type="checkbox" name="valor" value="2" id="checkbox2" data-toggle="toggle" data-onstyle="success">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget-content-left">
                                            <div class="widget-heading">Tipo de redación adecuado
                                            </div>
                                            <div class="widget-subheading">Sujeto a las especificaciones del cliente!
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="widget-content p-0">
                                    <div class="widget-content-wrapper">
                                        <div class="widget-content-left mr-3">
                                            <div class="switch has-switch switch-container-class" data-class="">
                                                <div class="switch-animate switch-on">
                                                    <input type="checkbox" name="valor" value="3" id="checkbox3" data-toggle="toggle" data-onstyle="success">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget-content-left">
                                            <div class="widget-heading">Texto con sentido
                                            </div>
                                            <div class="widget-subheading">Oraciones y párrafos que se entienden
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="widget-content p-0">
                                    <div class="widget-content-wrapper">
                                        <div class="widget-content-left mr-3">
                                            <div class="switch has-switch switch-container-class" data-class="">
                                                <div class="switch-animate switch-on">
                                                    <input type="checkbox" name="valor" value="4" id="checkbox4" data-toggle="toggle" data-onstyle="success">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget-content-left">
                                            <div class="widget-heading">Redacción optimizada
                                            </div>
                                            <div class="widget-subheading">Buen uso de palabras semántica
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="widget-content p-0">
                                    <div class="widget-content-wrapper">
                                        <div class="widget-content-left mr-3">
                                            <div class="switch has-switch switch-container-class" data-class="">
                                                <div class="switch-animate switch-on">
                                                    <input type="checkbox" name="valor" value="3" id="checkbox5" data-toggle="toggle" data-onstyle="success">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget-content-left">
                                            <div class="widget-heading">Adaptados a las especificaciones
                                            </div>
                                            <div class="widget-subheading">Sin errores ortográficos!
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="widget-content p-0">
                                    <div class="widget-content-wrapper">
                                        <div class="widget-content-left mr-3">
                                            <div class="switch has-switch switch-container-class" data-class="">
                                                <div class="switch-animate switch-on">
                                                    <input type="checkbox" name="valor" value="3" id="checkbox6" data-toggle="toggle" data-onstyle="success">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget-content-left">
                                            <div class="widget-heading">SEO optimizado 
                                            </div>
                                            <div class="widget-subheading">Sin errores ortográficos!
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="widget-content p-0">
                                    <div class="widget-content-wrapper">
                                        <div class="widget-content-left mr-3">
                                            <div class="switch has-switch switch-container-class" data-class="">
                                                <div class="switch-animate switch-on">
                                                    <input type="checkbox" name="valor" value="4" id="checkbox7" data-toggle="toggle" data-onstyle="success">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget-content-left">
                                            <div class="widget-heading">Variación keywords
                                            </div>
                                            <div class="widget-subheading">Sin errores ortográficos!
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            </div>
                            <li>
                                <br>
                                <textarea class="tinymce" id="descripcion"></textarea>
                                <div class="modal-footer" id="foot_tecnico2">
                                    <button type="button" onclick="atrasRevisar()" class="btn btn-warning pd-x-20">ATRAS</button>
                                    <button onclick="cerrarRevisar()" id="bot_revisar" class="btn btn-success pd-x-20">FINALIZAR</button>
                                    <button onclick="regCancelar()" id="bot_rechazar" class="btn btn-success pd-x-20" style="display: none;">FINALIZAR</button>
                                </div>
                            </li>
                            <li><br><br></li>
                        </ul>
                    </div>
                </div>
            <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
        </div>
    </div>
<div class="app-main__outer" style="width: 100%;flex: unset;">
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="pe-7s-diamond icon-gradient bg-warm-flame">
                        </i>
                    </div>
                    <div>ENTREGADAS
                        <div class="page-title-subheading">PEDIDOS ENTREGADOS POR LOS REDACTORES (DEJAR UNA VALORACIÓN Y UN COMENTARIO)
                        </div>
                    </div>
                </div>   
            </div>
        </div>
        <div class="tabs-animation">
            <div id="home" class="view_user">
                <div class="row view_home" id="asigned">
                    <div class="col-md-12">
                        <div class="main-card mb-3 card">
                            <div id="listarOrdenes" style="margin:2%;"></div>  
                        </div>            
                    </div>
                </div>
                
            </div>
        </div>
<script type="text/javascript" src="revisar/js/revisar9.js"></script>
<script src="add/vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>
<script src="add/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js">
</script>
<script src="add/vendors/toastr/toastr.min.js"></script>
<script src="add/vendors/bower_components/tinymce/tinymce.min.js"></script>
<script src="add/dist/js/tinymce-data.js"></script>
<style type="text/css">
    #mceu_15-body,#mceu_16-body{
        display: none !important;
    }

</style>
