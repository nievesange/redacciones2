<div class="ui-theme-settings">
    <button type="button"  id="TooltipDemo" class="btn-open-options btn btn-warning" style="display: none;">
        <i class="fa  fa-times fa-w-16 fa-2x"></i>
    </button>
    <button type="button"  id="TooltipDemo1" class=" btn btn-success" style="bottom: 120px;">
        <i class="fa fa-floppy-o fa-w-16 fa-2x" ></i>
    </button>
    <div class="theme-settings__inner">
            <div class="scrollbar-container">
                <div class="theme-settings__options-wrapper">
                    <input type="hidden" name="soporte_id" id="soporte_id">
                    <div class="p-3" >
                        <ul class="list-group">
                            <li><br></li>
                            <div id="solo_mensaje" class="panel_li" style="display: none;">
                                <li>RESPONDER 
                                </li>
                            </div>
                            <div id="solo_tick" class="panel_li" style="display: none;">
                                <li>ENVIAR UN MENSAJE A TODOS LOS REDACTORES
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-thumb-tack"></i>
                                            </div>
                                            <input type="text" id="titulo" placeholder="ASUNTO DEL TICKECT" class="form-control">
                                        </div>
                                    </div><!-- form-group -->
                                </li>
                            </div>
                            <div id="solo_nota" class="panel_li" style="display: none;">
                                <li>
                                    ENVIAR UNA NOTA GENERAL A TODOS LOS REDACTORES
                                </li>
                            </div>
                            <li>
                                <br>
                                <textarea class="tinymce" id="descripcion"></textarea>
                                
                            </li>
                            <li><br><br></li>
                            <li>
                                <button type="button" onclick="atrasRedactores()"   class="btn btn-warning pd-x-20" >ATRAS</button>
                                <button onclick="preguntar()" id="bot_mensaje" class="btn bot btn-success pd-x-20">FINALIZAR</button>
                                <button onclick="tickectPublicar()" id="bot_tick" class="btn bot btn-success pd-x-20" style="display: none;">FINALIZAR</button>
                                <button onclick="enviarNota()" id="bot_nota" class="btn bot btn-success pd-x-20" style="display: none;">FINALIZAR</button>
                                
                            </li>
                            <li><br><br></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
</div>
<div class="app-main__outer">
    <div class="app-main__inner" style="flex: 0;">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading ">
                    <div class="page-title-icon">
                        <i class="pe-7s-diamond icon-gradient bg-warm-flame">
                        </i>
                    </div>
                    <div class="">SOPORTE 
                        <div class="page-title-subheading">PANEL DE TICKECTS DE SOPORTE
                        </div>
                    </div>
                    <?php if (tipoUsuario() < 3 ){?>
                    <button type="button" onclick="tickectModal()" class="p-0 mr-2 btn btn-">
                        <span class="icon-wrapper icon-wrapper-alt rounded-circle">
                            <span class="icon-wrapper-bg bg-primary"></span>
                            <i class="icon text-primary ion ion-android-mail"></i>
                        </span>
                    </button>
                    <button type="button" onclick="nota()" class="p-0 mr-2 btn btn-">
                        <span class="icon-wrapper icon-wrapper-alt rounded-circle">
                            <span class="icon-wrapper-bg bg-primary"></span>
                            <i class="icon text-primary ion ion-android-desktop"></i>
                        </span>
                    </button>
                    <?php } ?>
                </div>   
            </div>
                
        </div>
       <?php if (tipoUsuario() < 3 ){?>
       <div class="page-title-wrapper" style="margin-bottom: 10%;">
            <div class="col-lg-12 col-md-12 col-xs-12" id="comenzar" style="padding: 0px !important" >  
                <div class="col-lg-2 col-md-12 col-xs-12"></div>
                
                <div class="col-lg-4 col-md-9 col-xs-9">
                    <select id="redactor" class="form-control ">
                        <option value="0">SELECCIONAR:</option>
                            <?php selectUsuario("(idtipousuario = 3) and activo = 1"); ?>
                    </select>
                </div>
                <div class="col-lg-4 col-md-12 col-xs-12">
                    <a href="javascript:void(0);" class="btn btn-success" id="comenzar_boton" style="font-size:14px;text-align:center;text-transform: none;margin-top: 1%;" onclick="buscarSoporte()">BUSCAR TICKECTS POR REDACTOR</a>
                </div>
                <div class="col-lg-2 col-md-3 col-xs-3">
                    <a href="javascript:void(0);" class="btn btn-primary" style="font-size:14px;text-align:center;text-transform: none;" onclick="recargar()"><i class="fa fa-refresh"></i></a>
                </div>
            </div>
        </div>
    <?php } ?>
    </div>
        <div class="tabs-animation">
            <div id="home" class="view_user">
                <div class="row view_home" id="">
                    <div class="col-md-12">
                        <div class="main-card mb-3 card">
                            <div id="listarOrdenes" style="margin:2%;"></div>  
                        </div>            
                    </div>
                </div>
            </div>
        </div>
<script type="text/javascript" src='soporte/js/soporte6.js?v=0.001'></script>  
<script src="add/vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>
<script src="add/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js">
</script>
<script src="add/vendors/toastr/toastr.min.js"></script>
<script src="add/vendors/bower_components/tinymce/tinymce.min.js"></script>
<script src="add/dist/js/tinymce-data.js"></script>
<style type="text/css">
    #mceu_16-body{
        display: none !important;
    }
</style>
