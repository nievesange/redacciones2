<div class="ui-theme-settings">
        <button type="button"  id="TooltipDemo" class="btn-open-options btn btn-warning" style="display: none;">
            <i class="fa  fa-times fa-w-16 fa-2x"></i>
        </button>
        <button type="button"  id="TooltipDemo1" class=" btn btn-success" style="bottom: 120px;">
            <i class="fa fa-floppy-o fa-w-16 fa-2x" ></i>
        </button>
        <div class="theme-settings__inner">
            <div class="scrollbar-container">
                <div class="theme-settings__options-wrapper">
                    <input type="hidden" name="articulo_id" id="articulo_id">
                    <div class="p-3" >
                        <ul class="list-group">
                            <li><br></li>
                            <div id="solo_soporte" class="panel_li" style="display: none;">
                                <li>SOPORTE 
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-thumb-tack"></i>
                                            </div>
                                            <input type="text" id="titulo" placeholder="ASUNTO DEL TICKECT" class="form-control">
                                        </div>
                                    </div><!-- form-group -->
                                </li>
                            </div>
                            <div id="solo_entregar" class="panel_li" style="display: none;">
                                <li><br><br><br><br><br>
                                    ENTREGAR 
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-thumb-tack"></i>
                                            </div>
                                            <input type="text" id="url" placeholder="URL DEL ARCHIVO" class="form-control">
                                        </div>
                                    </div><!-- form-group -->
                                </li>
                            </div>
                            
                            <li id="mensaje_nota" class="panel_li" style="display: none;">
                                <br>
                                <textarea class="tinymce" id="descripcion"></textarea>
                            </li>
                            <li>
                                <button type="button" onclick="atrasRedactores()"   class="btn btn-warning pd-x-20" >ATRAS</button>
                                <button onclick="preguntar()" id="bot_soporte" class="btn bot btn-success pd-x-20" style="display: none;">PREGUNTAR</button>
                                <button onclick="cerrar()" id="bot_entregar" class="btn bot btn-success pd-x-20" style="display: none;">ENTREGAR</button>
                                
                            </li>
                            <li><br><br></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
</div>

<div class="app-main__outer" style="width: 100%;flex: unset;">
    <div class="app-main__inner">
        <div class="col-md-12">
            <div class="page-title-subheading col-md-12" id="notes"><?php echo message(); ?>
            </div>
        </div>
        <div class="tabs-animation col-md-12">
            <div id="home" class="view_user">
                <div class="mb-3 card">
                    <div class="card-header-tab card-header">
                        <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                            <i class="header-icon lnr-charts icon-gradient bg-happy-green"> </i>
                            RESUMEN DE ACTIVIDAD
                        </div>
                    </div>
                    <div class="no-gutters row">

                       <!-- <div class="col-sm-9 col-md-3 col-xl-3">
                            <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
                                <div class="icon-wrapper rounded-circle">
                                    <div class="icon-wrapper-bg opacity-10 bg-success"></div>
                                    <i class="lnr-apartment text-dark opacity-8"></i>
                                </div>
                                <div class="widget-chart-content">
                                    <div class="widget-subheading">POR APROBACIÓN</div>
                                    <div class="widget-numbers text-success"><span id="sin_aprobar"></span></div>
                                        
                                </div>
                            </div>
                            <div class="divider m-0 d-md-none d-sm-block"></div>
                        </div>-->
                        <div class="col-sm-9 col-md-4 col-xl-4">
                            <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
                                <div class="icon-wrapper rounded-circle">
                                    <div class="icon-wrapper-bg opacity-10 bg-success"></div>
                                    <i class="lnr-apartment text-dark opacity-8"></i>
                                </div>
                                <div class="widget-chart-content">
                                    <div class="widget-subheading">ACUMULADO</div>
                                    <div class="widget-numbers text-success"><span id="acumulado"></span></div>
                                        
                                </div>
                            </div>

                            <div class="divider m-0 d-md-none d-sm-block"></div>
                        </div>

                        <div class="col-sm-9 col-md-4 col-xl-4">
                            <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
                                <div class="icon-wrapper rounded-circle">
                                    <div class="icon-wrapper-bg opacity-9 bg-danger"></div>
                                    <i class="lnr-keyboard text-white"></i>
                                </div>
                                <div class="widget-chart-content">
                                    <div class="widget-subheading">POR ENTREGAR</div>
                                    <div class="widget-numbers"><span id="palabras_hoy"></span></div>
                                    <div class="widget-description opacity-8 text-focus">
                                        PEDIDOS:
                                        <span class="text-info pl-1">
                                            <span class="pl-1" id="pedidos_hoy"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="divider m-0 d-md-none d-sm-block"></div>
                        </div>
                        
                        <div class="col-sm-9 col-md-4 col-xl-4">
                            <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
                                <div class="icon-wrapper rounded-circle">
                                    <div class="icon-wrapper-bg opacity-9 bg-success"></div>
                                    <i class="lnr-spell-check text-white"></i></div>
                                    <div class="widget-chart-content">
                                        <div class="widget-subheading">NIVEL</div>
                                        <div class="widget-numbers" id="nivel"></div>
                                        <div class="widget-description opacity-8 text-focus">
                                            <div class="d-inline text-danger pr-1">
                                                <span class="pl-1" id="progress"></span>
                                            </div>
                                            <span id="text"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="text-center d-block p-3 card-footer">
                            <button class="btn-pill btn-shadow btn-wide fsize-1 btn btn-primary btn-lg">
                                <span class="mr-2 opacity-7">
                                    <i class="icon icon-anim-pulse ion-ios-analytics-outline"></i>
                                </span>
                                <a href="?name=stats"> <span class="mr-1">ESTADÍSTICAS</span></a>
                            </button>
                        </div>
                    </div>
                </div>
                MIS ASIGNACIONES
                <div class="row view_home" id="asigned">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="panel panel-default card-view panel-refresh" style="border:0px;">
                            <div  id="collapse_0" class="panel-wrapper  in">
                                <div  class="panel-body">
                                    <div id="listarOrdenes"></div> <!-- Aqui fin ordenes-->
                                </div>
                            </div>
                        </div>       
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="panel panel-default card-view panel-refresh" style="border:0px;">
                            <div  id="collapse_0" class="panel-wrapper  in">
                                <div  class="panel-body">
                                    <div id="listarPalabrasAll"></div> <!-- Aqui fin ordenes-->
                                </div>
                            </div>
                        </div>       
                    </div>
                </div>
                <div class="col-sm-12 col-lg-6">
                    <div class="card-hover-shadow-2x mb-3 card" style="height: 550px;">
                        <div class="card-header-tab card-header">
                            <div class="card-header-title font-size-lg text-capitalize font-weight-normal"><i class="header-icon lnr-database icon-gradient bg-malibu-beach"> </i>ÚLTIMAS REVISIONES</div>
                        </div>
                        <div class="scroll-area-lg" style="height: 500px;">
                            <div class="scrollbar-container">
                                <div class="p-2">
                                    <ul class="todo-list-wrapper list-group list-group-flush" id="valoraciones">
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
                <div class="col-sm-12 col-lg-6">
                    <div class="card-hover-shadow-2x mb-3 card" style="height: 550px;">
                        <div class="card-header-tab card-header">
                            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                                <i class="header-icon pe-7s-check icon-gradient bg-amy-crisp"> </i><span id="value" style="margin-left:2%;font-weight: bold;font-size: 1rem;color:green"></span>
                            </div>
                            
                        </div>
                        <div class="scroll-area-lg" style="height: 500px">
                            <div class="scrollbar-container">
                                <div class="p-4">
                                    <div class="vertical-time-simple vertical-without-time vertical-timeline vertical-timeline--animate vertical-timeline--one-column">
                                        <div class="vertical-timeline-item dot-danger vertical-timeline-element">
                                            <div>
                                                <span class="vertical-timeline-element-icon bounce-in" style="margin-top: 1%;"></span>
                                                <div class="vertical-timeline-element-content bounce-in">
                                                    <h4 class="timeline-title" id="lunes"></h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-item dot-warning vertical-timeline-element">
                                            <div>
                                                <span class="vertical-timeline-element-icon bounce-in" style="margin-top: 1%;"></span>
                                                <div class="vertical-timeline-element-content bounce-in">
                                                    <h4 class="timeline-title" id="martes"></h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-item dot-success vertical-timeline-element">
                                            <div>
                                                <span class="vertical-timeline-element-icon bounce-in" style="margin-top: 1%;"></span>
                                                <div class="vertical-timeline-element-content bounce-in">
                                                    <h4 class="timeline-title" id="miercoles"></h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-item dot-primary vertical-timeline-element">
                                            <div>
                                                <span class="vertical-timeline-element-icon bounce-in" style="margin-top: 1%;"></span>
                                                <div class="vertical-timeline-element-content bounce-in">
                                                    <h4 class="timeline-title" id="jueves"></h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-item dot-info vertical-timeline-element">
                                            <div>
                                                <span class="vertical-timeline-element-icon bounce-in" style="margin-top: 1%;"></span>
                                                <div class="vertical-timeline-element-content bounce-in">
                                                    <h4 class="timeline-title" id="viernes"></h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-item dot-dark vertical-timeline-element">
                                            <div>
                                                <span class="vertical-timeline-element-icon bounce-in" style="margin-top: 1%;"></span>
                                                <div class="vertical-timeline-element-content bounce-in">
                                                  <h4 class="timeline-title" id="sabado"></h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-item dot-danger vertical-timeline-element">
                                            <div>
                                                <span class="vertical-timeline-element-icon bounce-in" style="margin-top: 1%;"></span>
                                                <div class="vertical-timeline-element-content bounce-in">
                                                    <h4 class="timeline-title" id="domingo"></h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-item dot-success vertical-timeline-element">
                                            <div>
                                                <span class="vertical-timeline-element-icon bounce-in" style="margin-top: 1%;"></span>
                                                <div class="vertical-timeline-element-content bounce-in">
                                                    <h4 class="timeline-title">
                                                        <div class="badge badge-danger ml-2" style="margin-top:1%;">COMENTARIO</div>
                                                    </h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-item dot-success vertical-timeline-element">
                                            <div>
                                                <span class="vertical-timeline-element-icon bounce-in" style="margin-top: 1%;"></span>
                                                <div class="vertical-timeline-element-content bounce-in">
                                                    <h4 class="timeline-title" id="comment">
                                                    </h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
<script type="text/javascript" src='home/js/home52.js?v=0.001'></script>   
<script src="add/vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>
<script src="add/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js">
</script>
<script src="add/vendors/toastr/toastr.min.js"></script>
<script src="add/vendors/bower_components/tinymce/tinymce.min.js"></script>
<script src="add/dist/js/tinymce-data.js"></script>
<style type="text/css">
    #mceu_16-body{
        display: none !important;
    }
</style>

        <script >
            $('#number').keyup(function(){
                var number = $('#number').val();
                var por = {1:0.01,2:0.02,3:0.03,4:0.04,5:0.05,6:0.06,7:0.07,8:0.08,9:0.09,10:0.10,11:0.11,12:0.12,13:0.13,14:0.14,15:0.15,16:0.16,17:0.17,18:0.18,19:0.19,20:0.20,21:0.21,22:0.22,23:0.23,24:0.24,25:0.25};
                var mostrar = "";
                for(var clave in por) {
                    result = number * por[clave];
                    result = parseFloat(number) - parseFloat(result);
                    mostrar += clave +" - "+result.toFixed(2)+"<br><br>";
                }
                
                $("#result").html(mostrar)
            });
        </script>