<div class="ui-theme-settings">
        <button type="button"  id="TooltipDemo" class="btn-open-options btn btn-warning" style="display: none;">
            <i class="fa  fa-times fa-w-16 fa-2x"></i>
        </button>
        <button type="button"  id="TooltipDemo1" class=" btn btn-success" style="bottom: 120px;">
            <i class="fa fa-floppy-o fa-w-16 fa-2x" ></i>
        </button>
        <div class="theme-settings__inner">
            <div class="scrollbar-container">
                <div class="theme-settings__options-wrapper">
                    <input type="hidden" name="redactor_id" id="redactor_id">
                    <div class="p-3" >
                        <ul class="list-group">
                            <li><br></li>
                            
                            <div id="solo_pagar" class="panel_li" style="display: none;">
                                <li>HACER UN PAGO A: <b class="redactor" style="margin-top: 35%;"></b><br><br>
                                    <div class="form-group">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-4">
                                            <span class="label label-primary" style="float: right;padding: 14px;font-size: 14px;border-radius: 0px;text-transform: capitalize;font-weight: 600;">MONTO A PAGAR: <span id='suma'></span> <i class="fa fa-dollar"></i></span>
                                        </div>
                                        
                                        <div class="col-md-2" style="margin-bottom: 15%;"></div>
                                        
                                    </div><!-- form-group -->
                                </li>
                            </div>
                            <li id="mensaje_nota" class="panel_li" style="display: none;">
                                
                            </li><br>
                            <li>
                                <button type="button" onclick="atraspagar()"   class="btn btn-warning pd-x-20" >ATRAS</button>
                                <button onclick="pagar()" id="bot_pagar" class="btn bot btn-success pd-x-20" style="display: none;">PAGAR</button>
                                
                            </li>
                            <li><br><br></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
</div>

<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading ">
                    <div class="page-title-icon">
                        <i class="pe-7s-diamond icon-gradient bg-warm-flame">
                        </i>
                    </div>
                    <div class="">PAGOS
                        <div class="page-title-subheading">PANEL DE PAGOS
                        </div>
                    </div>
                </div>   
            </div>
        </div>
        
        <div class="tabs-animation col-md-12">
            <div id="home" class="view_user">
                <div class="row view_home" id="asigned">
                    <div class="col-md-12">
                        <div class="panel panel-default card-view">
                            <div class="panel-heading">
                                <div class="card">
                                    
                                </div><!-- card -->
                        
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-wrapper in">
                                <div class="panel-body">
                                    <div class="table-wrap" id="mostrar">
                                        <div class="table-responsive" id="table">
                                            <table id="Table1" class="table table-hover display  pb-30" >
                                                <thead>
                                                    <tr>
                                                        <th style="text-align: center;">Usuario</th>
                                                        <th style="text-align: center;">MONTO</th>
                                                        <th style="text-align: center;">pagar</th>
                                                    </tr>
                                                </thead>
                                                <tfoot>
                                                    <tr>
                                                        <th style="text-align: center;">Usuario</th>
                                                        <th style="text-align: center;">MONTO</th>
                                                        <th style="text-align: center;">pagar</th>
                                                    </tr>
                                                </tfoot>
                                                <tbody  id="pagar">
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>           
                    </div>
                </div>
                
                  
            </div>
        </div>
<script type="text/javascript" src='pagar/js/pagar.js?v=0.002'></script>
<script src="add/vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>
<script src="add/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js">
</script>
<script src="add/vendors/toastr/toastr.min.js"></script>
<script src="add/vendors/bower_components/tinymce/tinymce.min.js"></script>
<script src="add/dist/js/tinymce-data.js"></script>
<style type="text/css">
    #mceu_16-body{
        display: none !important;
    }
</style>
