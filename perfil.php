
<link href="add/vendors/bower_components/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css"/>
<div class="app-main__outer">
    <div class="app-main__inner" style="padding-left: 10px;">
        <div class="app-page-title" style="margin-bottom: 0px;">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="pe-7s-diamond icon-gradient bg-warm-flame">
                        </i>
                    </div>
                    <div>MI PERFIL
                        <div class="page-title-subheading">AQUÍ PUEDES VER O ACTUALIZAR TU INFORMACIÓN PERSONAL
                        </div>
                    </div>
                </div>   
            </div>
        </div>
        <div class="tabs-animation">            
            <div  class="view_user">
                <div class="col-lg-12 col-md-12">
                    <div class="main-card mb-3 card">
                        <div class="card-body">
                              <div id="perfil_list" class="view_user">
                                <div class="col-md-12 col-lg-12 col-xl-12">
                                    <div class="card-shadow-primary profile-responsive card-border mb-3 card">
                                        <div class="dropdown-menu-header">
                                            <div class="dropdown-menu-header-inner bg-danger">

                                                <div class="menu-header-image" style="background-image: url('assets/images/dropdown-header/abstract1.jpg');"></div>
                                                <div class="menu-header-content btn-pane-right">
                                                    <div class="avatar-icon-wrapper mr-2 avatar-icon-xl">
                                                        <div class="avatar-icon"><img
                                                                src="images/<?php echo imgPerfil(); ?>" class="img-perfil" style="<?php echo imgPerfilStyle(); ?>"
                                                                alt="Perfil photo">
                                                        </div>
                                                    </div>
                                                    <div><h5 class="menu-header-subtitle"><?php echo nombreUsuario(); ?></h5><h6 class="menu-header-title" >NIVEL: <?php echo nivel(idUsuario()); ?></h6></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <form class="" >
                                <div class="form-group">
                                    <input type="hidden" name="perfil" id="perfil">
                                    <div class="col-lg-6 col-md-12 col-sm-12">
                                        <label > <b> NOMBRE</b></label>
                                        <input type="text" id='nombre' readonly="readonly" onblur="mayusculas(this)" class="form-control" placeholder="NOMBRE">
                                    </div>                    
                                    <div class="col-lg-6 col-md-12 col-sm-12">
                                        <label > <b> APELLIDOS</b></label>
                                        <input type="text" id='apellido' readonly="readonly" class="form-control" onblur="mayusculas(this)" placeholder="APELLIDO">
                                    </div>
                                </div><!-- form-group --><br><br><br><br>    
                                <div class="form-group">
                                    <div class="col-lg-6 col-md-12 col-sm-12">
                                        <br>
                                        <label > <b> CORREO ELECTRÓNICO</b></label>
                                        <input name="email" id="correo" placeholder="email de usuario" type="email" readonly="" class="form-control">
                                        <br>
                                        <div class="col-lg-4 col-md-12 col-sm-12">
                                            <label > <b> NRO</b></label>
                                            <input name="col" id='cod' placeholder="+58" type="text" class="form-control">
                                        </div>
                                        <div class="col-lg-8 col-md-12 col-sm-12">
                                            <label ><b>DE CONTACTO</b></label>
                                            <input name="tel" id='tel' placeholder="4121234567" type="text" class="form-control">
                                        </div>
                                        <br><br><br><br>
                                        <label > <b> CONTRASEÑA</b></label>
                                        <input name="password" placeholder="CONTRASEÑA NUEVA" id='password' type="password" class="form-control">
                                        <br>
                                        <label > <b> VALIDAR CONTRASEÑA</b></label>
                                        <input name="password" id='password2' placeholder="VALIDAR CONTRASEÑA NUEVA" type="password" class="form-control">
                                        <br>
                                        <label ><b> PALABRAS CADA 24 HORAS</b></label>
                                        <input name="palabras" placeholder="PALABRAS/24H" id='palabras' type="text" class="form-control">
                                    </div>
                                    <div class="col-lg-6" style="margin-top: 5px;">
                                        <br>
                                        <div class="main-card mb-3 card">
                                            <div class="card-body"><h5 class="card-title">INGRESE TUS PASARELAS DE PAGO Y ORDENE</h5>
                                               <table summary="Tabla editable para sumar filas y columnas" id="tabla"class="mb-0 table">
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </form>  
                            <br><br>  
                            <div class="col-md-12">
                                <div style="margin-left: 2%;" class="col-md-6">
                                    <br><br>
                                    <label><b>FOTO DE PERFIL </b></label><br>
                                    <?php $status= statusPerfil();
                                    if ($status) {
                                    ?>
                                    <form method="post" action="#" enctype="multipart/form-data">
                                       <div class="avatar-icon-wrapper mr-2 avatar-icon-xl ">
                                            <div class="text-center">
                                                 <div class="avatar-icon-wrapper mr-2 avatar-icon-xl ">
                                                    <div class="avatar-icon">
                                                        <div class="preview d-inline-block"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php 
                                    } else{?> 
                                        <div class="avatar-icon-wrapper mr-2 avatar-icon-xl">
                                            <div class="avatar-icon"><img
                                                    src="images/<?php echo imgPerfil(); ?>" class="img-perfil" style="<?php echo imgPerfilStyle(); ?>"
                                                    alt="Perfil photo">
                                            </div>
                                        </div>

                                    <?php } ?>
                                        <div class="card-body" style="">
                                            <div class="form-group">
                                                <input type="file" class="form-control-file" name="image" id="image">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <?php echo perfilHidden(); ?>
                                <?php 
                                    $status= statusPerfil();
                                    if ($status) {
                                    ?>
                                        <div class="col-md-5">
                                           <div class="main-card mb-3 card">
                                                <div class="card-body">
                                                    <div class="card-title">EDITAR IMÁGEN AQUÍ</div>
                                                    <div>
                                                        <img class="card-img-top" src="images/<?php echo imgPerfil(); ?>" id="image-crop" alt="Picture">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php 
                                } ?>
                                
                            </div>
                            
                        </div>
                        <div class="col-md-12 col-sm-12" style="float: right;margin-bottom:5%;">
                            <div class="col-md-9 col-sm-12"></div>
                            <div class="col-md-3 col-sm-12">
                                <a href="javascript:void(0);" class="btn btn-success btn-sm" onclick="  actualizarPerfil(0)"> ACTUALIZAR</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>   
    </div>    
</div> 
<script type="text/javascript" src='perfil/js/perfil13.js?v=0.005'></script>
<script src="add/vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>
<script src="add/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js">
</script>
<script src="add/vendors/toastr/toastr.min.js"></script>
<script src="add/vendors/bower_components/dropzone/dist/dropzone.js"></script>
<script src="add/dist/js/dropzone-data1.js"></script>
