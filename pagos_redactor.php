
<link href="add/vendors/bower_components/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css"/>
<div class="app-main__outer">
    <div class="app-main__inner" style="padding-left: 10px;">
        <div class="app-page-title" style="margin-bottom: 0px;">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="pe-7s-diamond icon-gradient bg-warm-flame">
                        </i>
                    </div>
                    <div>MIS PAGOS
                        <div class="page-title-subheading">AQUÍ PUEDES VER LOS PAGOS 
                        </div>
                    </div>
                </div>   
            </div>
        </div>
        <div class="tabs-animation">            
            <div  class="view_user">
                <div class="col-lg-12 col-md-12">
                    <div class="main-card mb-3 card">
                        <div class="card-body">
                              <div id="perfil_list" class="view_user">
                                <div class="col-md-12 col-lg-12 col-xl-12">
                                    <div class="card-shadow-primary profile-responsive card-border mb-3 card">
                                        <div class="dropdown-menu-header">
                                            <div class="dropdown-menu-header-inner bg-danger">

                                                <div class="menu-header-image" style="background-image: url('assets/images/dropdown-header/abstract1.jpg');"></div>
                                                <div class="menu-header-content btn-pane-right">
                                                    <div class="avatar-icon-wrapper mr-2 avatar-icon-xl">
                                                        <div class="avatar-icon"><img
                                                                src="images/<?php echo imgPerfil(); ?>" class="img-perfil"
                                                                alt="Perfil photo">
                                                        </div>
                                                    </div>
                                                    <div><h5 class="menu-header-subtitle"><?php echo nombreUsuario(); ?></h5><h6 class="menu-header-title" >NIVEL: <?php echo nivel(idUsuario()); ?></h6></div>
                                                    <div class="menu-header-btn-pane">
                                                        <button class="btn-icon btn btn-primary btn-sm" >ÚLTIMOS PAGOS</button>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <ul class="list-group list-group-flush" id="ultimos_pagos">
                                        </ul>
                                    </div>
                                </div>
                            </div>                                                           
                            <br><br><br>
                        </div>
                    </div>
                </div>
            </div> 
        </div>   
    </div>    
</div> 
<script type="text/javascript" src='pagar/js/pagar.js?v=0.002'></script>
<script src="add/vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>
<script src="add/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js">
</script>
<script src="add/vendors/toastr/toastr.min.js"></script>
<script src="add/vendors/bower_components/dropzone/dist/dropzone.js"></script>
<script src="add/dist/js/dropzone-data1.js"></script>
