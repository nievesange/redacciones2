<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
include('../../../include/coneccion.php');
include('../../../include/funciones.php');

$fechaToday = date('Y-m-d H:i:s');
$z = 1;
$id_logIn  = idUsuario();
$tipo_usuario = tipoUsuario();
$valor = utf8_decode($_POST['valor']);


$query="SELECT id,nombre, apellido, idtipousuario, status FROM usuario WHERE ( nombre LIKE '%$valor%' or apellido LIKE '%$valor%' ) AND idtipousuario > 2 and id != 1 and activo = 1 ORDER BY idtipousuario DESC ";

$res=$conn->query($query);
if($res->num_rows > 0){
    while($fila=$res->fetch_array()){
        $id = $fila['id'];
        $nombre = $fila['nombre'];
        $apellido = $fila['apellido'];
        $idtipousuario = $fila['idtipousuario'];
        $status = $fila['status'];
        $clase="";
        
        if ($idtipousuario==3) {
            $tipo="Redactor";
        }else if ($idtipousuario==4) {
            $tipo="Revisor";
        }else{
            $tipo = "";
        }
        $query1 = "SELECT id from chat_usuario where id_usuario_envia='$id' AND id_usuario_receptor='$id_logIn' AND status_mensaje = 1 order by id desc limit 1";
        $res1=$conn->query($query1);
        if($res1->num_rows>0){
            $clase = "icon text-danger icon-anim-pulse ion-android-notifications";             
        }

        $online = statusUsuarioChat($id);
        if ($online) {
            $online = "success";
        }else{
            $online = "warning";
        }

        $completo =  '<li class="nav-item">
            <button type="button" tabindex="0" class="dropdown-item" onclick="usuario_seleccionado('.$id.')">
                <div class="widget-content p-0">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left mr-3 '.$clase.'" style="font-size: 8px;">
                            <div class="avatar-icon-wrapper mr-2 avatar-icon-xm">
                                <div class="badge badge-bottom badge-'.$online.' badge-dot badge-dot-lg"></div>
                                <div class="avatar-icon">
                                   <img width="42" class="card-img-top img-perfil"  src="images/'.imgPerfilId($id).'" alt="" style="'.imgPerfilStyleId($id).'">
                                </div>
                            </div>
                        </div>
                        <div class="widget-content-left">
                            <div class="widget-heading" style="width: 200px;">'.$nombre.' '.$apellido.' </div>
                                <div class="widget-subheading">'.$tipo.'</div>
                        </div>
                    </div>
                </div>
            </button>
        </li>'; 
      
            $script = '';

            $productos[$id]= $completo.$script;

        $z++;
    
    }
}
if(empty($productos)){
    echo "<tr><td colspan=7 class='text-center text-muted'>SIN RESULTADOS..</td></tr> ";
}else{

    foreach($productos as $producto)
    {
        echo $producto;
    }
}
?>
