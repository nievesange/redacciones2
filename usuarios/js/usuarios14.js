function loadUsuarios(){
    var id=1;
    var url="usuarios/queries/usuarios/load.php";
    $.post(url,{id:id},
    function (response){
        //json = eval('('+response+')');
        $("#usuarios").html(response);
    });

}

function addUsuario(){
	var crear_nombre       = $('#crear_nombre').val();
	var crear_apellido     = $('#crear_apellido').val();
	var crear_correo       = $('#crear_correo').val();
	var crear_password     = $('#crear_password').val(); 
	var crear_password2    = $('#crear_password2').val(); 
	var crear_tipo_usuario = $('#crear_tipo_usuario').val(); 
    var crear_status       = $('#crear_status').val(); 
    var crear_pagos        = $('#pagos').val();
    var result = inputNumeros2(crear_pagos); 

	var correo_valido = validaCorreo("crear_correo");
  
    var crear_pagos = crear_pagos.replace(",", ".");

	if(crear_nombre==""){//&& contraseña =="" && contraseña1 == "" && cargo == 0){
        toastr["error"]("", "DEBE INGRESAR EL NOMBRE DE USUARIO");
        $("#crear_nombre").focus();
        return false;
    }else if(crear_apellido==""){
            toastr["error"]("", "DEBE INGRESAR EL APELLIDO DEL USUARIO");
            $("#crear_apellido").focus();
            return false;
    }else if(crear_pagos==""){
            toastr["error"]("", "DEBE INGRESAR EL PAGO POR 100 PALABRAS");
            $("#crear_apellido").focus();
            return false;
    }else if(result == 1){
        toastr["error"]("", "SOLO PUEDE INGRESAR NÚMEROS Y USAR EL (.) PARA SEPARAR DECIMALES");
        $("#palabras").focus();
        return false;
    }else if(crear_correo==""){
        toastr["error"]("", "DEBE INGRESAR EL CORREO ELECTRONICO");
        $("#crear_correo").focus();
        return false;
    }else if(correo_valido==false){
        //$("#crear_correo").focus();
        return false;
    }else if(crear_password==""){
        toastr["error"]("", "DEBE INGRESAR LA CONTRASEÑA");
        $("#crear_password").focus();
        return false;
    }else if(crear_password2==""){
        toastr["error"]("", "DEBE CONFIRMAR LA CONTRASEÑA");
        $("#crear_password2").focus();
        return false;
    }else if(crear_tipo_usuario=="0"){
        toastr["error"]("", "DEBE SELECCIONAR EL TIPO DE USUARIO");
        $("#crear_tipo_usuario").focus();
        return false;
    }else{          
        if (crear_password != crear_password2) {
            toastr["error"]("", "LAS CONTRASEÑAS NO COINCIDEN");
            $("#crear_password2").focus();
            return false;
        }else{
            $.post("usuarios/queries/usuarios/create.php",{crear_pagos:crear_pagos,crear_nombre:crear_nombre,crear_apellido:crear_apellido,crear_correo:crear_correo,crear_password:crear_password,crear_tipo_usuario:crear_tipo_usuario,crear_status:crear_status},
                function(data){
                    if(data == 1){
                        toastr["success"]("", "SE AGREGO EL USUAURIO CON EXITO");                        
                        $('#crear_nombre').val("");
                        $('#crear_apellido').val("");
                        $('#crear_correo').val("");
                        $('#crear_password').val(""); 
                        $('#crear_password2').val(""); 
                        $('#pagos').val(""); 
                        $('#crear_status').val("1"); 
                        loadUsuarios();
                        atrasRedactores();
                    }else if(data == 3){
                        toastr["warning"]("", "EL CORREO YA SE ENCUENTRA REGISTRADO");
                    }else{
                        toastr["error"]("", "ERROR AL AGREGAR USUARIO");
                    }
                }
            );
        }
    }

}

loadUsuarios();
function actualizarUsuario() {
    var id = $('#usuario_id').val();
    var nombre = $('#modalEditar_nombre').val();
    var apellido = $('#modalEditar_apellido').val();
    var pagos  = $('#pagosEdit2').val();
    var editar_password = $('#editar_password').val();
    var editar_password2 = $('#editar_password2').val();
    var tipo_usuario = $('#modalEditar_tipo_usuario').val();
    var pagos = pagos.replace(",", ".");
    if (editar_password == "" || editar_password == null || editar_password == undefined) {
        editar_password = "0";
        editar_password2 = "0";
    }
    if(nombre == '' || apellido == ''){
        toastr["warning"]("", "ALGUNOS CAMPOS ESTAN VACIOS");
        return false;
    }else if(editar_password != editar_password2) {
        toastr["error"]("", "LAS CONTRASEÑAS NO COINCIDEN");
        $("#editar_password").focus();
        return false;
    }else{
            $.post("usuarios/queries/usuarios/update.php",{pagos:pagos,tipo_usuario:tipo_usuario,editar_password:editar_password,id:id,nombre:nombre,apellido:apellido},function(data){
                if(data == 1){
                    toastr["success"]("", "USUARIO ACTUALIZADO CON EXITO");
                    $('#editar_password').val("");
                    $('#editar_password2').val("");
                    $('#pagosEdit').val("");
                    $('#editar_password2').hide();
                    loadUsuarios();
                    atrasRedactores();
                }else{
                    toastr["error"]("", "EROR ACTUALIZADO USUARIO");
                }
            });

    }
}

$('#editar_password').keyup(function(){
    $('#editar_password2').show('slow')
    var opcionPass = $('#opcionPass').val(1);

});

$('#buscador_usuario').keyup(function(){
    var buscador_usuario = $('#buscador_usuario').val();
    loadClientesCustom(buscador_usuario);
});

function loadClientesCustom(valor){
    var url="usuarios/queries/usuarios/load_custom.php";
    $.post(url,{valor:valor},
    function (response){
        //json = eval('('+response+')');
        $("#usuarios").html(response);
    });

}

$('#TooltipDemo').click(function(){
    atrasRedactores2();
});

function add() {
    $(".panel_li").hide();
    $("#solo_add").show();
    $(".bot").hide();
    $("#bot_add").show();

  
    $('#TooltipDemo').trigger('click');
    $("#TooltipDemo").show('slow');
    $(".card-footer").css('opacity','0.1');
}


function atrasRedactores(){
    $('#TooltipDemo').trigger('click');
    $("#TooltipDemo").hide('slow');
    $('#editar_password').val("");
    $('#editar_password2').val("");
    $('#pagosEdit').val("");
    $('#editar_password2').hide();
    $('#crear_nombre').val("");
    $('#crear_apellido').val("");
    $('#crear_correo').val("");
    $('#crear_password').val(""); 
    $('#crear_password2').val(""); 
    $('#pagos').val(""); 
    $('#crear_status').val("1"); 

}

function atrasRedactores2(){
    $("#TooltipDemo").hide('slow');
    $('#editar_password').val("");
    $('#editar_password2').val("");
    $('#pagosEdit').val("");
    $('#editar_password2').hide();
    $('#crear_nombre').val("");
    $('#crear_apellido').val("");
    $('#crear_correo').val("");
    $('#crear_password').val(""); 
    $('#crear_password2').val(""); 
    $('#pagos').val(""); 
    $('#crear_status').val("1"); 
}

