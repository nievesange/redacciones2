<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
include('../../../include/coneccion.php');
include('../../../include/funciones.php');

$fechaToday = date('Y-m-d H:i:s');
$z = 1;

$query="SELECT * FROM usuario where activo = 1 and id != 1  order by id desc ";
$res=$conn->query($query);
if($res->num_rows>0)
{
    while($fila=$res->fetch_array())
    {
        $id_usuario = $fila['id'];
        $pago       = pagoId($id_usuario);
        $nombre     = $fila['nombre'];
        $apellido   = $fila['apellido'];
        $correo     = $fila['correo'];
        $createdate = $fila['createdate'];
        $idtipousuario = $fila['idtipousuario'];
        $completo = '<tr>
                            <td style="text-align: center;">'.ucwords($nombre).' '.ucwords($apellido).'</td>
                            <td style="text-align: center;">'.strtoupper($correo).'</td>
                            <td style="text-align: center;">'.tiposUsuario($idtipousuario).'</td>
                            <td style="text-align: center;">'.$pago.'$</td>
                            
                            <td class="text-center" style="text-align: center;width:15%; " >
                                <div class="btn-toolbar">
                                    <div class="btn-group">
                                        <button id="hist'.$id_usuario.'" data-toggle="tooltip" data-placement="top" data-original-title="EDITAR USUARIO" type="button" class="btn btn-success btn-xs"><i class="fa fa-pencil-square"></i></button>
                                        <button id="delt_'.$id_usuario.'" data-toggle="tooltip" data-placement="top" data-original-title="ELIMINAR USUARIO" type="button" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                                    </div>
                                </div>
                            </td>
                        </tr>';
      
            $script = "<script type='text/javascript'>
                $('[data-toggle=\"tooltip\"]').tooltip();
                $('#hist".$id_usuario."').click(function(){
                    var id = ".$id_usuario.";
                    $.post('usuarios/queries/usuarios/search.php',{id:id},function(data){
                        var abc = jQuery.parseJSON(data);
                        $('#usuario_id').val(".$id_usuario.");

                        $('#pagosEdit2').val(abc.pago);
                        $('#modalEditar_nombre').val(abc.nombre);
                        $('#modalEditar_apellido').val(abc.apellido);
                        $('#modalEditar_correo').val(abc.correo);
                        $('#modalEditar_tipo_usuario').val(abc.idtipousuario);
                        

                        $('.panel_li').hide();
                        $('#solo_act').show();
                        $('.bot').hide();
                        $('#bot_act').show();                        
                        $('#TooltipDemo').trigger('click');
                        $('#TooltipDemo').show('slow');
                    });
                                        
                });
                $('#delt_".$id_usuario."').click(function(){
                    var limite = 0;
                    var id = ".$id_usuario.";
                        $.post('usuarios/queries/usuarios/search.php',
                        {id:id},
                        function(data){
                            var abc = jQuery.parseJSON(data);

                            swal({
                              title: 'ESTA SEGURO?',
                              text: 'QUE DESEA ELIMINAR A '+abc.nombre+' '+ abc.apellido,
                              type: 'warning',
                              showCancelButton: true,
                              confirmButtonColor: '#DD6B55',
                              confirmButtonText: 'SI, ELIMINAR!',
                              cancelButtonText: 'NO',
                              closeOnConfirm: true,
                              closeOnCancel: true
                            },
                            function(isConfirm){
                              if (isConfirm) {

                                $.post('usuarios/queries/usuarios/delete.php',
                                {id:id},
                                function(data){
                                    if(data == 1){
                                        toastr['success']('', 'EE ELIMINO CON EXITO');
                                        loadUsuarios();
                                    }else{
                                        toastr['error']('', 'NO SE PUDO ELIMINAR..');
                                    }
                                });

                              }
                            });


                        });

                });
            </script>";

            $productos[$id_usuario]= $completo.$script;
        $z++;

    }
}

if(empty($productos))
{
    echo "<tr><td colspan=7 class='text-center text-muted'>NO SE ENCONTRARON RESULTADOS</td></tr>";
}else{

    foreach($productos as $producto)
    {
        echo $producto;
    }
}
?>
