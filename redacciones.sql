-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 01-07-2019 a las 10:51:32
-- Versión del servidor: 10.1.38-MariaDB-0+deb9u1
-- Versión de PHP: 7.1.27-1+0~20190307202204.14+stretch~1.gbp7163d5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `buyatext`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulos`
--

CREATE TABLE `articulos` (
  `articulo_id` int(11) NOT NULL,
  `articulo_ref` varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `articulo_redactor` int(11) NOT NULL,
  `articulo_tiempo` int(11) NOT NULL,
  `articulo_descripcion` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `articulo_url` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `articulo_palabras` int(11) NOT NULL,
  `articulo_date` varchar(30) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `articulo_entrega` varchar(30) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `articulo_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `chat_usuario`
--

CREATE TABLE `chat_usuario` (
  `id` int(11) NOT NULL,
  `id_usuario_envia` int(11) NOT NULL,
  `id_usuario_receptor` int(11) NOT NULL,
  `mensaje` text CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `fecha_mensaje` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status_mensaje` int(11) NOT NULL,
  `tipo_user` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `messages`
--

CREATE TABLE `messages` (
  `id_message` int(11) NOT NULL,
  `id_redactor` int(11) NOT NULL,
  `text_message` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `date_message` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status_message` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos`
--

CREATE TABLE `pagos` (
  `id_pago` int(11) NOT NULL,
  `id_redactor` int(11) NOT NULL,
  `monto_pago` float NOT NULL,
  `fecha_pago` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfil_redactor`
--

CREATE TABLE `perfil_redactor` (
  `id_perfil` int(11) NOT NULL,
  `id_redactor` int(11) NOT NULL,
  `monto_acumulado` varchar(5) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  `monto_total` varchar(5) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  `status_perfil` int(11) NOT NULL,
  `ultima_conexion` varchar(30) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `ultimo_pago` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfil_usuario`
--

CREATE TABLE `perfil_usuario` (
  `id_perfil` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `cod` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `tel` varchar(12) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `horas` int(11) NOT NULL,
  `img` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'avatar.jpg',
  `style` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `status_perfil` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rechazos`
--

CREATE TABLE `rechazos` (
  `id_rechazo` int(11) NOT NULL,
  `id_articulo` int(11) NOT NULL,
  `contenido_rechazo` text CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `status_rechazo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `soporte_comentarios`
--

CREATE TABLE `soporte_comentarios` (
  `id_comentario` int(11) NOT NULL,
  `id_soporte` int(11) NOT NULL,
  `id_autor` int(11) NOT NULL,
  `comentario` text CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `chechk_date_comentario` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '0',
  `comentario_date` varchar(30) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `soporte_tickects`
--

CREATE TABLE `soporte_tickects` (
  `id_soporte` int(11) NOT NULL,
  `id_articulo` int(11) NOT NULL,
  `id_redactor` int(11) NOT NULL,
  `asunto` text CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `contenido` text CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `tickects_date` varchar(30) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `chechk_date_tickets` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `soporte_tickects`
--

INSERT INTO `soporte_tickects` (`id_soporte`, `id_articulo`, `id_redactor`, `asunto`, `contenido`, `tickects_date`, `chechk_date_tickets`, `status`) VALUES
(30, 9, 1, 'FGRE', '<p>BF</p>', '2019-05-06 18:49:38', '0', 0),
(31, 9, 1, 'GNGF', '<p>NGFFGBNCGF</p>', '2019-05-06 18:50:03', '0', 0),
(32, 9, 1, 'gfdg', '<p>ghtrbgf</p>', '2019-05-06 18:53:47', '0', 0),
(33, 9, 1, 'bgrfg', '<p>regertwer</p>', '2019-05-06 18:54:38', '0', 0),
(34, 12, 1, 'bfg', '<p>hgggr</p>', '2019-05-06 18:55:17', '0', 0),
(35, 12, 1, 'gfer', '<p>rttrr</p>', '2019-05-06 18:55:37', '0', 0),
(36, 12, 1, 'hmge', '<p>fgnmgfd</p>', '2019-05-06 18:56:39', '0', 0),
(37, 9, 1, 'jhgf', '<p>gfdg</p>', '2019-05-06 18:57:19', '0', 0),
(38, 12, 1, 'rgr', '<p>gfgg</p>', '2019-05-06 18:57:26', '0', 0),
(39, 9, 1, 'bgf', '<p>gbfd</p>', '2019-05-06 19:07:22', '0', 0),
(40, 9, 1, 'jmhngf', '<p>hngfd</p>', '2019-05-06 19:08:02', '0', 0),
(41, 9, 1, ' ngfd', '<p>ngfd</p>', '2019-05-06 19:08:37', '0', 0),
(42, 12, 1, 'mjhngbv', '<p><strong>nbvcx</strong></p>', '2019-05-06 19:11:18', '0', 0),
(43, 0, 85, 'PAGO POR 18.91$', 'Ha recibido un pago por un monto de 18.91$', '2019-05-13 08:15:06', '0', 1),
(44, 0, 85, 'hola', '<p>vdsasdvf</p>', '2019-05-13 08:44:07', '0', 2),
(45, 0, 85, 'hola', '<p>hollaa</p>', '2019-05-13 09:02:19', '2019-05-13 21:31:03', 0),
(46, 0, 85, 'PAGO POR 24.60$', 'Ha recibido un pago por un monto de 24.60$', '2019-05-13 09:04:40', '2019-05-13 21:30:57', 1),
(47, 0, 85, 'hola pueblo', '<p>ejemplo de prueba</p>', '2019-05-13 21:23:23', '2019-05-13 21:30:52', 1),
(48, 0, 84, 'hola pueblo', '<p>ejemplo de prueba</p>', '2019-05-13 21:23:23', '1', 1),
(49, 0, 85, 'ejemplo de ticket', '<p>bgfdvbg</p>', '2019-05-13 21:31:52', '2019-05-13 21:32:02', 1),
(50, 0, 84, 'ejemplo de ticket', '<p>bgfdvbg</p>', '2019-05-13 21:31:52', '1', 1),
(51, 10, 85, 'gfdsad', '<p>bdsacxvdfsasdcfdse</p>', '2019-05-14 22:09:04', '2019-05-16 05:15:15', 2),
(52, 17, 85, 'tPRUEBA', '<p>dfrewqSCVFGTRE</p>', '2019-05-14 22:14:03', '2019-05-16 05:15:07', 2),
(53, 17, 85, 'tPRUEBA', '<p>dfrewqSCVFGTRE</p>', '2019-05-14 22:14:03', '2019-05-16 05:15:11', 2),
(54, 10, 85, 'VFERTG', '<p>FERTFD</p>', '2019-05-14 22:14:48', '2019-05-15 19:37:02', 2),
(55, 17, 85, 'FDW', '<p>FDEEE</p>', '2019-05-14 22:15:00', '2019-05-15 19:37:00', 2),
(56, 0, 85, 'PAGO POR 0.62$', 'Ha recibido un pago por un monto de 0.62$', '2019-05-15 17:28:32', '2019-05-15 17:28:46', 1),
(57, 0, 85, 'PAGO POR 40.82$', 'Ha recibido un pago por un monto de 40.82$', '2019-05-15 17:30:48', '2019-05-15 17:33:20', 1),
(58, 10, 85, 'pregunta1', '<p>gbdfewfb</p>', '2019-05-15 17:37:32', '2019-05-15 17:37:41', 2),
(59, 0, 85, 'general', '<p>general</p>', '2019-05-15 19:32:22', '2019-05-15 19:37:10', 1),
(60, 0, 84, 'general', '<p>general</p>', '2019-05-15 19:32:22', '1', 1),
(61, 0, 87, 'general', '<p>general</p>', '2019-05-15 19:32:22', '1', 1),
(62, 0, 85, 'hola', '<p>hola</p>', '2019-05-15 19:37:34', '2019-05-15 19:37:42', 2),
(63, 0, 85, 'fdvgfr', '<p>efgrew</p>', '2019-05-15 19:38:52', '2019-05-15 19:38:59', 2),
(64, 0, 85, 'mnbf', '<p>nhgfe</p>', '2019-05-15 19:40:53', '2019-05-15 19:41:05', 2),
(65, 0, 85, 'PAGO POR 20.41$', 'Ha recibido un pago por un monto de 20.41$', '2019-05-31 22:42:59', '1', 1),
(66, 0, 85, 'PAGO POR 0.20$', 'Ha recibido un pago por un monto de 0.20$', '2019-05-31 22:53:35', '1', 1),
(67, 0, 85, 'PAGO POR 0.10$', 'Ha recibido un pago por un monto de 0.10$', '2019-05-31 23:02:27', '1', 1),
(68, 0, 85, 'PAGO POR 0.40$ A TRAVÉS DE: 1', 'Ha recibido un pago por un monto de 0.40$', '2019-06-17 15:40:12', '1', 1),
(69, 0, 85, 'PAGO POR 4.40$ A TRAVÉS DE: undefined', 'Ha recibido un pago por un monto de 4.40$', '2019-06-17 15:43:10', '1', 1),
(70, 0, 85, 'PAGO POR 5.50$ A TRAVÉS DE: Skrill', 'Ha recibido un pago por un monto de 5.50$', '2019-06-17 15:45:30', '2019-06-17 15:45:45', 1),
(71, 0, 85, 'PAGO POR 1.20$ A TRAVÉS DE: TRANSFERWISE', 'Ha recibido un pago por un monto de 1.20$', '2019-06-17 15:59:42', '1', 1),
(72, 0, 85, 'PAGO POR 0.50$ A TRAVÉS DE: TRANSFERWISE', 'Ha recibido un pago por un monto de 0.50$', '2019-06-17 16:42:07', '1', 1),
(73, 0, 85, 'PAGO POR 0.60$ A TRAVÉS DE: PAYPAL', 'Ha recibido un pago por un monto de 0.60$', '2019-06-17 16:46:28', '2019-06-17 16:46:38', 2),
(74, 0, 88, 'PAGO POR 10$ A TRAVÉS DE: undefined', 'Ha recibido un pago por un monto de 10$', '2019-06-17 19:28:23', '1', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `apellido` varchar(255) DEFAULT NULL,
  `correo` varchar(255) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `correo_paypal` varchar(155) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `idtipousuario` int(11) DEFAULT NULL,
  `createdate` varchar(30) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `activo` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `nombre`, `apellido`, `correo`, `pass`, `correo_paypal`, `idtipousuario`, `createdate`, `status`, `activo`) VALUES
(1, 'EDGAR', 'NIEVES', 'edgardo.2903@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', '', 1, '2017-11-27 17:30:44', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_pago`
--

CREATE TABLE `usuario_pago` (
  `id_usuario_pago` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `correo_paypal` varchar(255) DEFAULT NULL,
  `nivel_paypal` int(11) NOT NULL,
  `correo_skrill` varchar(255) DEFAULT NULL,
  `nivel_skrill` int(11) NOT NULL,
  `correo_airtm` varchar(255) DEFAULT NULL,
  `nivel_airtm` int(11) NOT NULL,
  `correo_uphold` varchar(255) DEFAULT NULL,
  `nivel_uphold` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_tipo`
--

CREATE TABLE `usuario_tipo` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `createdate` varchar(30) DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario_tipo`
--

INSERT INTO `usuario_tipo` (`id`, `nombre`, `createdate`, `status`) VALUES
(1, 'SUPERADMIN', '2017-11-14', 1),
(2, 'ADMIN', '2017-11-14', 1),
(3, 'REDACTOR', '2017-11-14', 1),
(4, 'REVISOR', '2017-11-14', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `valoraciones_redactor`
--

CREATE TABLE `valoraciones_redactor` (
  `id_valoraciones` int(11) NOT NULL,
  `redactor_id` int(11) NOT NULL,
  `articulo_id` int(11) NOT NULL,
  `numero` int(11) NOT NULL,
  `comment` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `opc1` int(11) NOT NULL DEFAULT '0',
  `opc2` int(11) NOT NULL DEFAULT '0',
  `opc3` int(11) NOT NULL DEFAULT '0',
  `opc4` int(11) NOT NULL DEFAULT '0',
  `opc5` int(11) NOT NULL DEFAULT '0',
  `opc6` int(11) NOT NULL DEFAULT '0',
  `opc7` int(11) NOT NULL DEFAULT '0',
  `date_valoracion` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `articulos`
--
ALTER TABLE `articulos`
  ADD PRIMARY KEY (`articulo_id`);

--
-- Indices de la tabla `chat_usuario`
--
ALTER TABLE `chat_usuario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id_message`);

--
-- Indices de la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD PRIMARY KEY (`id_pago`);

--
-- Indices de la tabla `perfil_redactor`
--
ALTER TABLE `perfil_redactor`
  ADD PRIMARY KEY (`id_perfil`);

--
-- Indices de la tabla `perfil_usuario`
--
ALTER TABLE `perfil_usuario`
  ADD PRIMARY KEY (`id_perfil`);

--
-- Indices de la tabla `rechazos`
--
ALTER TABLE `rechazos`
  ADD PRIMARY KEY (`id_rechazo`);

--
-- Indices de la tabla `soporte_comentarios`
--
ALTER TABLE `soporte_comentarios`
  ADD PRIMARY KEY (`id_comentario`);

--
-- Indices de la tabla `soporte_tickects`
--
ALTER TABLE `soporte_tickects`
  ADD PRIMARY KEY (`id_soporte`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario_pago`
--
ALTER TABLE `usuario_pago`
  ADD PRIMARY KEY (`id_usuario_pago`);

--
-- Indices de la tabla `usuario_tipo`
--
ALTER TABLE `usuario_tipo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `valoraciones_redactor`
--
ALTER TABLE `valoraciones_redactor`
  ADD PRIMARY KEY (`id_valoraciones`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `articulos`
--
ALTER TABLE `articulos`
  MODIFY `articulo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT de la tabla `chat_usuario`
--
ALTER TABLE `chat_usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=255;
--
-- AUTO_INCREMENT de la tabla `messages`
--
ALTER TABLE `messages`
  MODIFY `id_message` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT de la tabla `pagos`
--
ALTER TABLE `pagos`
  MODIFY `id_pago` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `perfil_redactor`
--
ALTER TABLE `perfil_redactor`
  MODIFY `id_perfil` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `perfil_usuario`
--
ALTER TABLE `perfil_usuario`
  MODIFY `id_perfil` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `rechazos`
--
ALTER TABLE `rechazos`
  MODIFY `id_rechazo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `soporte_comentarios`
--
ALTER TABLE `soporte_comentarios`
  MODIFY `id_comentario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `soporte_tickects`
--
ALTER TABLE `soporte_tickects`
  MODIFY `id_soporte` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;
--
-- AUTO_INCREMENT de la tabla `usuario_pago`
--
ALTER TABLE `usuario_pago`
  MODIFY `id_usuario_pago` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `usuario_tipo`
--
ALTER TABLE `usuario_tipo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `valoraciones_redactor`
--
ALTER TABLE `valoraciones_redactor`
  MODIFY `id_valoraciones` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
