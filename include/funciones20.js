function validaCorreo(id){
    if(document.getElementById(id).value!=""){
        var partes_cor=document.getElementById(id).value.split("@");
        if(partes_cor[0]==""){
            toastr["error"]("", "FORMATO DE CORREO INCORRECTO");
            //$("#"+id).val("");
            $("#"+id).focus();
            return false;
        }
        if(partes_cor[1]== null || partes_cor[1]==""){
            toastr["error"]("", "FORMATO DE CORREO INCORRECTO");
            //$("#"+id).val("");
            $("#"+id).focus();
            return false;
        }else{
            var partes_ext=partes_cor[1].split(".");
            if(partes_ext[0]==""){
                toastr["error"]("", "FORMATO DE CORREO INCORRECTO");
                //$("#"+id).val("");
                $("#"+id).focus();
                return false;
            }
            if(partes_ext[1]== null || partes_ext[1]==""){
                toastr["error"]("", "FORMATO DE CORREO INCORRECTO");
                //$("#"+id).val("");
                $("#"+id).focus();
                return false;
            }
        }
        return true;
    }
}

function exportarP(orientacion){
    $("#datos_a_enviar").val( $("<div>").append( $("#Exportar_tabla").eq(0).clone()).html());
    var datos=encodeURIComponent($('#Exportar_tabla').html());
    window.open('funcionesphp/ficheropdf.php?data='+datos+'&orientacion='+orientacion,'_self','left=200,top=150,width=600,height=60');
}

function mayusculas(campo){
    campo.value=campo.value.toUpperCase();
}

function numeroSucursales(valor) {
    $.post("sucursales/queries/sucursales/consulta.php",{valor:valor},function(data){
        
        return data;
    });
} 
// verificar que el texto tiene solo numeros..
function inputNumeros(texto){
    var numeros="0123456789";
    for(i=0; i<texto.length; i++){
        if (numeros.indexOf(texto.charAt(i),0)==-1){
            return 1;
        }
    }
    return 0;
}

function inputNumeros2(texto){
    var numeros="0123456789.";
    for(i=0; i<texto.length; i++){
        if (numeros.indexOf(texto.charAt(i),0)==-1){
            return 1;
        }
    }
    return 0;
}

function soloNumeros(){
    if ((event.keyCode < 48) || (event.keyCode > 57)) 
    event.preventDefault();
}

// Limitra número de caracteres de un texto..

function limitarCaracteres(n_caracter,text){
    var numero = text.length;
    if (numero < n_caracter) {
        return 1;
    }else{
        return 0;
    }
}

// Agrega el - a los numeros telefónicos

function numTelefono(n_caracter,text){
    var numero = text.length;
    if (numero == n_caracter) {
        return text+"-";
    }else{
        return text;
    }
}

function redireccionar(argument) {
    if (argument == 1) {
        location.href ="?dir=4";
    }else{
        location.href ="?dir=3";
    }
}

// cambiar pago automatico a contado para clientes temporales en crear

function tipoCliente(campo){
    var tipo = campo.value;
    if (tipo == 1) {
        $("#crear_pago").val(6);
        $("#crear_pago").prop('disabled', true);
    }else{
        $("#crear_pago").val(0);
        $("#crear_pago").prop('disabled', false);
    }
}

function tipoDoc(campo){
    var tipo = campo.value;
    if (tipo == 1) {
        $("#crear_rfc").attr('placeholder', '* R.F.C. DEL CLIENTE');
        $("#crear_dfc").attr('placeholder', '* DIRECCION FISCAL DEL CLIENTE');    
    }else{
        $("#crear_rfc").attr('placeholder', ' R.F.C. DEL CLIENTE');
        $("#crear_dfc").attr('placeholder', ' DIRECCION FISCAL DEL CLIENTE');    
    }
}

// cambiar pago automatico a contado para clientes temporales en editar

function tipoEditCliente(campo){
    var tipoEdit = campo.value;
    if (tipoEdit == 1) {
        $("#editar_pago").val(6);
        $("#editar_pago").prop('disabled', true);
    }else{
        $("#editar_pago").val(0);
        $("#editar_pago").prop('disabled', false);
    }
}

function tipoEditDoc(campo){
    var tipoEdit = campo.value;
    if (tipoEdit == 1) {
        $("#editar_rfc").attr('placeholder', '* R.F.C. DEL CLIENTE');
        $("#editar_dfc").attr('placeholder', '* DIRECCION FISCAL DEL CLIENTE');    
    }else{
        $("#editar_rfc").attr('placeholder', ' R.F.C. DEL CLIENTE');
        $("#editar_dfc").attr('placeholder', ' DIRECCION FISCAL DEL CLIENTE');    
    }
}

//exportat a excel

function exportarE(nombre){
    $("#datos_a_enviar").val( $("<div>").append( $("#Exportar_tabla").eq(0).clone()).html());
    var datos=encodeURIComponent($('#Exportar_tabla').html());
    window.open('funcionesphp/ficheroexcel.php?data='+datos+'&nombre='+nombre,'_self','left=200,top=150,width=600,height=60');
}

function loaderIn(){
    jQuery("#loader").fadeIn("slow");
}

function loaderOut(){
    jQuery("#loader").fadeOut("slow");
}


$("form").keypress(function(e) {
    if (e.which == 13) {
        return false;
    }
});

function editarPass(id){
    $.post('usuarios/queries/usuarios/search.php',{id:id},function(data){
        var abc = jQuery.parseJSON(data);
        $('#Editar_id').val(id);
        $('#Editar_nombre').val(abc.nombre);
        $('#Editar_apellido').val(abc.apellido);
        $('#Editar_correo').val(abc.correo);
        $('#Editar_correo_paypal').val(abc.correo_paypal);
        $('#Editar_status').val(abc.status);
        $('#cambiar_pass').modal('show');
    });
}

function actualizar(){
    var id = $('#Editar_id').val();
    var nombre = $('#Editar_nombre').val();
    var apellido = $('#Editar_apellido').val();
    var paypal = $('#Editar_correo_paypal').val();
    var editar_password = $('#editarpassword').val();
    var editar_password2 = $('#editarpassword2').val();
    if (editar_password == "" || editar_password == null || editar_password == undefined) {
        editar_password = "0";
        editar_password2 = "0";
    }
    if(nombre == '' || apellido == ''){
        toastr["warning"]("", "ALGUNOS CAMPOS ESTAN VACIOS");
        return false;
    }else if(editar_password != editar_password2) {
        toastr["error"]("", "LAS CONTRASEÑAS NO COINCIDEN");
        $("#editarpassword").focus();
        return false;
    }else if(paypal == '') {
        toastr["error"]("", "EL CORREO PAYPAL NO PUEDE ESTAR VACÍO");
        $("#editarpassword").focus();
        return false;
    }else{
        $.post("usuarios/queries/usuarios/update2.php",{editar_password:editar_password,id:id,nombre:nombre,paypal:paypal,apellido:apellido},function(data){
            if(data == 1){
                toastr["success"]("", "DATOS ACTUALIZADO CON EXITO");
                $('#editarpassword').val("");
                $('#editarpassword2').val("");
                $('#cambiar_pass').modal('hide');
            }else{
                toastr["error"]("", "EROR ACTUALIZADO DATOS");
            }
        });

    }

}