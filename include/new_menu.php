<div class="app-container app-theme-white body-tabs-shadow fixed-header fixed-sidebar">
    <div class="app-header header-shadow">
        <div class="app-header__logo">
            <div class="nav-header pull-left">
                    <div class="logo-wrap">
                        <a href="?name=">
                            <img class="brand-img" src="assets/images/logo.png" width="180" alt="brand"/>
                        </a>
                    </div>
                </div>  
            <div class="header__pane ml-auto">
                <div>
                    <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
        </div>
        <div class="app-header__mobile-menu">
            <div>
                <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
        <div class="app-header__menu">
            <span>
                <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                    <span class="btn-icon-wrapper">
                        <i class="fa fa-ellipsis-v fa-w-6"></i>
                    </span>
                </button>
            </span>
        </div>    
        <div class="app-header__content">
            <div class="app-header-left">
                   
            </div>
            <div class="app-header-right">
                <div class="header-dots">
                    <div onclick="messagesss()">
                        <div class="dropdown" >
                            <button type="button" aria-haspopup="true"  aria-expanded="false" data-toggle="dropdown" class="p-0 mr-2 btn btn-link" id="messagesss">
                                <span class="icon-wrapper icon-wrapper-alt rounded-circle"><span class="icon-wrapper-bg bg-primary"></span>                                <i class="icon text-primary  ion-android-notifications"><span style="text-decoration: none;font-size: 10px;top: 0px;position: absolute;">0</span></i></span>
                            </button>
                            <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu-xl rm-pointers dropdown-menu dropdown-menu-right">
                                <div class="dropdown-menu-header mb-0">
                                    <div class="dropdown-menu-header-inner bg-deep-blue">
                                        <div class="menu-header-image opacity-1" style="background-image: url('assets/images/dropdown-header/city3.jpg');"></div>
                                        <div class="menu-header-content text-dark">
                                            <span class="badge badge-danger ml-2">TICKECTS NUEVOS</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab-messages-header" role="tabpanel">
                                        <div class="scroll-area-sm">
                                            <div class="scrollbar-container">
                                                <div class="p-3">
                                                    <div class="notifications-box" id="messages_new">
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <ul class="nav flex-column">
                                    <li class="nav-item-divider nav-item"></li>
                                    <li class="nav-item-btn text-center nav-item">
                                        <a href="?name=soporte"class="btn-shadow btn-wide btn-pill btn btn-focus btn-sm">SOPORTE</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                   <?php if (tipoUsuario() < 3 ) { ?>
                         <a href="?name=chat" >
                    <?php }elseif (tipoUsuario() == 3) { ?>
                         <a href="?name=chatred">
                    <?php } else{ ?>
                        <a href="?name=chatrev">
                    <?php } ?>

                        <div class="dropdown">
                            <button type="button" aria-haspopup="true" data-toggle="dropdown" aria-expanded="false" class="p-0 btn btn-link dd-chart-btn" id="tickes">
                                <span class="icon-wrapper icon-wrapper-alt rounded-circle"><span class="icon-wrapper-bg bg-primary"></span>                                <i class="icon text-primary  ion-android-hangout"><span style="text-decoration: none;font-size: 10px;top: 0px;position: absolute;">0</span></i></span>
                            </button>
                           <!-- <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu-xl rm-pointers dropdown-menu dropdown-menu-right">
                                <div class="dropdown-menu-header mb-0">
                                    <div class="dropdown-menu-header-inner bg-deep-blue">
                                        <div class="menu-header-image opacity-1" style="background-image: url('assets/images/dropdown-header/city3.jpg');"></div>
                                        <div class="menu-header-content text-dark">
                                            <span class="badge badge-danger ml-2">MENSAJE DE CHAT NUEVOS</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab-messages-header" role="tabpanel">
                                        <div class="scroll-area-sm">
                                            <div class="scrollbar-container">
                                                <div class="p-3">
                                                    <div class="notifications-box" id="tickes_new">
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <ul class="nav flex-column">
                                    <li class="nav-item-divider nav-item"></li>
                                    <li class="nav-item-btn text-center nav-item">
                                        <a href="?name=soporte"class="btn-shadow btn-wide btn-pill btn btn-success btn-sm">SOPORTE</a>
                                    </li>
                                </ul>
                            </div>                            -->
                        </div>
                    </a>
                </div>
                
                <div class="header-btn-lg pr-0">
                    <div class="widget-content p-0">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="btn-group">
                                    <a data-toggle="dropdown" onclick="cargaStatus()" aria-haspopup="true" aria-expanded="false" class="p-0 btn" ><span id="indicador_status"></span>
                                        <div class="avatar-icon-wrapper mr-2 avatar-icon-xl">
                                            <div class="avatar-icon">
                                                <img width="42" class=" img-perfil" src="images/<?php echo imgPerfil(); ?>" alt="" style="<?php echo imgPerfilStyle(); ?>" >
                                                <i class="fa fa-angle-down ml-2 opacity-8"></i>
                                            </div>
                                        </div>
                                    </a>
                                    <div tabindex="-1" role="menu" aria-hidden="true" class="rm-pointers dropdown-menu-lg dropdown-menu dropdown-menu-right">
                                        <div class="dropdown-menu-header">
                                            <div class="dropdown-menu-header-inner bg-info">
                                                <div class="menu-header-image opacity-2" style="background-image: url('assets/images/dropdown-header/city3.jpg');"></div>
                                                <div class="menu-header-content text-left">
                                                    <div class="widget-content p-0">
                                                        <div class="widget-content-wrapper">
                                                            <div class="widget-content-left mr-3">
                                                                <div class="avatar-icon-wrapper mr-2 avatar-icon-xl">
                                                                    <div class="avatar-icon">
                                                                        <img width="42" class="  img-perfil"
                                                                     src="images/<?php echo imgPerfil(); ?>"
                                                                     alt=""  style="<?php echo imgPerfilStyle(); ?>" >
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="widget-content-left">
                                                                <div class="widget-heading" style="color:#333"><?php echo nombreUsuario(); ?>
                                                                </div>
                                                                <div class="widget-subheading opacity-8"><?php echo tiposUsuario(tipoUsuario()); ?>
                                                                </div>
                                                            </div>
                                                            <div class="widget-content-right mr-2" id="status">
                                                                 

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <ul class="nav flex-column">
                                            <li class="nav-item-btn text-center nav-item">
                                                <div class="widget-content-right mr-2">
                                                    <a href="?name=perfil" class="btn-pill btn-shadow btn-shine btn btn-primary">PERFIL
                                                    </a>
                                                    <a href="login/salir.php" class="btn-pill btn-shadow btn-shine btn btn-focus">SALIR
                                                    </a>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>    
   

    <div class="app-main">
        <div class="app-sidebar sidebar-shadow">
            <div class="app-header__logo">
                <div class="logo-src"></div>
                <div class="header__pane ml-auto">
                    <div>
                        <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="app-header__mobile-menu">
                <div>
                    <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
            <div class="app-header__menu">
                <span>
                    <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                        <span class="btn-icon-wrapper">
                            <i class="fa fa-ellipsis-v fa-w-6"></i>
                        </span>
                    </button>
                </span>
            </div>
            <!-- MENU LEFT-->
            <div class="scrollbar-sidebar">
                <div class="app-sidebar__inner">
                    <ul class="vertical-nav-menu">
                        <li class="app-sidebar__heading">Menu</li>
                        <li class="mm-active">
                            <a href="?name=">
                                <i class="metismenu-icon pe-7s-home"></i>
                                    HOME
                            </a>
                        </li>
                        <li>
                            <a href="?name=perfil">
                                <i class="metismenu-icon pe-7s-id"></i>PERFIL
                            </a>
                        </li>
                        <?php if (tipoUsuario() < 3 ) { ?>
                        <li>
                            <a href="?name=chat">
                                <i class="metismenu-icon pe-7s-comment">
                                </i>CHAT
                            </a>
                        </li>
                        <?php } 
                        if (tipoUsuario() == 3 ) { ?>
                        <li>
                           <a href="?name=chatred">
                                <i class="metismenu-icon pe-7s-comment">
                                </i>CHAT
                            </a>
                        </li>
                         <?php } 
                         if (tipoUsuario() == 4 ) { ?>
                        <li>
                           <a href="?name=chatrev">
                                <i class="metismenu-icon pe-7s-comment">
                                </i>CHAT
                            </a>
                        </li>
                         <?php } ?>
                        <?php if (tipoUsuario() != 4 ){ ?>
                        <li class="app-sidebar__heading">GESTIÓN</li>
                        <li>
                            <a href="?name=history">
                                <i class="metismenu-icon pe-7s-folder"></i>ASIGNACIONES
                            </a>
                        </li>
                        <li>
                            <a href="?name=soporte">
                                <i class="metismenu-icon pe-7s-ticket"></i>SOPORTE
                            </a>
                        </li>
                        <?php } ?>
                        <?php if (tipoUsuario() == 3){ ?>
                        <li>
                            <a href="?name=pagos_redactor">
                                <i class="metismenu-icon pe-7s-cash"></i>
                                PAGOS
                            </a>
                        </li>
                        <?php } ?>
                        <?php if (tipoUsuario() == 3){ ?>
                            <li>
                                <a href="?name=stats">
                                    <i class="metismenu-icon pe-7s-graph1"></i>ESTADÍSTICAS
                                    <!--<i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>-->
                                </a>
                            </li>    
                        <?php } ?>
                            
                    <?php if (tipoUsuario() != 3 ){ ?>
                        </li>
                        <li class="app-sidebar__heading">ADMINISTRACIÓN</li>
                        <?php if (tipoUsuario() != 4 ){ ?>
                        <li>
                            <a href="?name=activas">
                                <i class="metismenu-icon pe-7s-note"></i>REDACTANDO
                                
                            </a>
                            
                        </li>
                        <?php } ?>
                        <li>
                            <a href="?name=entregadas">
                                <i class="metismenu-icon pe-7s-pin"></i>
                                ENTREGADAS
                            </a>
                            
                        </li>
                        <?php if (tipoUsuario() != 4 ){ ?>
                        <li>
                            <a href="?name=asignar">
                                <i class="metismenu-icon pe-7s-attention"></i>
                                POR ASIGNAR
                            </a>
                            
                        </li>
                        <li>
                            <a href="?name=publicar">
                                <i class="metismenu-icon pe-7s-copy-file"></i>
                                PUBLICAR NUEVAS
                               
                            </a>
                            
                        </li>
                        <li>
                            <a href="?name=redactores">
                                <i class="metismenu-icon pe-7s-users"></i>
                                REDACTORES
                            </a>
                            
                        </li>
                        <li>
                            <a href="?name=pagos">
                                <i class="metismenu-icon pe-7s-cash"></i>
                                PAGOS
                            </a>
                            
                        </li>
                    <?php } ?>
                    <?php } ?>
                        <li class="app-sidebar__heading">OPCIONES</li>
                        <?php if (tipoUsuario() < 3 ) { ?>
                        <li>
                            <a href="?name=user">
                                <i class="metismenu-icon pe-7s-config">
                                </i>USUARIOS
                            </a>
                        </li>
                        <?php } ?>
                        
                        <li>
                            <a href="login/salir.php">
                                <i class="metismenu-icon pe-7s-power">
                                </i>SALIR
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div> 
        <script>
           

        </script>