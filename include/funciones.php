<?php 
	//ini_set("session.cookie_lifetime","86400");
	//ini_set("session.gc_maxlifetime","86400");
	//date_default_timezone_set('Europe/Madrid');
	//$arc = array(21 => "password");
	
	$folder = array(1 => "administrador", 2 => "usuarios", 3 => "revisar", 4 => "publicar", 5 => "pagos", 6 => "activas", 7 => "redactores", 8 => "usr", 9 => "soporte");

	$status_ordenes = array(1 => "SIN ASIGNAR", 2 => "ASIGNADA", 3 => "ENTREGADA", 4 => "APROBADA");
	$status_ordenes_color = array(1 => "inverse", 2 => "primary", 3 => "warning", 4 => "danger", 5 => "success", 6 => "success", 7 => "success", 8 => "success", 10 => "inverse");
	$status_color = array(1 => "grey", 2 => "blue", 3 => "yellow", 4 => "red", 5=> "green", 6 => "gold", 7 => "orange", 8 => "green");

	$articulos_status = array(0 => "inverse", 1 => "primary", 2 => "success", 3 => "warning", 4 => "info");
	$soporte_status = array(0 => "inverse", 1 => "warning", 2  => "success");
	
	
	
	// permite el inicio de sesíon - no usada por ahora
	function inicioSesiones($id,$usuario,$cargo){
		session_start();
		$_SESSION['id']      = $id;
		$_SESSION['usuario'] = $usuario;
		$_SESSION['cargo']   = $cargo;
		$exito = true;
		return $exito;
	}

	// validar con contraseña maestra

	function passMaster($pass){
		include('coneccion.php');
		$sql = "SELECT pass FROM usuario where ( id = 1 or correo = 'INFO@JUANMACARRILLO.ES') and activo = 1 ";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
			if($pass == $fila["pass"]){
				return 1;
			}
        }
		return false;
	}

	// Ver mensajes de panel

	function message(){
		session_start();
		$id = $_SESSION['BACKEND_id'];
		include('coneccion.php');
		$message = '';
		$sql = "SELECT id_message,text_message,date_message FROM messages where id_redactor = '$id' and status_message = 1 order by id_message DESC limit 1";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
			$id = $fila["id_message"];
        	$message = '<div class="card-shadow-warning border mb-3 card card-body border-warning col-md-12" id="ver_nota"><h5 class="card-title" id="text_nota">'.$fila["text_message"].'</h5> <a href="javascript:void(0);" onclick="eliminarNota('.$id.');" style="margin-left:99%;"><i class="pe-7s-trash" style="color:red;font-size:16px;"></i></a> </div>';        	 
        }
		return $message;
	}

	function messageAdmin(){
		session_start();
		$id = $_SESSION['BACKEND_id'];
		include('coneccion.php');
		$message = '<div class="card-shadow-warning border mb-3 card card-body border-warning col-md-12" id="ver_nota">NO HAY NOTAS EN EL PANEL</div>';
		$sql = "SELECT id_message,text_message,date_message FROM messages where id_redactor = '$id' and status_message = 1 order by id_message DESC limit 1";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
			$id = $fila["id_message"];
        	$message = '<div class="card-shadow-warning border mb-3 card card-body border-warning col-md-12" id="ver_nota"><h5 class="card-title" id="text_nota">'.$fila["text_message"].'</h5> <a href="javascript:void(0);" onclick="eliminarNota('.$id.');" style="margin-left:99%;"><i class="pe-7s-trash" style="color:red;font-size:16px;"></i></a> </div>';        	 
        }
		return $message;
	}
	// devuleve la fecha del último pago
	function lastPage($id){
		session_start();
		$id = $_SESSION['BACKEND_id'];
		include('coneccion.php');
		$sql = "SELECT ultimo_pago from perfil_redactor where id_redactor = '$id'";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
        	$message = $fila["ultimo_pago"];        	 
        }
		return $message;
	}

	// devuleve status de usuario para el chat según id
	function statusUsuarioChat($id){
		$ahora =  date('Y-m-d H:i:s');
		include('coneccion.php');
		$sql = "SELECT status, date_change from usuario_online where id_usuario = '$id' limit 1";
		$result = $conn->query($sql);
		$status = 0;
		while($fila=$result->fetch_array()){
        	$status = $fila["status"];  
        	$date = $fila["date_change"];        	 
        }
        if ($status == 0) {
        	return 0;
        }else{
        	$diferencia = diferenciaMin($date,$ahora);
        	if ($diferencia > 30) {
        		return 0;
        	}else{
        		return 1;
        	}
        }
	}

	// devuleve img de perfil del logueado
	function imgPerfil(){
		session_start();
		$id = $_SESSION['BACKEND_id'];
		include('coneccion.php');
		$sql = "SELECT img from perfil_usuario where id_usuario = '$id'";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
        	$img = $fila["img"];        	 
        }
        if (!$img) {
        	$img = "avatar.jpg";
        }
        $img = str_replace("\n", "", $img);
		return $img;
	}

	function perfilHidden(){
		session_start();
		$id = $_SESSION['BACKEND_id'];
		include('coneccion.php');
		$sql = "SELECT img from perfil_usuario where id_usuario = '$id'";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
        	$img = $fila["img"];        	 
        }
        if (!$img) {
        	$img = "avatar.jpg";
        }
        $img = '<input type="hidden" name="src_img" id="src_img" value="'.$img.'">';
		return $img;
	}

	// devuleve img de perfil del logueado
	function statusPerfil(){
		session_start();
		$id = $_SESSION['BACKEND_id'];
		include('coneccion.php');
		$sql = "SELECT status_perfil from perfil_usuario where id_usuario = '$id'";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
        	$img = $fila["status_perfil"];        	 
        }
        if ($img == 2) {
        	return true;
        }
		return false;
	}

	// DEVUELCE STYLO IMG DE PERFIL DE LOGUEADO
	function imgPerfilStyle(){
		session_start();
		$id = $_SESSION['BACKEND_id'];
		include('coneccion.php');
		$sql = "SELECT style from perfil_usuario where id_usuario = '$id'";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
        	$img = $fila["style"];        	 
        }
        if (!$img) {
        	$img = "";
        }
		return $img;
	}
	function imgPerfilStyleId($id){
		include('coneccion.php');
		$sql = "SELECT style from perfil_usuario where id_usuario = '$id'";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
        	$img = $fila["style"];        	 
        }
        if (!$img) {
        	$img = "";
        }
		return $img;
	}

	// devuleve img de perfil por ID
	function imgPerfilId($id){
		include('coneccion.php');
		$sql = "SELECT img from perfil_usuario where id_usuario = '$id'";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
        	$img = $fila["img"];        	 
        }
        if (!$img) {
        	$img = "avatar.jpg";
        }
		return $img;
	}

	function ultima_coneId($id){
        include('coneccion.php');
        $sql = "SELECT ultima_conexion from perfil_redactor where id_usuario = '$id'";
        $result = $conn->query($sql);
        while($fila=$result->fetch_array()){
            $ultima_conexion= $fila["ultima_conexion"];             
        }
        if (!$ultima_conexion) {
            $ultima_conexion = "No se conecta desde hace mucho tiempo";
        }
        return $ultima_conexion;
    }

    function usuario_seleccionado($id) {
	   include('coneccion.php');
	        $sql = "id,nombre, apellido, idtipousuario, status FROM usuario where id_usuario = '$id'";
	        $result = $conn->query($sql);
	        while($fila=$result->fetch_array()){
	            $id = $fila['id'];
	            $nombre = $fila['nombre'];
	            $apellido = $fila['apellido'];
	            $idtipousuario = $fila['idtipousuario'];  
	            $usuario = $nombre. " ". $apellido;        
	        }

	        return $usuario;
	}

	// devuleve la fecha del último pago
	function lastPageId($id){
		include('coneccion.php');
		$sql = "SELECT ultimo_pago from perfil_redactor where id_redactor = '$id'";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
        	$message = $fila["ultimo_pago"];        	 
        }

        if ($message == "") {
        	$message = "SIN PAGOS";
        }else{
        	$message = ConvFecha2($message);
        }
		return $message;
	}

	function lastPageId2($id){
		include('coneccion.php');
		$sql = "SELECT ultimo_pago from perfil_redactor where id_redactor = '$id'";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
        	$message = $fila["ultimo_pago"];        	 
        }

        if ($message == "") {
        	$message = "2018-01-01 00:00:01";
        }else{
        	$message = ConvFecha($message);
        }
		return $message;
	}

	// VER SI HAY COMENTARIOS SIN LEER

	function commentsRed(){
		include('coneccion.php');
		session_start();
		$id = $_SESSION['BACKEND_id'];
		$tipo_user = $_SESSION['BACKEND_idtipousuario'];
		$tick = 0;
		$msg = 0;
		$sql = "SELECT id_soporte,chechk_date_tickets from soporte_tickects where id_redactor = '$id' and status < 2";	
		
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
        	$id_soporte = $fila["id_soporte"];
        	$chechk_date_tickets = $fila["chechk_date_tickets"];
        	if ($chechk_date_tickets == 1) { // 1 = creado por admin
        		$tick = $tick + 1; 
        	}

			$sql2 = "SELECT id_comentario from soporte_comentarios where id_soporte = '$id_soporte' and chechk_date_comentario = 1"; // 1 =  creado por admin
			$result2 = $conn->query($sql2);
			while($fila2=$result2->fetch_array()){
		       	$tick = $tick + 1; 	
		    }
        	
        }
        $query1 = "SELECT status_mensaje from chat_usuario where id_usuario_receptor = '$id' and status_mensaje = 1";
        $result2 = $conn->query($query1);
		while($fila2=$result2->fetch_array()){
        	$msg = $msg + 1;
        }
		
		return $msg.','.$tick; 
		
		
	}

	function commentsRedAdmin(){
		include('coneccion.php');
		session_start();
		$id = $_SESSION['BACKEND_id'];
		$tick = 0;
		$msg = 0;
		$sql = "SELECT id_soporte,chechk_date_tickets from soporte_tickects where status < 2";
		
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
        	$id_soporte = $fila["id_soporte"];
        	$chechk_date_tickets = $fila["chechk_date_tickets"];
        	if ($chechk_date_tickets == 2) { // 1 = creado por admin
        		$tick = $tick + 1; 
        	}

			$sql2 = "SELECT id_comentario from soporte_comentarios where id_soporte = '$id_soporte' and chechk_date_comentario = 2"; // 1 =  creado por admin
			$result2 = $conn->query($sql2);
			while($fila2=$result2->fetch_array()){
		       	$tick = $tick + 1; 	
		    }
        	
        }

        $query1 = "SELECT status_mensaje from chat_usuario where id_usuario_receptor = '$id' and status_mensaje = 1";
        $result2 = $conn->query($query1);
		while($fila2=$result2->fetch_array()){
        	$msg = $msg + 1;
        }
		
		return $msg.','.$tick; 
		
		
	}

	// devuelve la ref de un articulo
	function searcRef($id){
		include('coneccion.php');
		$sql = "SELECT articulo_ref from articulos where articulo_id = '$id'";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
        	$message = $fila["articulo_ref"];        	 
        }
		return $message;
	}
	// detectar si existe registros igual y evita que se dupliquen en los create.php
	function CountSomething($table,$filtro){
		include('coneccion.php');

		$numRows = 0;
		$sql = "SELECT * FROM  $table WHERE $filtro";
		$result = $conn->query($sql);
		$numRows = $result->num_rows;

		return $numRows;
	}

	//convierte la fecha 
	function ConvFecha($fecha){ // $fecha aaaa-mm-dd
		$sep=explode(" ", $fecha);
		$fec=explode("-", $sep[0]);
		$hor=explode(":", $sep[1]);
		$fechafinal=$fec[2]." DE ".devuelveMes($fec[1])." DEL ".$fec[0]." A LAS ".$hor[0].":".$hor[1];
		return $fechafinal; // $fechafinal = dd de mm del aaaa
	}

	function ConvFecha2($fecha){ // $fecha aaaa-mm-dd
		$sep=explode(" ", $fecha);
		$fec=explode("-", $sep[0]);
		$hor=explode(":", $sep[1]);
		$fechafinal=$fec[2]." DE ".devuelveMes($fec[1])." DEL ".$fec[0];
		return $fechafinal; // $fechafinal = dd de mm del aaaa
	}

	// devuelve el nombre del mes
	function devuelveMes($mes){
		$meses = array("01" => "ENERO", "02" => "FEBRERO", "03" => "MARZO", "04" => "ABRIL", "05" => "MAYO", "06" => "JUNIO", "07" => "JULIO", "08" => "AGOSTO", "09" => "SEPTIEMBRE", 10 => "OCTUBRE", 11 => "NOVIEMBRE", 12 => "DICIEMBRE");
		$nombre=$meses[$mes];
		return $nombre;
	}

	// devuelve el nombre del status de la orden según valor

	

	//devuelve la url del folder
	function devFolder($url,$num){
	    $urls = explode("/", $url);
	    return $urls[$num];
	}


	// devuelve el id del tipo de usuario para usarlo como roles y permisología
	function tipoUsuario() {
		session_start();
		$sessions_idtipousuario = $_SESSION['BACKEND_idtipousuario'];
		return $sessions_idtipousuario;
	}

	// devuelve el nombre de usuario que esta logueado en sistema
	function nombreUsuario() {
		session_start();
		$sessions_nombre = $_SESSION['BACKEND_nombre'];
		$sessions_apellido = $_SESSION['BACKEND_apellido'];
		$datos = ucwords($sessions_nombre)." ".ucwords($sessions_apellido);
		return $datos;
	}

	// id del usuario logueado
	function idUsuario() {
		session_start();
		$sessions_id = $_SESSION['BACKEND_id'];
		return $sessions_id;
	}

	// llena un select con los usuarios activos
	function selectUsuario($tipo){
		$numRows = 0;
		$sql = "SELECT id, nombre, apellido FROM usuario where $tipo ";
		$activas = CountSomething('',"");
		include('coneccion.php');
		$slect = '';
		
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
			$sql2 = "SELECT * FROM  articulos WHERE articulo_redactor = '".$fila['id']."' AND (articulo_status = 0 or articulo_status = 3 )";
			$result2 = $conn->query($sql2);
			$activas = $result2->num_rows;
        	$cliente = ucwords($fila['nombre'])." ".ucwords($fila['apellido'])." (".$activas.")";
            $slect .= "<option value='".$fila['id']."'>".utf8_encode($cliente)."</option>";
        }
		echo $slect;
	}

	function selectUsuario2($tipo){
		$numRows = 0;
		$sql = "SELECT id, nombre, apellido FROM usuario where $tipo ";
		$activas = CountSomething('',"");
		include('coneccion.php');
		$slect = '';
		
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
			$sql2 = "SELECT * FROM  articulos WHERE articulo_redactor = '".$fila['id']."' AND (articulo_status = 0 or articulo_status = 3 )";
			$result2 = $conn->query($sql2);
			$activas = $result2->num_rows;
        	$cliente = ucwords($fila['nombre'])." ".ucwords($fila['apellido'])."(".$activas.")";
            $slect .= "<option value='".$fila['id']."'>".utf8_encode($cliente)."</option>";
        }
		return $slect;
	}

	// devuelve las ref activas de un redactor
	function refActivas($id)	{
		
	}
	// llena un select con los Clientes activos
	function selectCliente(){
		include('coneccion.php');
		$slect = '<option value="0">SELECCIONE CLIENTE</option>';
		$sql = "SELECT cliente_id, cliente_nombre, cliente_numero FROM clientes where cliente_activo = 1 order by cliente_id desc";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
        	$cliente = ucwords($fila['cliente_numero']). " - ".ucwords($fila['cliente_nombre']);
            $slect .= "<option value='".$fila['cliente_id']."'>"./*utf8_encode(*/$cliente/*)*/."</option>";
        }
		echo $slect;
	}

	// llena un select con los Parnert activos
	function selectParnert(){
		include('coneccion.php');
		$slect = '<option value="0">SELECCIONE PARNERT</option>';
		$sql = "SELECT parnert_id, parnert_razon, parnert_contacto FROM perfil_parnert where parnert_status = 1 order by parnert_id desc";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
        	$cliente = ucwords($fila['parnert_razon']). " - ".ucwords($fila['parnert_contacto']);
            $slect .= "<option value='".$fila['parnert_id']."'>"./*utf8_encode(*/$cliente/*)*/."</option>";
        }
		echo $slect;
	}

	// llena un select con los tipos de usuarios disponbibles para seleccionar al momento de crear usuarios
	function selectTipoUsuario(){
		include('coneccion.php');
		$slect = '<option value="0">TIPO DE USUARIO</option>';
		$sql = "SELECT id,nombre FROM usuario_tipo where status = 1 ";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
			if ($fila["id"]>1) {
				$cliente = ucwords($fila['nombre']);
	            $slect .= "<option value='".$fila['id']."'>"./*utf8_encode(*/$cliente/*)*/."</option>";
            }
        }
		echo $slect;
	}

	// devuelve el nombre del tipo de usuario selesccionado por id
	function tiposUsuario($id){
		include('coneccion.php');
		$sql = "SELECT nombre FROM usuario_tipo where id = '$id' and status = 1";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
        	$tipo_usuario = ucwords($fila['nombre']);
        }
		return $tipo_usuario;
	}

	// devuelve el id del tipo de usuario seleccionado por id del usuario
	function idTipoUsuario($id){
		include('coneccion.php');
		$sql = "SELECT idtipousuario FROM usuario where id = '$id'";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
        	$tipo_usuario = ucwords($fila['idtipousuario']);
        }
		return $tipo_usuario;
	}

	// status usuario
	function statusUsuario($id){
		include('coneccion.php');
		$sql = "SELECT status FROM usuario where id = '$id'";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
        	$tipo_usuario = $fila['status'];
        }
		return $tipo_usuario;
	}


	// devuelve el nombre del Cliente de acuerdo al ID
	function refId($id){
		include('coneccion.php');
		$sql = "SELECT articulo_ref FROM articulos where articulo_id = '$id'";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
        	$cliente_id = ucwords($fila['articulo_ref']);
        }
		return $cliente_id;
	}
	function clienteNum($id){
		include('coneccion.php');
		$sql = "SELECT cliente_numero, cliente_nombre FROM clientes where cliente_id = '$id'";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
        	$cliente_id = ucwords($fila['cliente_numero']);
        }
		return $cliente_id;
	}

	// devuelve el nombre del Cliente de acuerdo al ID
	function tecnicoId($id){
		include('coneccion.php');
		$sql = "SELECT tecnico_nombre FROM tecnico where tecnico_id = '$id'";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
        	$cliente_id = ucwords($fila['tecnico_nombre']);
        }
		return $cliente_id;
	}

	// devuelve el nombre del Parnert de acuerdo al ID
	function parnertId($id){
		include('coneccion.php');
		$sql = "SELECT parnert_razon FROM perfil_parnert where parnert_id = '$id'";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
        	$cliente_id = ucwords($fila['parnert_razon']);
        }
		return $cliente_id;
	}
	// devuelve el nombre de la Sucursal de acuerdo al ID
	function sucursalId($id){
		include('coneccion.php');
		$sql = "SELECT cs_codigo, cs_nombre FROM clientes_sucursal where cs_id = '$id'";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
        	$sucursal_id = ucwords($fila['cs_codigo'])." - ".ucwords($fila['cs_nombre']);
        }
		return $sucursal_id;
	}

		// devuelve el nombre del vendedor de acuerdo al ID
	function vendedorId($id){
		include('coneccion.php');
		$sql = "SELECT vendedor_nombre FROM vendedor where vendedor_id = '$id'";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
        	$sucursal_id = ucwords($fila['vendedor_nombre']);
        }
		return $sucursal_id;
	}

	// devuelve el nombre del Sistema de acuerdo al ID
	function servicioId($id){
		include('coneccion.php');
		$sql = "SELECT servicio_nombre FROM servicio where servicio_id = '$id'";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
        	$servicio_id = ucwords($fila['servicio_nombre']);
        }
		return $servicio_id;
	}

	// devuelve el nombre del servicio de acuerdo al ID
	function sistemaId($id){
		include('coneccion.php');
		$sql = "SELECT sistema_nombre FROM sistema where sistema_id = '$id'";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
        	$sistema_id = ucwords($fila['sistema_nombre']);
        }
		return $sistema_id;
	}	

	// Devuelve el status de una orden.

	function idStatusOrden($id){
		include('coneccion.php');
		$sql = "SELECT os_status FROM orden_servicio where os_id = '$id'";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
        	$sistema_id = $fila['os_status'];
        }
		return $sistema_id;
	}

	// Devuelve el nombre del usuario segun el ID

	function usuarioId($id){
		include('coneccion.php');
		$sql = "SELECT nombre, apellido FROM usuario where id = '$id'";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
        	$sistema_id = ucwords($fila['nombre'])." ".ucwords($fila['apellido']);
        }
		return $sistema_id;
	}
	function correoId($id){
		include('coneccion.php');
		$sql = "SELECT correo FROM usuario where id = '$id'";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
        	$sistema_id = $fila['correo'];
        }
		return $sistema_id;
	}
	function correoPay($id){
		include('coneccion.php');
		$sql = "SELECT correo_paypal FROM usuario_pago where id = '$id'";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
        	$sistema_id = $fila['correo_paypal'];
        }
		return $sistema_id;
	}
	function pagoId($id){
		include('coneccion.php');
		$sql = "SELECT monto_total FROM perfil_redactor where id_redactor = '$id'";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
        	$sistema_id = $fila['monto_total'];
        }
		return $sistema_id;
	}

	// Carga los datos de tipo de servicio y otros
	function selectCarga($table){
		include('coneccion.php');
		if ($table=="servicio") {
			$slect = '<option value="0">SELECCIONE SERVICIO</option>';
			$sql = "SELECT * FROM $table where servicio_status = 1 and servicio_id < 7";
			$result = $conn->query($sql);

			while($fila=$result->fetch_array()){
				$slect .= "<option value='".$fila['servicio_id']."'>"./*utf8_encode(*/$fila['servicio_nombre']/*)*/."</option>";
			}
		}elseif ($table=="sistema") {
			$slect = '<option value="0">SELECCIONE SISTEMA</option>';
			$sql = "SELECT * FROM $table where sistema_status = 1 and sistema_id < 7";
			$result = $conn->query($sql);
			while($fila=$result->fetch_array()){
				$slect .= "<option value='".$fila['sistema_id']."'>"./*utf8_encode(*/$fila['sistema_nombre']/*)*/."</option>";
			}
		}elseif ($table=="tecnico") {
			$slect = '<option value="0">ASIGNAR A:</option>';
			$sql = "SELECT * FROM $table where tecnico_status = 1";
			$result = $conn->query($sql);
			while($fila=$result->fetch_array()){
				$slect .= "<option value='".$fila['tecnico_id']."'>"./*utf8_encode(*/$fila['tecnico_nombre']/*)*/."</option>";
			}
		}else{
			$slect = '<option value="0">SELECCIONE VENDEDOR</option>';
			$sql = "SELECT * FROM $table where vendedor_status = 1";
			$result = $conn->query($sql);
			while($fila=$result->fetch_array()){
				$slect .= "<option value='".$fila['vendedor_id']."'>"./*utf8_encode(*/$fila['vendedor_nombre']/*)*/."</option>";
			}
		}
	
		echo $slect;
		
	}
	// funcion para exportar a pdf - 
	function exportar(){
		$datos="";
		$datos.='<div style="float:right;margin-bottom:10px;">';
		$datos.='	<button type="button" data-toggle="tooltip" data-placement="left" data-original-title="Exportar a excel" class="btn btn-success btn-sm excel" id="excel" onclick="exportarE()"><i style="font-size:14px;" class="fa fa-file-excel-o"></i> Exportar a Excel</button>';
		//$datos.='	<button type="button" data-toggle="tooltip" data-placement="top" data-original-title="Exportar a Word" class="btn btn-info btn-sm excel" onclick="exportarW()"><i style="font-size:14px;" class="fa fa-file-word-o"></i> Exportar a Word</button>';
		$datos.='	<button type="button" data-placement="right" data-original-title="Exportar a PDF" class="btn btn-danger btn-sm excel" onclick="exportarP()"><i style="font-size:14px;" class="fa fa-file-pdf-o"></i> Exportar a PDF</button>';
		$//datos.="	<script type='text/javascript'>$(\"[data-toggle='tooltip']\").tooltip();</script>";
		$datos.='</div>';
		echo $datos;
	}
	


	// devuelve el status del Cliente de acuerdo al ID
	function statusCliente($id){
		include('coneccion.php');
		$sql = "SELECT cliente_status FROM clientes where cliente_id = '$id'";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
        	$status = $fila['cliente_status'];
        }
		return $status;
	}

	// devuelve el correo del Cliente de acuerdo al ID
	function correoCliente($id){
		include('coneccion.php');
		$sql = "SELECT cliente_correo FROM clientes where cliente_id = '$id'";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
        	$status = strtoupper($fila['cliente_correo']);
        }
		return $status;
	}

	// devuelve el id del tipo de pago del Cliente de acuerdo al ID
	function pagoCliente($id){
		include('coneccion.php');
		$sql = "SELECT cliente_pago FROM clientes where cliente_id = '$id'";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
        	$status = $fila['cliente_pago'];
        }
		return $status;
	}

	// devuelve la dirección del cliente de acuerdo al ID
	function direccionCliente($id){
		include('coneccion.php');
		$sql = "SELECT cliente_direccionF FROM clientes where cliente_id = '$id'";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
        	$status = $fila['cliente_direccionF'];
        }
		return $status;
	}

	// devuelve la dirección del cliente de acuerdo al ID
	function direccionSucursal($id){
		include('coneccion.php');
		$sql = "SELECT cs_direccion FROM clientes_sucursal where cs_id = '$id'";
		$result = $conn->query($sql);
		while($fila=$result->fetch_array()){
        	$status = $fila['cs_direccion'];
        }
		return $status;
	}

	function correo($correo,$titulo,$contenido,$remite){

		$subject = $titulo;
										
		$msgformato = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
		    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		    <title>ISISA</title>
		    <style type="text/css">
		    body {font-family: Arial, Helvetica, sans-serif;}
		    <!--
		       .empresa {
		        font-size: 18px;
		       }
		       .notificacion {
		        font-size: 18px;
		       }
		    -->
		    </style>
		    </head>
		    <body>';
		
		$header ='</body> </html>';
		$msgformat = $msgformato.$contenido.$header;
		$headers .= "MIME-Version: 1.0\r\n"; 
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n"; 
		$headers .= "From: ISISA"." <no-responder@isisa.com>\r\n"; 
		$headers .="Reply-To: ".$remite."\n"; 
										
		mail($correo, $subject, $msgformat, $headers);
	}

	// Consulta numero de impresiones de una orden

	function numeroImpresiones($id){
		include('coneccion.php');
		$query="SELECT valor_impresiones FROM impresiones_pdf WHERE id_os = '$id'";
		$res=$conn->query($query);
		if($res->num_rows>0){
			 while($fila=$res->fetch_array()){
			 	$valor_impresiones = $fila['valor_impresiones'];
			 
			 }
		}else {
			$valor_impresiones=0;
		}
		return $valor_impresiones;
	}

	function DevuelveFechaTimeStamp($fecha){
		$fec=explode(" ", $fecha);
		$fec1=explode("-", $fec[0]);
		$fechafinal=$fec1[2]."/".$fec1[1]."/".$fec1[0];
		return $fechafinal." ".$fec[1];
	}
	// deveueve diferencia en dias
	function compara_fechas($fecha1,$fecha2) { 
	    $fec          = explode("-", $fecha1);
	    $fec2         = explode("-", $fecha2);
	    $dif_segundos = mktime(0,0,0,$fec[1],$fec[2],$fec[0]) - mktime(0,0,0,$fec2[1],$fec2[2],$fec2[0]); // Calcula la diferencia en segundos
	    $dif_dias     = $dif_segundos / (60 * 60 * 24);  // Convertir diferencia en dias
	    $dif_dias     = abs($dif_dias); // le quita el posible signo negativo
	    $dif          = floor($dif_dias); // Quita los decimales
	    return $dif;                          
	}
	// deveueve diferencia en horas.
	function diferenciaHoras($fecha1,$fecha2) {
		$fecha_date1 = explode(" ", $fecha1);
		$fecha_date2 = explode(" ", $fecha2);

		$fecha1 = $fecha_date1[0];
		$fecha2 = $fecha_date2[0];

		$hora1 = $fecha_date1[1];
		$hora2 = $fecha_date2[1];

		$fec          = explode("-", $fecha1);
	    $fec2         = explode("-", $fecha2);

	    $hora1 = explode(":", $hora1);
	    $hora2 = explode(":", $hora2);


	    $fecha1=mktime($hora1[0],$hora1[1],$hora1[2],$fec[1],$fec[2],$fec[0]);
	    $fecha2=mktime($hora2[0],$hora2[1],$hora2[2],$fec2[1],$fec2[2],$fec2[0]);
		

	    $dif_segundos = $fecha1-$fecha2;
	    $dif_dias     = $dif_segundos/60/60;  // Convertir diferencia en dias $horas=$segundos/60/60;
	    $dif_dias     = abs($dif_dias); // le quita el posible signo negativo
	    $dif          = floor($dif_dias); // Quita los decimales
	    return $dif;                          
	}

	function diferenciaMin($fecha1,$fecha2) {
		$fecha_date1 = explode(" ", $fecha1);
		$fecha_date2 = explode(" ", $fecha2);

		$fecha1 = $fecha_date1[0];
		$fecha2 = $fecha_date2[0];

		$hora1 = $fecha_date1[1];
		$hora2 = $fecha_date2[1];

		$fec          = explode("-", $fecha1);
	    $fec2         = explode("-", $fecha2);

	    $hora1 = explode(":", $hora1);
	    $hora2 = explode(":", $hora2);


	    $fecha1=mktime($hora1[0],$hora1[1],$hora1[2],$fec[1],$fec[2],$fec[0]);
	    $fecha2=mktime($hora2[0],$hora2[1],$hora2[2],$fec2[1],$fec2[2],$fec2[0]);
		

	    $dif_segundos = $fecha1-$fecha2;
	    $dif_dias     = $dif_segundos/60;  // Convertir diferencia en dias $horas=$segundos/60/60;
	    $dif_dias     = abs($dif_dias); // le quita el posible signo negativo
	    $dif          = floor($dif_dias); // Quita los decimales
	    return $dif;                          
	}

	/**
	* @Descripcion: Elimina los acentos de una cadena
	* @param $string
	* @return $string sin acentos
	*/

	function EliminarAcentos($String){
		$tofind = "ÀÁÂÄÅàáâäÒÓÔÖòóôöÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ";
		$replac = "AAAAAaaaaOOOOooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn";
		return utf8_encode(strtr(utf8_decode($String), utf8_decode($tofind),$replac));
	}	

	function antinyeccion($cadena){
		$cadena=str_replace(array("'","\""," AND "," and "," OR "," or "), array("","","","","",""), $cadena);
		return $cadena;
	}

	function paginacion($paginado,$pag_actual){
		$pag="";
		$pag.="<div class='container' id='paginacion' style='text-align: center; bottom:10px;right:4px;width:100%;position:absolute;top:100%;'><ul class='pagination' style='margin:0 auto;'>";

			//==> Si la pagina actual es la primera, este boton no tendra funcion
			if($pag_actual>1)
				$funcion_atr=' onclick="pag('.($pag_actual-1).')" ';
			else
				$funcion_atr=" ";

		$pag.='<li><a '.$funcion_atr.' style="cursor:pointer;">&laquo;</a></li>';
		    for ($a=0;$a<($paginado+1);$a++)
		    {
		    	$pag.='<li id="pag'.($a+1).'"><a onclick="pag('.($a+1).')" style="cursor:pointer;"><b>'.($a+1).'</b></a></li>';
			}	
			//==> Si la pagina actual es la ultima, este boton no tendra funcion
			if($paginado>=$pag_actual)
				$funcion_sig=' onclick="pag('.($pag_actual+1).')" ';
			else
				$funcion_sig=" ";

		$pag.='<li><a '.$funcion_sig.' style="cursor:pointer;">&raquo;</a></li>';	
		$pag.="</ul></div>";

		echo $pag;
	}

	function inicio_fin_semana($fecha){

	    $diaInicio="Monday";
	    $diaFin="Sunday";

	    $strFecha = strtotime($fecha);

	    $fechaInicio = date('Y-m-d',strtotime('last '.$diaInicio,$strFecha));
	    $fechaFin = date('Y-m-d',strtotime('next '.$diaFin,$strFecha));

	    if(date("l",$strFecha)==$diaInicio){
	        $fechaInicio= date("Y-m-d",$strFecha);
	    }
	    if(date("l",$strFecha)==$diaFin){
	        $fechaFin= date("Y-m-d",$strFecha);
	    }
	    return Array("fechaInicio"=>$fechaInicio,"fechaFin"=>$fechaFin);
	}
	function calcular_dias($fecha){
		$fechas = "";
		for( $i = 6; $i > 0; $i-- ){
			$fechas .= date("Y-m-d", strtotime("$fecha   -$i day")).",";
		}
		return $fechas;
	}
	function nivel($id){
		include('coneccion.php');
		$query = "SELECT numero from valoraciones_redactor where redactor_id = '$id' order by id_valoraciones DESC limit 5 ";
		$res = $conn->query($query);
		$valor = 0;
		$num = 0;
		$nivel = 0;
		if($res->num_rows > 0){
    		while($fila=$res->fetch_array()){
    			$valoracion = $fila['numero'];
    			$valor = $valor + $valoracion;
    			$num = $num +1;
		   	}
		   	$nivel = round($valor/$num,0);
		}else{
			$nivel = 7;
		}
		return $nivel;
	}
?> 

