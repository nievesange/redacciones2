    <!-- Disable tap highlight on IE -->
    <link rel="icon" href="favicon.ico" type="image/x-icon">


	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Panel Redactaria</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no"
    />
    <meta name="description" content="Panel Redactaria" />

    <!-- Disable tap highlight on IE -->
    <meta name="msapplication-tap-highlight" content="no">
<link href="add/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    
    <link href="add/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" type="text/css">
        
    <!-- Custom CSS -->
    <link href="add/dist/css/style.css" rel="stylesheet" type="text/css">
    <link href="add/vendors/bower_components/jquery-wizard.js/css/wizard.css" rel="stylesheet" type="text/css"/>
    <link href="add/vendors/bower_components/jquery.steps/demo/css/jquery.steps.css" rel="stylesheet">
    <link href="add/vendors/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" type="text/css"/>
    <link href="add/vendors/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">
    <link href="add/vendors/toastr/toastr.min.css" rel="stylesheet" type="text/css"/>
            


<link href="add/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>    
<link href="add/vendors/bower_components/dropify/dist/css/dropify.min.css" rel="stylesheet" type="text/css"/>


<link href="add/vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="./main.8d288f825d8dffbbe55e.css" rel="stylesheet">
<script src="add/vendors/bower_components/jquery/dist/jquery.min.js"></script>
<script src="add/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<style>
    ::-webkit-scrollbar{
        background: rgba(0, 0, 0, 0);
        width: 5px;
        right: 0px;
    }
    ::-webkit-scrollbar-track{
        background:#FBFBFB;
        box-shadow: 3px 3px 10px rgba(0,0,0,.25) inset;

    }
    ::-webkit-scrollbar-thumb{
        background: #84c4ff0f;
        border-radius: 10px;
        border:1px solid rgba(0,0,0,0.3);
        box-shadow: -3px -3px 6px rgba(66,137,224,1) inset,3px 3px 3px rgba(158,226,225,1) inset;
    }
</style>