<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
include('../../../include/coneccion.php');
include('../../../include/funciones.php');

$fechaToday = date('Y-m-d H:i:s');
$fecha_date = date('Y-m-d');
$z = 1;
$id = idUsuario();

$query="SELECT correo_paypal, nivel_paypal, correo_skrill, nivel_skrill, correo_airtm, nivel_airtm, correo_uphold, nivel_uphold FROM usuario_pago where id_usuario = '$id' order by id_usuario_pago DESC limit 1";
$res=$conn->query($query);
if($res->num_rows>0){
    while($fila=$res->fetch_array()){
        $correo_paypal = $fila['correo_paypal'];
        $nivel_paypal = $fila['nivel_paypal'];
        $correo_skrill = $fila['correo_skrill'];
        $nivel_skrill = $fila['nivel_skrill'];
        $correo_airtm = $fila['correo_airtm'];
        $nivel_airtm = $fila['nivel_airtm'];
        $correo_uphold = $fila['correo_uphold'];
        $nivel_uphold = $fila['nivel_uphold'];
        $orden = array($nivel_paypal => $correo_paypal, $nivel_skrill => $correo_skrill, $nivel_airtm => $correo_airtm, $nivel_uphold => $correo_uphold);
        $orden_id = array($nivel_paypal => 1, $nivel_skrill => 2, $nivel_airtm => 3, $nivel_uphold => 4);
        $orden_name = array($nivel_paypal => 'correo_paypal', $nivel_skrill => 'correo_skrill', $nivel_airtm => 'correo_airtm', $nivel_uphold => 'correo_uphold');
        $orden_place = array($nivel_paypal => 'Correo Paypal', $nivel_skrill => 'Correo Skrill', $nivel_airtm => 'Correo Transferwise', $nivel_uphold => 'Correo Uphold');
        $orden_tr = array($nivel_paypal => '10', $nivel_skrill => '20', $nivel_airtm => '30', $nivel_uphold => '40');
         $orden_image = array($nivel_paypal => 'images/paypal.png', $nivel_skrill => 'images/skrill.png', $nivel_airtm => 'images/transferwise.png', $nivel_uphold => 'images/uphold.jpg');
       
        $completo = 
        '<tbody id="cuerpo">
                <tr id="'.$orden_tr[1].'">
                    <th scope="row" style="width: 70px;" id="img1">
                        &nbsp;
                        <img src="images/arriba.jpg" alt="subir fila" class="imgA"/>&nbsp;
                        <img src="images/abajo.jpg" alt="bajas fila" class="imgA"/>
                    </th>
                    <td style="width: 230px; padding-right:0px;">
                        <input type="text" name="'.$orden_name[1].'" id="'.$orden_name[1].'" placeholder="'.$orden_place[1].'" value="'.$orden[1].'" style="width: 200px; padding-left:10px;">
                        <input type="hidden"  id="'.$orden_id[1].'" value="1">
                    </td>
                    <td>
                        <img src="'.$orden_image[1].'" style="width: 30px;">
                    </td>
                </tr>
                <tr id="'.$orden_tr[2].'">
                    <th scope="row">
                        &nbsp;
                        <img src="images/arriba.jpg" alt="subir fila" class="imgA" />&nbsp;
                        <img src="images/abajo.jpg" alt="bajas fila" class="imgA"/>
                    </th>
                    <td style="width: 230px; padding-right:0px;">
                        <input type="text" name="'.$orden_name[2].'" id="'.$orden_name[2].'" placeholder="'.$orden_place[2].'" value="'.$orden[2].'" style="width: 200px; padding-left:10px;">
                         <input type="hidden" id="'.$orden_id[2].'" value="2">
                    </td>
                     <td>
                        <img src="'.$orden_image[2].'" style="width: 30px;">
                    </td>
                </tr>
                <tr id="'.$orden_tr[3].'">
                    <th scope="row">
                        &nbsp;
                        <img src="images/arriba.jpg" alt="subir fila" class="imgA"/>&nbsp;
                        <img src="images/abajo.jpg" alt="bajas fila" class="imgA" />
                    </th>
                    <td style="width: 230px; padding-right:0px;">
                        <input type="text" name="'.$orden_name[3].'" id="'.$orden_name[3].'" placeholder="'.$orden_place[3].'" value="'.$orden[3].'" style="width: 200px; padding-left:10px;">
                         <input type="hidden" id="'.$orden_id[3].'" value="3">
                    </td>
                     <td>
                        <img src="'.$orden_image[3].'" style="width: 60px; height:30px;">
                    </td>
                </tr>
                <tr id="'.$orden_tr[4].'">
                    <th scope="row">
                        &nbsp;
                        <img src="images/arriba.jpg" alt="subir fila" class="imgA"/>&nbsp;
                        <img src="images/abajo.jpg" alt="bajas fila" class="imgA"/>
                    </th>
                    <td style="width: 230px; padding-right:0px;">
                        <input type="text" name="'.$orden_name[4].'" id="'.$orden_name[4].'" placeholder="'.$orden_place[4].'" value="'.$orden[4].'" style="width: 200px; padding-left:10px;" >
                         <input type="hidden" id="'.$orden_id[4].'" value="4">
                    </td>
                     <td>
                        <img src="'.$orden_image[4].'" style="width: 40px;">
                    </td>
                </tr>
            </tbody>';
    }
}else{
    $completo = 
        '<tbody id="cuerpo">
                <tr id="10">
                    <th scope="row" style="width: 70px;" id="img1">
                        &nbsp;
                        <img src="images/arriba.jpg" alt="subir fila" class="imgA"/>&nbsp;
                        <img src="images/abajo.jpg" alt="bajas fila" class="imgA"/>
                    </th>
                    <td>
                        <input type="text" name="correo_paypal" id="correo_paypal" placeholder="Correo Paypal" style="width: 200px; padding-left:10px;">
                        <input type="hidden"  id="1" value="1">
                    </td>
                     <td>
                        <img src="images/paypal.png" style="width: 30px;">
                    </td>
                </tr>
                <tr id="20">
                    <th scope="row">
                        &nbsp;
                        <img src="images/arriba.jpg" alt="subir fila" class="imgA" />&nbsp;
                        <img src="images/abajo.jpg" alt="bajas fila" class="imgA"/>
                    </th>
                    <td>
                        <input type="text" name="correo_skrill" id="correo_skrill" placeholder="Correo Skrill" style="width: 200px; padding-left:10px;">
                         <input type="hidden" name="skrill" id="2" value="2">
                    </td>
                     <td>
                        <img src="images/skrill.png" style="width: 30px;">
                    </td>
                </tr>
                <tr id="30">
                    <th scope="row">
                        &nbsp;
                        <img src="images/arriba.jpg" alt="subir fila" class="imgA"/>&nbsp;
                        <img src="images/abajo.jpg" alt="bajas fila" class="imgA" />
                    </th>
                    <td>
                        <input type="text" name="correo_airtm" id="correo_airtm" placeholder="Correo AirTM" style="width: 200px; padding-left:10px;">
                         <input type="hidden" name="airtm" id="3" value="3">
                    </td>
                     <td>
                        <img src="images/transferwise.png" style="width: 30px;">
                    </td>
                </tr>
                <tr id="40">
                    <th scope="row">
                        &nbsp;
                        <img src="images/arriba.jpg" alt="subir fila" class="imgA"/>&nbsp;
                        <img src="images/abajo.jpg" alt="bajas fila" class="imgA"/>
                    </th>
                    <td>
                        <input type="text" name="correo_uphold" id="correo_uphold" placeholder="Correo Uphold" style="width: 200px; padding-left:10px;" >
                         <input type="hidden" name="uphold" id="4" value="4">
                    </td>
                     <td>
                        <img src="images/uphold.jpg" style="width: 30px;">
                    </td>
                </tr>
            </tbody>'; 
}

echo $completo;

?>
