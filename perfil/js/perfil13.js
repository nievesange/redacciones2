function loadOrden(){
    //loaderIn();
    var id = 1;
    var url="perfil/queries/perfil/load.php";
    $.post(url,{id:id},
    function (response){
        $("#ultimos_pagos").html(response);
    });

    var url2="perfil/queries/perfil/load_page.php";
    $.post(url2,{id:id},
    function (response){
        $("#tabla").html(response);
        iniciarTabla();
    });
    
}
loadOrden();
loadResult();

function loadResult() {
    var id = 1;
    var url2="perfil/queries/perfil/load_result.php";
    $.post(url2,{id:id},
    function (response0){
        json   = eval('('+response0+')');
        $('#nombre').val(json.nombre);
        $('#apellido').val(json.apellido);
        $('#correo').val(json.correo);
        $('#cod').val(json.cod);
        $('#tel').val(json.tel);
        $('#palabras').val(json.horas);
        $("#perfil").val(json.error);
        $(".preview img").attr(json.style);
        
    });
}
function actualizarPerfil(type) {

    var correo_paypal = $('#correo_paypal').val();
    var nivel_paypal = $('#1').val();
    var correo_skrill = $('#correo_skrill').val();
    var nivel_skrill = $('#2').val();
    var correo_airtm = $('#correo_airtm').val();
    var nivel_airtm = $('#3').val();
    var correo_uphold = $('#correo_uphold').val();
    var nivel_uphold = $('#4').val();
    var cod           = $('#cod').val();
    var tel           = $('#tel').val();
    var password      = $('#password').val();
    var password2     = $('#password2').val();
    var palabras      = $('#palabras').val();
    var perfil        = $('#perfil').val();
    var img           = $("#src_img").val();
    var style         = $(".preview img").attr('style');

    if (password == "" || password == null || password == undefined) {
        password = "0";
        password2 = "0";
    }

    if (password != password2) {
        toastr["error"]("", "LAS CONTRASEÑAS NO COINCIDEN");
        $("#password2").focus();
        return false;
    }else{
        $.post("perfil/queries/perfil/update.php",{style:style,password:password,correo_paypal:correo_paypal, nivel_paypal:nivel_paypal, correo_skrill:correo_skrill, nivel_skrill:nivel_skrill, correo_airtm:correo_airtm,  nivel_airtm: nivel_airtm, correo_uphold:correo_uphold, nivel_uphold:nivel_uphold, cod:cod,tel:tel,palabras:palabras,perfil:perfil,img:img},
            function(data){
                if(data == 1){
                    toastr["success"]("", "DATOS ACTUALIZADO CON EXITO");
                    if (type== 1) {
                        location.reload();
                    }else{
                        location.reload();
                    }                        
                    loadResult();
                    $(".img-perfil").attr("src", 'images/'+img);
                }else if(data == 3){
                    toastr["warning"]("", "EL CORREO YA SE ENCUENTRA REGISTRADO");
                }else{
                    toastr["error"]("", "ERROR AL ACTUALIZAR");
                }
            });
    }
}



//var style = $(img1).attr('style');

$(document).ready(function() {
    $("#image").change(function(e) {  
        var formData = new FormData();
        var files = $('#image')[0].files[0];
        formData.append('file',files);
        $.ajax({
            url: 'perfil/queries/perfil/upload.php',
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response == 1) {
                    alert('NO guardo.');
                } else if(response != 0){
                    var image         =  response.split('/');
                    var img           = image[1];
                    $("#src_img").val(img);
                    actualizarPerfil(1);
                }else {
                    alert('Formato de imagen incorrecto.');
                }
            }
        });
        return false;
    });

});



// Mover 

/* *** código para la página moverfilas.html *** */
var tab;
var filas;
var tr;
function iniciarTabla() {
  tab = document.getElementById('cuerpo');
  filas = tab.getElementsByTagName('tr');
  for (i=0; ele = filas[i]; i++) {
    ele.getElementsByTagName('img')[0].onclick = function() { mover(this,-1) }
    ele.getElementsByTagName('img')[1].onclick = function() { mover(this,1) }
  }
  mostrarOcultar();
}

// Ocultar imagen subir en primera fila y bajar en última fila. Mostrar el resto de imágenes
function mostrarOcultar() {
  for (i=0; ele = filas[i]; i++) {
    ele.getElementsByTagName('img')[0].style.visibility = (i==0) ? 'hidden' : 'visible';
    ele.getElementsByTagName('img')[1].style.visibility = (i==filas.length-1) ? 'hidden' : 'visible';
  }
}

function mover(obj,num) {
  fila = obj.parentNode.parentNode;
  var id = $(fila).attr("id");
  for (i=0; ele = tab.getElementsByTagName('tr')[i]; i++)
    if (ele == fila) {numFila=i;
        break
    }
   // asignar valor destino
    var fila = parseInt(numFila);
    var num2 =  parseInt(num)*-1;
    var uno = parseInt(fila) + parseInt(num);
    ele = tab.getElementsByTagName('tr')[uno];
    var id1 = $(ele).attr("id");
    id_x = parseInt(id1)/10;
    var valor1 = $("#"+id_x).val();
    var valor2 =  parseInt(valor1) + num2;
    $("#"+id_x).val(valor2);

    // Asignar valor origen
    id_ini = parseInt(id)/10;
    var valor = $("#"+id_ini).val();
    var newv  = parseInt(valor) + parseInt(num);
    $("#"+id_ini).val(newv);
    

   // alert("fila_ini: " + fila_ini + "fila_operacion: " + operacion + "id_ini: " + id_in + "id_fin: " + id_fin + "valor: " + valor )

  copia = filas[numFila].cloneNode(true);
  // Añadir evento onclick a las imágenes
  copia.getElementsByTagName('img')[0].onclick = function() { mover(this,-1) }
  copia.getElementsByTagName('img')[1].onclick = function() { mover(this,1) }

  tab.removeChild(filas[numFila]);
  numFila += num;
  if (numFila > filas.length-1) {
    tab.appendChild(copia);
  }else{
    tab.insertBefore(copia,filas[numFila]);
    mostrarOcultar();
  }
}
