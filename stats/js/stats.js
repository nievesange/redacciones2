function load_graf() {
	$( "#ingresos" ).html('');

    $.ajax({
        url:"stats/queries/stats/ingresos.php", // getchart.php
        dataType: 'JSON',
        type: 'POST',
        data: {get_values: true},
        success: function(response) {
           config = {
			      data: response,
			      xkey: 'fecha',
			      ykeys: ['resultado'],
			      labels: ['Resultado'],
			      fillOpacity: 0.6,
			      hideHover: 'auto',
			      behaveLikeLine: true,
			      resize: true,
			      pointFillColors:['#ffffff'],
			      pointStrokeColors: ['black'],
			      lineColors:['gray','red']
			};
			config.element = 'ingresos';
			Morris.Area(config);
        }
    });


    $.ajax({
        url:"stats/queries/stats/pagos.php", // getchart.php
        dataType: 'JSON',
        type: 'POST',
        data: {get_values: true},
        success: function(response) {
           config = {
			      data: response,
			      xkey: 'fecha',
			      ykeys: ['resultado'],
			      labels: ['Resultado'],
			      fillOpacity: 0.6,
			      hideHover: 'auto',
			      behaveLikeLine: true,
			      resize: true,
			      pointFillColors:['green'],
			      pointStrokeColors: ['green'],
			      lineColors:['green','red']
			};
			config.element = 'pagos';
			Morris.Area(config);
        }
    });

      $.ajax({
        url:"stats/queries/stats/pagos_list.php", // getchart.php
        dataType: 'JSON',
        type: 'POST',
        data: {get_values: true},
        success: function(response) {
           config = {
			      data: response,
			      xkey: 'fecha',
			      ykeys: ['resultado'],
			      labels: ['Resultado'],
			      fillOpacity: 0.6,
			      hideHover: 'auto',
			      behaveLikeLine: true,
			      resize: true,
			      pointFillColors:['green'],
			      pointStrokeColors: ['green'],
			      lineColors:['green','red']
			};
			config.element = 'pagos_list';
			Morris.Area(config);
        }
    });
				
}
load_graf();

loadResult();
function loadResult() {
    var id = 1;
    var url2="home/queries/home/load_result.php";
    $.post(url2,{id:id},
    function (response0){
        colors = ["red", "green","blue"];
        vale = ["ban", "check","edit"];
        vale2 = ["danger", "success","primary"];
        mensaje = ["MUY MAL", "MAL","MALO","NECESITAS MEJORAR","NECESITAS MEJORAR","NECESITAS MEJORAR","NECESITAS MEJORAR","MUY BIEN","MUY BIEN","EXCELENTE","MUCHA CALIDAD"];
        json   = eval('('+response0+')');
        var progress = json.nivel*10;
        $("#total_retiro").val(json.total);
        $("#acumulado").html(json.dinero+" $");
        $("#palabras_hoy").html(json.palabras);

        $("#nivel").html(json.nivel);
        $("#pedidos_hoy").html(json.num);
        $("#text").html(mensaje[json.nivel]);
        $("#progress").html(progress+"%");
        $("#comment").html(json.comment);
        $("#value").html("REF: "+json.ref+" VALORACIÓN: "+json.numero);
   
    });
}