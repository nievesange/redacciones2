<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
function url_completa($forwarded_host = false) {
    $ssl   = !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on';
    $proto = strtolower($_SERVER['SERVER_PROTOCOL']);
    $proto = substr($proto, 0, strpos($proto, '/')) . ($ssl ? 's' : '' );
    return $proto;
} 
$host = url_completa();
if ($host == "http" ) {
	//header('Location: https://www.redactaria.com/panel/');
}
session_start();
$sessions_id = $_SESSION['BACKEND_id'];
if($sessions_id == ''){

?>

<!DOCTYPE html>
<html lang="en">
	<head><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<title>Panel Redactaria</title>
		<meta name="description" content="Panel Redactaria" />
		<meta name="keywords" content="redactaria,panel redactaria, redactores redactaria" />
		<meta name="author" content="Redactaria 2019"/>
		
		<!-- Favicon -->
		<link rel="shortcut icon" href="favicon.ico">
		<link rel="icon" href="favicon.ico" type="image/x-icon">
		
		<!-- vector map CSS -->
		<link href="./main.8d288f825d8dffbbe55e.css" rel="stylesheet"></head>
		<!--<script src='https://www.google.com/recaptcha/api.js?hl=es' async defer></script>-->
	</head>
	<body>
		<div class="app-container app-theme-white body-tabs-shadow">
        <div class="app-container">
            <div class="h-100">
                <div class="h-100 no-gutters row">
                    <div class="d-none d-lg-block col-lg-4">
                        <div class="slider-light">
                            <div class="slick-slider">
                                <div>
                                    <div class="position-relative h-100 d-flex justify-content-center align-items-center bg-plum-plate" tabindex="-1">
                                        <div class="slide-img-bg" style="background-image: url('assets/images/originals/city.jpg');"></div>
                                        <div class="slider-content"><h3>Actualizaciones semanales</h3>
                                            <p>Somos el &uacutenico equipo de redacci&oacuten que actualiza su panel de forma semanal para brindar los mejores servicios.</p></div>
                                    </div>
                                </div>
                                <div>
                                    <div class="position-relative h-100 d-flex justify-content-center align-items-center bg-premium-dark" tabindex="-1">
                                        <div class="slide-img-bg" style="background-image: url('assets/images/originals/citynights.jpg');"></div>
                                        <div class="slider-content"><h3>Gesti&oacuten de pedidos</h3>
                                            <p>No hay otro panel igual. Con nosotros podr&aacutes gestionar todos tus pedidos, ganancias y mucho m&aacutes.</p></div>
                                    </div>
                                </div>
                                <div>
                                    <div class="position-relative h-100 d-flex justify-content-center align-items-center bg-sunny-morning" tabindex="-1">
                                        <div class="slide-img-bg" style="background-image: url('assets/images/originals/citydark.jpg');"></div>
                                        <div class="slider-content"><h3>Nuestra misi&oacuten</h3>
                                            <p>Nuestra misi&oacuten principal es fidelizar a los redactores y mantener un buen equipo de redacci&oacuten.</p></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="h-100 d-flex bg-white justify-content-center align-items-center col-md-12 col-lg-8">
                        <div class="mx-auto app-login-box col-sm-12 col-md-10 col-lg-9">
                            <center><img class="brand-img mr-10" src="assets/images/logo.png" alt="Redactaria"/></center>
                            <h4 class="mb-0">
                            <span class="d-block">Bienvenido,</span>
                            <span>Recuerda acceder a la plataforma diariamente.</span></h4>
	                       
                            <div class="divider row"></div>
                            <div>
                                <form class="">
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <div class="position-relative form-group"><label for="exampleEmail" class="">Email</label><input name="email" placeholder="Correo electronico" type="email" class="form-control" required="" id="user"></div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="position-relative form-group"><label for="examplePassword" class="">Contrase&ntilde;a</label><input name="password" placeholder="Ingresa tu contrase&ntilde;a" type="password" class="form-control" required="" id="contraseña">
                                        </div>
                                    </div>
                                    <div class="col-md-6"></div>
                                   <!-- <div class="g-recaptcha col-md-6" data-sitekey="6LdRQJEUAAAAAAT_gChgiBfZeZbtx4Hd3NV23l4j"></div><br><br><br>-->
                                    <div class="divider row"></div>
                                    <div class="col-md-8"></div>
                                    <div class="d-flex align-items-center col-md-4" style="margin-top:5%;">
                                        <div class="float-right">
                                            <a href="javascript:void(0)" onclick="login();" class="btn btn-primary btn-lg">ACCEDER AL PANEL</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>

		
		<!-- jQuery -->
		<div class="app-drawer-overlay d-none animated fadeIn"></div><script type="text/javascript" src="./assets/scripts/main.8d288f825d8dffbbe55e.js"></script></body>
		<script src="add/vendors/bower_components/jquery/dist/jquery.min.js"></script>
		
		<!-- Bootstrap Core JavaScript -->
		<script src="add/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="add/vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>
		
		<!-- Slimscroll JavaScript -->
		<script src="add/dist/js/jquery.slimscroll.js"></script>
		
		<!-- Init JavaScript -->
		<script src="add/dist/js/init.js"></script>
		<script src="js/index3.js"></script>
		<script src="add/vendors/toastr/toastr.min.js"></script>

	</body>
</html>

<?php 
}else{
    //header('Location: es');
    $sessions_id = $_SESSION['BACKEND_id'];
	$sessions_nombre = $_SESSION['BACKEND_nombre'];
	$sessions_apellido = $_SESSION['BACKEND_apellido'];
	$sessions_correo = $_SESSION['BACKEND_correo'];
	$sessions_idtipousuario = $_SESSION['BACKEND_idtipousuario'];

		
		$archivo = $_REQUEST["pkv"];
		$dir     = $_REQUEST["dir"];
		$name     = $_REQUEST["name"];
		include("include/funciones.php")
		?>

		<!DOCTYPE html>
		<html lang="es">
		
		<!doctype html>
<html lang="en">

<head> <?php include('include/new_header.php') ?></head>
<body>
	<script src="include/funciones20.js"></script>

            <?php include 'include/new_menu.php'; 
			if (!$name ||  $name == "" || $name == null || $name == False) {
				if ($sessions_idtipousuario < 3) {
					$folder = "statsadmin";	
				}elseif ($sessions_idtipousuario == 4) {
					$folder = "entregadas";	
				}else{
					$folder = "home";
				}
		        
			}else{
			    $folder = $name;
			}
			
			$url = $folder.".php";
			include($url);

            ?>   
            </div>
    </div>
</div>
<?php include 'include/extras.php'; ?>
<div class="app-drawer-overlay d-none animated fadeIn"></div><script type="text/javascript" src="./assets/scripts/main.8d288f825d8dffbbe55e.js"></script></body>
</html>

		<script>
			$(window).scroll(function(){
			    if ($(this).scrollTop() > 100) {
			        $('.scrollup').fadeIn();
			    } else {
			        $('.scrollup').fadeOut();
			   }
			});
			$('.scrollup').click(function(){
			    $("html, body").animate({ scrollTop: 0 }, 900);
			    return false;
			});
			// MENU VISTAS

			function messagesss() {
				var id = 1;
	            var url="home/queries/home/messages.php";
	            $.post(url,{id:id},
	            function (response){
	                $("#messages_new").html(response);
	            });
			}

			
			function tickesss() {
				var id = 1;
	            var url="home/queries/home/tickects.php";
	            $.post(url,{id:id},
	            function (response){
	                $("#tickes_new").html(response);
	            });
			}

			function init_notify() {
		       		var id = 1;
		            var url="home/queries/home/notify.php";
		            $.post(url,{id:id},
		            function (response){
		                json   = eval('('+response+')');
		                if (json.tick > 0) {
		                	$("#tickes").html('<span class="icon-wrapper icon-wrapper-alt rounded-circle"><span class="icon-wrapper-bg bg-success"></span>                                <i class="icon text-success icon-anim-pulse ion-android-hangout"><span style="text-decoration: none;font-size: 10px;top: 0px;position: absolute;">'+json.tick+'</span></i></span>');
		                } else {
		                	$("#tickes").html('<span class="icon-wrapper icon-wrapper-alt rounded-circle"><span class="icon-wrapper-bg bg-primary"></span>                                <i class="icon text-primary  ion-android-hangout"><span style="text-decoration: none;font-size: 10px;top: 0px;position: absolute;">'+json.tick+'</span></i></span>');
		                }
		                if (json.msg > 0) {
		                	$("#messagesss").html('<span class="icon-wrapper icon-wrapper-alt rounded-circle"><span class="icon-wrapper-bg bg-success"></span>                                <i class="icon text-success icon-anim-pulse ion-android-notifications"><span style="text-decoration: none;font-size: 10px;top: 0px;position: absolute;">'+json.msg+'</span></i></span>');
		                } else {
		                	$("#messagesss").html('<span class="icon-wrapper icon-wrapper-alt rounded-circle"><span class="icon-wrapper-bg bg-primary"></span>                                <i class="icon text-primary  ion-android-notifications"><span style="text-decoration: none;font-size: 10px;top: 0px;position: absolute;">'+json.msg+'</span></i></span>');
		                }
		            });  
				
			}
			setInterval(function(){
		    	init_notify();        
		    }, 3000);
			init_notify();
			function cargaStatus() {
				var id = 1;
	            var url="home/queries/home/status.php";
	            $.post(url,{id:id},
	            function (response){
	                json   = eval('('+response+')');
	                if (json.result == 0) {
	                	$("#status").html('<a href="javascript:void(0);" onclick="changeStatus(1)" class="btn-pill btn-shadow btn-warning btn btn-focus">INACTIVO</a>');
	                	$("#indicador_status").html('<span class="badge badge-dot badge-dot-sm badge-warning" style="position: relative;">NOTIFICACIONES</span>');
	                } else {
	                	$("#status").html('<a href="javascript:void(0);" onclick="changeStatus(0)" class="btn-pill btn-shadow btn-success btn btn-focus">ACTIVO</a>');
	                	$("#indicador_status").html('<span class="badge badge-dot badge-dot-sm badge-success" style="position: relative;">NOTIFICACIONES</span>');
	                }
	            });
			}
			cargaStatus();
			function changeStatus(status) {
				var url="home/queries/opciones/status.php";
	            $.post(url,{status:status},
	            function (response){
	               cargaStatus(); 
	            });
			}
			
		</script>

	</html><?php 
}

?>
