
<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="pe-7s-diamond icon-gradient bg-warm-flame">
                        </i>
                    </div>                   
                    <div>PUBLICAR
                        <div class="page-title-subheading">PUBLICA Y ASIGNA NUEVOS PEDIDOS
                        </div>
                    </div>
                </div>   
            </div>
        </div>
        <div class="tabs-animation">
            <div id="home" class="view_user">
                <div class="row view_home" id="asigned">
                    <div class="col-sm-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h4><span class="number"><i class=" txt-black"></i></span><span class="head-font " style="text-transform: none;">FORMULARIO DE PEDIDOS</span></h4>
                            </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper in">
                        <div class="panel-body">
                            <form id="example-advanced-form" action="#" style="margin-bottom: 2%;">
                                
                                <fieldset>
                                    <div class="row">
                                        <div class="col-sm-12"></div>
                                        <div class="col-sm-4">
                                            <div class="form-wrap">
                                                
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="fa fa-thumb-tack" style="padding-top: 5px;"></i></div>
                                                        <input type="text" class="form-control "  name="direccion" id="ref_pedido" placeholder="REF DEL PEDIDO" >
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                           <div class="form-wrap">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="fa fa-pencil-square-o" style="padding-top: 5px;"></i></div>
                                                        <input type="text" class="form-control "  name="palabras" id="palabras" placeholder="CANT. DE PALABRAS" >
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-wrap">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="fa fa-clock-o" style="padding-top: 5px;"></i></div>
                                                        <select id="tiempo_entrega" class="form-control " name=""  >
                                                           <option value="24" selected="selected">24 Horas</option> 
                                                           <option value="48">48 Horas</option>  
                                                           <option value="72">72 Horas</option>  
                                                        </select>
                                                    </div> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="width: 100%;">
                                            <div class="col-md-12">
                                                <div class="panel-wrapper  in">
                                                    <div class="panel-body" id="text_description">
                                                        <textarea class="tinymce" id="descripcion_general" style="display: none;"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12" id="comenzar">
                                            <div class="col-lg-5 col-md-6 col-xs-12"></div>
                                                <div class="col-lg-4 col-md-6 col-xs-12">
                                                    <select id="redactor" class="form-control ">
                                                        <option value="0">SELECCIONAR:</option>
                                                            <?php     
                                                            selectUsuario("(idtipousuario = 3) and activo = 1 and status = 1");
                                                            ?>
                                                    </select>
                                                </div>
                                                <div class="col-lg-3 col-md-6 col-xs-12">
                                                    <a href="javascript:void(0);" class="btn btn-success" id="comenzar_boton" style="font-size:14px;text-align:center;text-transform: none;margin-top: 1%;" onclick="comenzarPedidos()">ASIGNAR A ESTE REDACTOR</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                            </form>
                    </div>
                </div>
                </div>
               
            </div>
        </div>
<script type="text/javascript" src='publicar/js/publicar4.js'></script>  
<script src="add/vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>
<script src="add/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js">
</script>
<script src="add/vendors/toastr/toastr.min.js"></script>
<script src="add/vendors/bower_components/tinymce/tinymce.min.js"></script>
<script src="add/dist/js/tinymce-data.js"></script>
<style type="text/css">
    #mceu_16-body{
        display: none !important;
    }
</style>
<script>
    $("#descripcion_general").show('slow');
</script>
