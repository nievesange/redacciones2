carga_inicio();
function carga_inicio() {
    var url2="chatred/queries/chatRedactor/inicio.php";
    $.post(url2,function (response0){
        json   = eval('('+response0+')');  
        $("#usuario_sel").html(json.nombre+" "+json.apellido); 
        $("#id_Urep").html(json.id_r);
        $('#id_Urep').css('display','none');
        var img = json.img;
        if (img =="" || img == null) {
           img = "avatar.jpg";
        } else{
            img == json.img;
            
        }         
        $("#img").attr("src", "images/"+img);
        $("#img").attr("style", json.style);
        loadMensajes(json.id_r);      
        load_usuarios_conectados();    
    });
}

function loadMensajes(id){
    var url="chatred/queries/chatRedactor/result2.php";
    $.post(url,{id:id},function (response){
        $("#listarOrdenes").html(response);
        var div = document.getElementById('vista');
        div.scrollTop = '100000'; 
    });
}

function load_usuarios_conectados(){
    var url2="chatred/queries/chatRedactor/load_usuarios_conectados.php";
    $.post(url2,function (response){
        $("#usuarios_conectados").html(response);
    });

}

$(function() {
    $(document).keypress(function(e) {
        if(e.which == 13) {
            event.preventDefault();
            guardarmensaje();
        }
    });
})

setInterval(function(){actulizaMensaje();}, 1000);
var cargar_usuario = setInterval(function(){load_usuarios_conectados();}, 1000);

function actulizaMensaje() {
    var id =  $('#id_Urep').html();
    $('#id_Urep').css('display','none');
    var url="chatred/queries/chatRedactor/mensajes.php";
    $.post(url,{id:id},function (response){
       if (response != 0) {
            $("#listarOrdenes").html(response);
            var div = document.getElementById('vista');
            div.scrollTop = '100000';     
        }
    });
}

function guardarmensaje(id) {
    var id =  $('#id_Urep').html();
    var mensaje = $('#mensaje').val();
    mensaje = mensaje.replace('<', '');
    mensaje = mensaje.replace('>', '');
    if(mensaje=="" || id ==""){
        $("#mensaje").focus();
        return false;
    }else{
        $.post("chatred/queries/chatRedactor/chat_mensaje.php",{mensaje:mensaje, id:id},
            function(data){
                if(data == 1){                    
                    $('#mensaje').val("");
                    usuario_seleccionado(id);
                }else if (data == 4){
                    toastr["error"]("", "SELECCIONE UN USUARIO PARA ENVIAR EL MENSAJE");
                    $("#mensaje").focus();
                }else{
                     toastr["error"]("", "NO SE PUEDE ENVIAR EL MENSAJE");
                    $("#mensaje").focus();
                }
            }
        );
    }
}

function usuario_seleccionado(id) {
    var url2="chatred/queries/chatRedactor/result.php"; 
    $.post(url2,{id:id},function (response0){
        json   = eval('('+response0+')');
        $("#usuario_sel").html(json.nombre+" "+json.apellido);
        $("#id_Urep").html(json.id_r);
        var img = json.img;
        if (img =="" || img == null) {
           img = "avatar.jpg";
        } else{
            img == json.img;
        }
        $("#img").attr("src", "images/"+img);
        $("#img").attr("style", json.style);
        loadMensajes(json.id_r);
        if ($("#closed_movil").hasClass('is-active')){
            $("#closed_movil").trigger('click');
        }
        $("#mensaje").focus();
    });
}

$('#buscar').keyup(function(){
    var buscador_usuario = $('#buscar').val();
    loadUsuarioCustom(buscador_usuario);

});

function loadUsuarioCustom(valor){
    if (valor != "") {
        $("#limpiar").show('slow');
        clearInterval(cargar_usuario);    
        var url="chatred/queries/chatRedactor/load_custom.php";
        $.post(url,{valor:valor},
        function (response){
            $("#usuarios_conectados").html(response);
        });
    }else{
        $("#limpiar").hide('slow');
        load_usuarios_conectados();
        cargar_usuario = setInterval(function(){load_usuarios_conectados();}, 1000);
    }
}

function limpiar() {
    $('#buscar').val("");
    $("#limpiar").hide('slow');
    load_usuarios_conectados();
    cargar_usuario = setInterval(function(){load_usuarios_conectados();}, 1000);
}


var pantalla = { "height": '800px' };
var vista = { "height": '600px' };
var pant = { "height": '650px', "box-shadow" : 'none' };
var pantalla1 = { "height": '500px' };
var vista1 = { "height": '300px' };
var pant1 = { "height": 'auto', "box-shadow" : 'none' };


if (screen.height > 1000 ) {
    $("#pantalla").css(pantalla);
    $("#pantalla1").css(pantalla);
    $("#pantalla2").css(pantalla);
    $("#vista").css(vista);
    $("#listarOrdenes").css(vista);
    $("#pant").css(pant);

}
if (screen.height < 1000 ) {
    $("#pantalla").css(pantalla1);
    $("#pantalla1").css(pantalla1);
    $("#pantalla2").css(pantalla1);
    $("#vista").css(vista1);
    $("#listarOrdenes").css(vista1);
    $("#pant").css(pant1);

}