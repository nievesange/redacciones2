function loadOrden(){
    //loaderIn();
    var id = 1;
    var url="revisar/queries/revisar/load.php";
    $.post(url,{id:id},
    function (response){
        $("#listarOrdenes").html(response);
    });
    
}
loadOrden();

function aprobarModal(id) {
    $("#solo_rechazar").hide();
    $("#solo_aprobar").show();

    $("#bot_rechazar").hide();
    $("#bot_revisar").show();

    $("#articulo_id").val(id);
    $('#TooltipDemo').trigger('click');
    $("#TooltipDemo").show('slow');
}

$('#TooltipDemo').click(function(){
    atrasRevisar2();
});

function rechazado(id) {
    $("#solo_aprobar").hide();
    $("#solo_rechazar").show();

    $("#bot_revisar").hide();
    $("#bot_rechazar").show();

    $("#articulo_id").val(id);
    $('#TooltipDemo').trigger('click');
    $("#TooltipDemo").show('slow');
}
function atrasRevisar(){
    $('#TooltipDemo').trigger('click');
    $("#TooltipDemo").hide('slow');
    
    $(".toggle").removeClass('btn-success');
    $(".toggle").addClass('btn-light off');
    $('input[id="checkbox1"]').prop('checked', false);
    $('input[id="checkbox2"]').prop('checked', false);
    $('input[id="checkbox3"]').prop('checked', false);
    $('input[id="checkbox4"]').prop('checked', false);
    $('input[id="checkbox5"]').prop('checked', false);
    $('input[id="checkbox6"]').prop('checked', false);
    $('input[id="checkbox7"]').prop('checked', false);
    tinymce.get("descripcion").setContent('');
}

function atrasRevisar2(){
    $("#TooltipDemo").hide('slow');
    $(".toggle").removeClass('btn-success');
    $(".toggle").addClass('btn-light off');
    $('input[id="checkbox1"]').prop('checked', false);
    $('input[id="checkbox2"]').prop('checked', false);
    $('input[id="checkbox3"]').prop('checked', false);
    $('input[id="checkbox4"]').prop('checked', false);
    $('input[id="checkbox5"]').prop('checked', false);
    $('input[id="checkbox6"]').prop('checked', false);
    $('input[id="checkbox7"]').prop('checked', false);
    tinymce.get("descripcion").setContent('');
}

function cerrarRevisar() {
    var id = $("#articulo_id").val();
    var autor  = $("#autor_"+id).val();
    var numero = 0; 
    var opc1 = 0;
    var opc2 = 0;
    var opc3 = 0;
    var opc4 = 0;
    var opc5 = 0;
    var opc6 = 0;
    var opc7 = 0;
    if ($('input[id="checkbox1"]').prop('checked')){
        numero = numero + 3;
        opc1 = 1;    
    }
    
    if ($('input[id="checkbox2"]').prop('checked')){
        numero = numero + 2;
        opc2 = 1;
    }
    if ($('input[id="checkbox3"]').prop('checked')){
        numero = numero + 1;
        opc3 = 1;
    } 
   if ($('input[id="checkbox4"]').prop('checked')){
        numero = numero + 1;
        opc4 = 1;
    } 
    if ($('input[id="checkbox5"]').prop('checked')){
        numero = numero + 1;
        opc5 = 1;
    } 
    if ($('input[id="checkbox6"]').prop('checked')){
        numero = numero + 1;
        opc6 = 1;
    } 
    if ($('input[id="checkbox7"]').prop('checked')){
        numero = numero + 1;
        opc7 = 1;
    }
    var content = tinymce.get("descripcion").getContent();
        swal({
            title: 'ESTA SEGURO?',
            text: 'QUE DESEA APROBAR ESTE PEDIDO',
            type: 'success',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'SI, APROBAR!',
            cancelButtonText: 'NO',
            closeOnConfirm: true,
            closeOnCancel: true
        },function(isConfirm){
            if (isConfirm) {
               var url="revisar/queries/opciones/aprobar.php";
                $.post(url,{id:id,numero:numero,content:content,autor:autor,opc1:opc1,opc2:opc2,opc3:opc3,opc4:opc4,opc5:opc5,opc6:opc6,opc7:opc7},function(data){
                    if(data == 1){
                        toastr['success']('', 'APROBADO, Y SE HAN ACTUALIZADO LOS VALORES');
                        atrasRevisar();
                        loadOrden();
                            
                    }else if(data == 3){
                        toastr["warning"]("", "NO SE HA PODIDO REALIZAR LA APROBACIÓN");
                        atrasRevisar();
                        loadOrden();
                    }else{
                        toastr['error']('', 'ERROR AL APROBAR');
                        atrasRevisar();
                        loadOrden();
                    }
                });
                
                
            }
        });
}

function regCancelar(){
    var id = $("#articulo_id").val();
    var motivo = tinymce.get("descripcion").getContent();
    var url="revisar/queries/opciones/rechazar.php";
    $.post(url,{id:id,motivo:motivo},function(data){
        if(data == 1){
            toastr['success']('', 'APROBADO, Y SE HAN ACTUALIZADO LOS VALORES');
            atrasRevisar();
            loadOrden();
        }else if(data == 3){
            toastr["warning"]("", "NO SE HA PODIDO REALIZAR LA ENTREGA");
            atrasRevisar();
            loadOrden();
        }else{
            toastr['error']('', 'ERROR AL APROBAR');
            atrasRevisar();
            loadOrden();
        }
    });
}