<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
include('../../../include/coneccion.php');
include('../../../include/funciones.php');

$fechaToday = date('Y-m-d H:i:s');
$z = 1;
$id_logIn  = idUsuario();
$tipo_usuario = tipoUsuario();


$query="SELECT articulo_id, articulo_ref, articulo_redactor, articulo_descripcion, articulo_palabras, articulo_date, articulo_entrega, articulo_url FROM articulos Where  articulo_status = 1 order by articulo_entrega ASC ";

$res=$conn->query($query);
if($res->num_rows > 0){
    while($fila=$res->fetch_array()){
        $articulo_id          = $fila['articulo_id'];
        $articulo_ref         = $fila['articulo_ref'];
        $articulo_redactor    = usuarioId($fila['articulo_redactor']);
        $id_articulo_redactor = $fila['articulo_redactor'];
        $articulo_descripcion = $fila['articulo_descripcion'];
        $articulo_palabras    = $fila['articulo_palabras'];
        $articulo_entrega     = $fila['articulo_entrega'];
        $articulo_url         = $fila['articulo_url'];
        $date     = ConvFecha($fila['articulo_date']);
       
             $completo = '<div class="panel panel-'.$status_ordenes_color[10].' card-view panel-refresh" >
                            <div class="refresh-container2">
                                <div class="la-anim-1"></div>
                            </div>
                            <div class="panel-heading">
                                 <input type="hidden" value="'.$id_articulo_redactor.'" id="autor_'.$articulo_id.'">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-dark inline-block"> <span style="color:blue;"> REF: '.$articulo_ref.' - </span> '.$articulo_palabras.' PALABRAS <span style="color:blue"> </span><span style="color:blue;"> <i class="fa  fa-arrow-right"></i> '.$articulo_redactor.'</span></h6>
                                    <h6 class="panel-title txt-dark inline-block"><span style="color:red">FECHA '.$date.' </span></h6>
                                    
                                </div>

                                <div class="pull-right" >                                   
                                    <a class="pull-left inline-block mr-15"  data-toggle="collapse" href="#collapse_'.$articulo_id.'" aria-expanded="true" >
                                        <i class="zmdi zmdi-chevron-down" style="color:#333;"></i>
                                        <i class="zmdi zmdi-chevron-up" style="color:#333;"></i>
                                    </a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div  id="collapse_'.$articulo_id.'" class="panel-wrapper collapse">
                                <div  class="panel-body">
                                    <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-wrap">
                                                    <form class="form-horizontal" role="form">
                                                        <div class="form-body">
                                                            <h6 class="txt-dark capitalize-font"><i class="zmdi zmdi-account-box mr-10"></i>DETALLES DE LA REDACCIÓN</h6>
                                                            <hr class="light-grey-hr"/>
                                                            <div class="row" style="overflow-x:auto">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <div class="col-md-12">
                                                                            '.$articulo_descripcion.'<br>
                                                                            <p class="form-control-static"> <a href="'.$articulo_url.'" target="_blank" class="btn btn-primary pd-x-20">DESCARGAR</a></p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                    </form>
                                                    <br>
                                                    <div class="" role="document" id="rechazar_'.$articulo_id.'" style="display:none;">
                                                        <div>
                                                        <input type="hidden" id="articulo_id" value="'.$articulo_id.'">
                                                            <div class="col-md-12 col-sm-12">
                                                                <textarea class="tinymce" id="motivo_'.$articulo_id.'"></textarea><br>
                                                            </div>
                                                            <button onclick="preguntar('.$articulo_id.')"  class="btn btn-success pd-x-20">RECHAZAR</button><br> 
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="pull-right">
                                                            <button onclick="rechazado('.$articulo_id.')" class="btn btn-danger pd-x-20">RECHAZAR</button>
                                                                <button onclick="aprobarModal('.$articulo_id.')" class="btn btn-success pd-x-20" style="margin-bottom:0px !important;">APROBAR</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    <div class="pull-left inline-block"><br>    
                                        <ul class="nav nav-pills" style="font-size:12px;">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>';
      
                    $script = '';

            $productos[$articulo_id]= $completo.$script;

        $z++;
    
    }
}

if(empty($productos)){
    echo "<tr><td colspan=7 class='text-center text-muted'>NO SE ENCONTRARON RESULTADOS..</td></tr> ";
}else{

    foreach($productos as $producto)
    {
        echo $producto;
    }
}
?>
