<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
include('../../../include/coneccion.php');
include('../../../include/funciones.php');

$fechaToday = date('Y-m-d H:i:s');
$z = 1;
$id_logIn  = idUsuario();
$tipo_usuario = tipoUsuario();


$date = $_POST['date'];
$dat = $_POST['dat'];

if ($date == "") {
    $date = "2010-01-01 00:00:001";
}
if ($dat == "") {
    $dat = $fechaToday;
}

$status_art = array(0 => "focus", 1 => "primary", 2 => "success", 3 => "warning", 4 => "danger");
$colors =  array(0=>"red", 1=> "green", 2 =>"blue" );
$vale =  array(0=>"ban", 1=> "check", 2 =>"edit" );
$vale2 =  array(0=>"danger", 1=> "success", 2 =>"primary" );

if ($tipo_usuario < 3) {
$query="SELECT articulo_id, articulo_ref, articulo_redactor, articulo_descripcion, articulo_palabras, articulo_tiempo, articulo_date, articulo_entrega,articulo_status FROM articulos Where articulo_date >= '$date' and articulo_date <= '$dat'  order by articulo_date ASC ";
}else{
    $query="SELECT articulo_id, articulo_ref, articulo_redactor, articulo_descripcion, articulo_palabras, articulo_tiempo, articulo_date, articulo_entrega,articulo_status FROM articulos Where  articulo_date >= '$date' and articulo_date <= '$dat' and articulo_redactor = '$id_logIn' and articulo_status != 4   order by articulo_date ASC ";
}

$res=$conn->query($query);
if($res->num_rows > 0){
    while($fila=$res->fetch_array()){
        $articulo_id       = $fila['articulo_id'];
        $articulo_ref      = $fila['articulo_ref'];
        $articulo_redactor = "- ".usuarioId($fila['articulo_redactor']);
        $articulo_tiempo   = $fila['articulo_tiempo'];
        $articulo_palabras = $fila['articulo_palabras'];
        $articulo_status = $fila['articulo_status'];
        $articulo_descripcion = $fila['articulo_descripcion'];
        $articulo_date     = diferenciaHoras($fila['articulo_date'],$fechaToday);
        $date     = ConvFecha($fila['articulo_date'],$fechaToday);
        $restante = $articulo_tiempo - $articulo_date;
        
        if ($fila['articulo_redactor'] == $id_logIn) {
            $articulo_redactor = "";
        }

        if ($articulo_status == 4) {
            $articulo_redactor = "NO ASIGNADA";
        }
       
             $completo = '<div class="panel panel-'.$status_art[$articulo_status].' card-view panel-refresh" >
                            <div class="refresh-container2">
                                <div class="la-anim-1"></div>
                            </div>
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-dark inline-block"><span style="color:blue">REF: '.$articulo_ref.' - </span> '.$articulo_palabras.' PALABRAS <span style="color:blue;">'.$articulo_redactor.'</span></h6>
                                </div>
                                <div class="pull-right" >
                                    <h6 class="panel-title txt-dark inline-block"><span style="color:white">FECHA '.$date.' </span></h6>
                                    <a class="pull-left inline-block mr-15"  data-toggle="collapse" href="#collapse_'.$articulo_id.'" aria-expanded="true" >
                                        <i class="zmdi zmdi-chevron-down" style="color:white;"></i>
                                        <i class="zmdi zmdi-chevron-up" style="color:white;"></i>
                                    </a>
                                   
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div  id="collapse_'.$articulo_id.'" class="panel-wrapper collapse">
                                <div  class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-wrap">
                                                <form class="form-horizontal" role="form">
                                                    <div class="form-body">
                                                        <h6 class="txt-dark capitalize-font"><i class="zmdi zmdi-format-subject mr-10"></i>DETALLES DE LA REDACCIÓN
                                                        </h6>
                                                        <hr class="light-grey-hr"/>
                                                        <div class="row" style="overflow-x:auto;">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <div class="col-md-12">
                                                                        '.$articulo_descripcion.'<br>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div><br><br>
                                                        ';
                                                        switch ($articulo_status) {
                                                            case '1':// entregada
                                                                break;
                                                            case '2':
                                                                $query2 = "SELECT numero,comment,opc1,opc2,opc3,opc4,opc5,opc6,opc7 from valoraciones_redactor where redactor_id = '$id_logIn' and articulo_id = '$articulo_id' order by id_valoraciones DESC limit 1";

                                                            $res2 = $conn->query($query2);
                                                            if($res2->num_rows > 0){
                                                                while($fila2=$res2->fetch_array()){
                                                                   
                                                                    $lunes = $fila2['opc1'];
                                                                    $martes = $fila2['opc2'];
                                                                    $miercoles = $fila2['opc3'];
                                                                    $jueves = $fila2['opc4'];
                                                                    $viernes = $fila2['opc5'];
                                                                    $sabado = $fila2['opc6'];
                                                                    $domingo = $fila2['opc7'];
                                                                    $comment = $fila2['comment'];
                                                                    $numero = $fila2['numero'];
                                                                }
                                                            }else{
                                                                $lunes = 2;
                                                                $martes = 2;
                                                                $miercoles = 2;
                                                                $jueves = 2;
                                                                $viernes = 2;
                                                                $sabado = 2;
                                                                $domingo = 2;
                                                                $comment = "SIN VALORACIONES";
                                                                $numero = 7;
                                                            }
                                                            $completo .= '<div class="col-sm-12 col-lg-12">
    <div class="card-hover-shadow-2x mb-3 card" style="height: 550px;">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                <i class="header-icon pe-7s-check icon-gradient bg-amy-crisp"> </i><span style="margin-left:2%;font-weight: bold;font-size: 1rem;color:green">  VALORACIÓN: '.$numero.'</span>
            </div>            
        </div>
        <div class="scroll-area-lg" style="height: 500px">
            <div class="scrollbar-container">
                <div class="p-4">
                    <div class="vertical-time-simple vertical-without-time vertical-timeline vertical-timeline--animate vertical-timeline--one-column">
                    <div class="vertical-timeline-item dot-danger vertical-timeline-element">
                        <div>
                            <span class="vertical-timeline-element-icon bounce-in" style="margin-top: 1%;"></span>
                            <div class="vertical-timeline-element-content bounce-in">
                                <h4 class="timeline-title"><span class="pull-left inline-block capitalize-font" style="margin-top:1%;color:'.$colors[$lunes].'">ORTOGRAFÍA/GRAMÁTICA</span> <button class="border-0 btn-transition btn btn-outline-'.$vale2[$lunes].'"> <i class="fa fa-'.$vale[$lunes].'"></i></button></h4>
                            </div>
                        </div>
                    </div>
                    <div class="vertical-timeline-item dot-warning vertical-timeline-element">
                        <div>
                            <span class="vertical-timeline-element-icon bounce-in" style="margin-top: 1%;"></span>
                            <div class="vertical-timeline-element-content bounce-in">
                                <h4 class="timeline-title" ><span class="pull-left inline-block capitalize-font" style="margin-top:1%;color:'.$colors[$martes].'">TIPO DE REDACCIÓN</span> <button class="border-0 btn-transition btn btn-outline-'.$vale2[$martes].'"> <i class="fa fa-'.$vale[$martes].'"></i></button></h4>
                            </div>
                        </div>
                    </div>
                    <div class="vertical-timeline-item dot-success vertical-timeline-element">
                        <div>
                            <span class="vertical-timeline-element-icon bounce-in" style="margin-top: 1%;"></span>
                            <div class="vertical-timeline-element-content bounce-in">
                                <h4 class="timeline-title" >
                                <span class="pull-left inline-block capitalize-font" style="margin-top:1%;color:'.$colors[$miercoles].'">TEXTOS CON SENTIDO</span> <button class="border-0 btn-transition btn btn-outline-'.$vale2[$miercoles].'"> <i class="fa fa-'.$vale[$miercoles].'"></i></button>
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="vertical-timeline-item dot-primary vertical-timeline-element">
                        <div>
                            <span class="vertical-timeline-element-icon bounce-in" style="margin-top: 1%;"></span>
                            <div class="vertical-timeline-element-content bounce-in">
                                <h4 class="timeline-title"><span class="pull-left inline-block capitalize-font" style="margin-top:1%;color:'.$colors[$jueves].'">REDACCIÓN OPTIMIZADA</span> <button class="border-0 btn-transition btn btn-outline-'.$vale2[$jueves].'"> <i class="fa fa-'.$vale[$jueves].'"></i></button></h4>
                            </div>
                        </div>
                    </div>
                    <div class="vertical-timeline-item dot-info vertical-timeline-element">
                        <div>
                            <span class="vertical-timeline-element-icon bounce-in" style="margin-top: 1%;"></span>
                            <div class="vertical-timeline-element-content bounce-in">
                                <h4 class="timeline-title"><span class="pull-left inline-block capitalize-font" style="margin-top:1%;color:'.$colors[$viernes].'">ADAPTADO A LAS ESPECIFICACIONES</span> <button class="border-0 btn-transition btn btn-outline-'.$vale2[$viernes].'"> <i class="fa fa-'.$vale[$viernes].'"></i></button></h4>
                            </div>
                        </div>
                    </div>
                    <div class="vertical-timeline-item dot-dark vertical-timeline-element">
                        <div>
                            <span class="vertical-timeline-element-icon bounce-in" style="margin-top: 1%;"></span>
                            <div class="vertical-timeline-element-content bounce-in">
                              <h4 class="timeline-title" >
                              <span class="pull-left inline-block capitalize-font" style="margin-top:1%;color:'.$colors[$sabado].'">SEO OPTIMIADO</span> <button class="border-0 btn-transition btn btn-outline-'.$vale2[$sabado].'"> <i class="fa fa-'.$vale[$sabado].'"></i></button></h4>
                            </div>
                        </div>
                    </div>
                    <div class="vertical-timeline-item dot-danger vertical-timeline-element">
                        <div>
                            <span class="vertical-timeline-element-icon bounce-in" style="margin-top: 1%;"></span>
                            <div class="vertical-timeline-element-content bounce-in">
                                <h4 class="timeline-title"><span class="pull-left inline-block capitalize-font" style="margin-top:1%;color:'.$colors[$domingo].'">VARIACIÓN KEYWORDS</span> <button class="border-0 btn-transition btn btn-outline-'.$vale2[$domingo].'"> <i class="fa fa-'.$vale[$domingo].'"></i></button></h4>
                            </div>
                        </div>
                    </div>
                    <div class="vertical-timeline-item dot-success vertical-timeline-element">
                        <div>
                            <span class="vertical-timeline-element-icon bounce-in" style="margin-top: 1%;"></span>
                            <div class="vertical-timeline-element-content bounce-in">
                                <h4 class="timeline-title">
                                    <div class="badge badge-danger ml-2" style="margin-top:1%;">COMENTARIO</div>
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="vertical-timeline-item dot-success vertical-timeline-element">
                        <div>
                            <span class="vertical-timeline-element-icon bounce-in" style="margin-top: 1%;"></span>
                            <div class="vertical-timeline-element-content bounce-in">
                                <h4 class="timeline-title">'.$comment.'
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
       
</div>
</div>';
                                                                
                                                                break;
                                                            case '3':
                                                                $query3="SELECT id_rechazo, contenido_rechazo FROM rechazos where id_articulo = '$articulo_id' and status_rechazo = 0";
                                                                    $res3=$conn->query($query3);
                                                                    if($res3->num_rows > 0){
                                                                        while($fila3=$res3->fetch_array()){
                                                                            $motivos = $fila3['contenido_rechazo'];
                                                                            $id_rechazo = $fila3['id_rechazo'];
                                                                        }
                                                                        $completo .= '<div class="form-body">
                                                                    <h6 class="txt-dark capitalize-font"><i class="zmdi zmdi-block mr-10"></i>MOTIVO DE LA DEVOLUCIÓN</h6>
                                                                    <hr class="light-grey-hr"/>
                                                                    <div class="row" style="overflow-x: auto;">
                                                                        <div class="col-md-12">
                                                                                
                                                                                    '.$motivos.'
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>';
                                                                    }
                                                                
                                                                break;
                                                            case '4':
                                                                $completo .= '';
                                                                break;
                                                            default:
                                                                $completo .= "";
                                                                break;
                                                        }                
                                                            $completo .='
                                                        <div class="col-lg-12 col-md-12 col-xs-12" id="">
                                                            <div class="col-lg-5 col-md-6 col-xs-12"></div>
                                                                <div class="col-lg-4 col-md-6 col-xs-12">
                                                                    
                                                                </div>
                                                                <div class="col-lg-3 col-md-6 col-xs-12">
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pull-left inline-block"><br>    
                                        <ul class="nav nav-pills" style="font-size:12px;">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>';
      
                    $script = '';

            $productos[$articulo_id]= $completo.$script;

        $z++;
    
    }
}

if(empty($productos)){
    echo "<tr><td colspan=7 class='text-center text-muted'>NO SE ENCONTRARON RESULTADOS..</td></tr> ";
}else{

    foreach($productos as $producto)
    {
        echo $producto;
    }
}
?>
