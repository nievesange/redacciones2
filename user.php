<div class="ui-theme-settings">
        <button type="button"  id="TooltipDemo" class="btn-open-options btn btn-warning" style="display: none;">
            <i class="fa  fa-times fa-w-16 fa-2x"></i>
        </button>
        <button type="button"  id="TooltipDemo1" class=" btn btn-success" style="bottom: 120px;">
            <i class="fa fa-floppy-o fa-w-16 fa-2x" ></i>
        </button>
        <div class="theme-settings__inner">
            <div class="scrollbar-container">
                <div class="theme-settings__options-wrapper">
                    <input type="hidden" name="redactor_id" id="redactor_id">
                    <div class="p-3" >
                        <ul class="list-group">
                            <li><br></li>
                            <li style="display: none;" class="panel_li" id="solo_add">
                                <h3>AGREGAR UN NUEVO USUARIO</h3>
                                <div class="modal-body">
                                <div class="form-group">
                                    <div class="col-lg-6 col-md-12 col-sm-12">
                                        <input type="text" id='crear_nombre' class="form-control" onblur="mayusculas(this)" placeholder="NOMBRE">
                                    </div>                    
                                    <div class="col-lg-6 col-md-12 col-sm-12">
                                        <input type="text" id='crear_apellido' onblur="mayusculas(this)" class="form-control" placeholder="APELLIDO">
                                    </div>
                                </div><!-- form-group --><br><br>
                                <div class="form-group">                    
                                   <div class="input-group">                  
                                        <div class="input-group-addon">
                                            <i class="fa fa-dollar" style="font-size: 20px;"></i>
                                        </div>
                                        <input type="text" id='pagos' class="form-control" placeholder="PAGO POR 100 PALABRAS EJ. 0.1 0.15 0.2 ">
                                    </div>
                                </div><!-- form-group -->

                                <div class="form-group">                    
                                    <input type="text" id='crear_correo' class="form-control" placeholder="CORREO ELECTRONICO EJ. ejemplo@ejemplo.com">
                                </div><!-- form-group -->
                                <div class="form-group">                    
                                    <input type="password" id='crear_password' class="form-control" placeholder="CONTRASEÑA">
                                </div><!-- form-group -->
                                <div class="form-group">                    
                                    <input type="password" id='crear_password2' class="form-control" placeholder="VALIDAR CONTRASEÑA">
                                </div><!-- form-group -->
                                
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="icon-check" style="font-size: 20px;"></i></div>
                                        <select id='crear_tipo_usuario' class="form-control " name="modalEditar_color">
                                            <?php selectTipoUsuario(); ?>
                                        </select>
                                    </div>  
                                </div>
                            </div><!-- modal-body --></li>
                            <li style="display: none;" class="panel_li" id="solo_act">                   
                                <h3>EDITAR USUARIO</h3>
                                <div class="modal-body">
                        <input type="hidden" id='usuario_id'>
                        <div class="form-group">
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <input type="text" id='modalEditar_nombre' onblur="mayusculas(this)" class="form-control" placeholder="NOMBRE">
                            </div>                    
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <input type="text" id='modalEditar_apellido' class="form-control" onblur="mayusculas(this)" placeholder="APELLIDO">    
                            </div>
                        </div><!-- form-group --><br><br>
                          <div class="form-group">  
                                <div class="input-group">                  
                                    <div class="input-group-addon">
                                        <i class="fa fa-dollar" style="font-size: 20px;"></i>
                                    </div>
                                    <input type="text" id='pagosEdit2' class="form-control" placeholder="PAGO POR 100 PALABRAS EJ. 0.1 0.15 0.2 ">
                                </div>
                            </div><!-- form-group -->


                        <div class="form-group">                    
                            <input type="text" id='modalEditar_correo' class="form-control" readonly="readonly">
                        </div><!-- form-group -->
                        <div class="form-group">                    
                            <input type="password" id='editar_password' class="form-control" placeholder="CONTRASEÑA NUEVA">
                        </div><!-- form-group -->
                        <div class="form-group">                    
                            <input type="password" id='editar_password2' style="display: none;" class="form-control" placeholder="VALIDAR CONTRASEÑA NUEVA">
                        </div><!-- form-group -->
                        <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><i class="icon-check" style="font-size: 20px;"></i></div>
                            <select id='modalEditar_tipo_usuario' class="form-control " name="modalEditar_color">
                                <?php selectTipoUsuario(); ?>
                            </select>
                        </div>  
                    </div>
                    </div><!-- modal-body --></li>
                            
                            <li>
                                <button type="button" onclick="atrasRedactores()"   class="btn btn-warning pd-x-20" >ATRAS</button>
                                <button onclick="actualizarUsuario()" id="bot_act" class="btn bot btn-success pd-x-20">ACTUALIZAR</button>
                                <button onclick="addUsuario()" id="bot_add" class="btn bot btn-success pd-x-20" style="display: none;">GUARDAR</button>
                                
                            </li>
                            <li><br><br></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
</div>

<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading ">
                    <div class="page-title-icon">
                        <i class="pe-7s-diamond icon-gradient bg-warm-flame">
                        </i>
                    </div>
                    <div class="">PANEL DE USUARIOS
                        <div class="page-title-subheading">ADMINISTRAR LOS USUARIOS EN EL SISTEMA
                        </div>
                    </div>
                </div>   
            </div>
        </div>
         <form id="search_form" role="search"  style="width: 60%;display: inline-block;" class="top-nav-search pull-left">
            <div class="input-group">
                <input type="text" name="buscador_usuario" id="buscador_usuario" class="form-control" placeholder="BUSQUEDA POR NOMBRE O CORREO">
            </div>
        </form>
        <div class="card-body">
            <p class="card-text text-right">
                <button class='btn btn-primary' onclick="add();">AGREGAR USUARIO</button>
            </p>
        </div>
        
        <div class="tabs-animation col-md-12">
            <div id="home" class="view_user">
                <div class="row view_home" id="asigned">
                    <div class="col-md-12">
                        <div class="panel panel-default card-view">
                            <div class="panel-heading">
                                <div class="card">
                                    
                                </div><!-- card -->
                        
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-wrapper in">
                                <div class="panel-body">
                                    <div class="table-wrap" id="mostrar">
                                        <div class="table-responsive" id="table">
                                            <table id="Table1" class="table table-hover display  pb-30" >
                                                <thead>
                                                    <tr>
                                                        <th style="text-align: center;">Usurio</th>
                                                        <th style="text-align: center;">Correo</th>
                                                        <th style="text-align: center;">Tipo de Usuario</th>
                                                        <th style="text-align: center;">Pago</th>
                                                        <th style="text-align: center;">Opciones</th>
                                                    </tr>
                                                </thead>
                                                <tfoot>
                                                    <tr>
                                                        <th style="text-align: center;">Usuario</th>
                                                        <th style="text-align: center;">Correo</th>
                                                        <th style="text-align: center;">Tipo de Usuario</th>
                                                        <th style="text-align: center;">Pago</th>
                                                        <th style="text-align: center;">Opciones</th>
                                                    </tr>
                                                </tfoot>
                                                <tbody  id="usuarios">
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>           
                    </div>
                </div>
                
                  
            </div>
        </div>
<script type="text/javascript" src='usuarios/js/usuarios14.js'></script>
<script src="add/vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>
<script src="add/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js">
</script>
<script src="add/vendors/toastr/toastr.min.js"></script>
<script src="add/vendors/bower_components/tinymce/tinymce.min.js"></script>
<script src="add/dist/js/tinymce-data.js"></script>
<style type="text/css">
    #mceu_16-body{
        display: none !important;
    }
</style>
