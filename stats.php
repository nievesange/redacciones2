<div class="app-main__outer" style="width: 100%;">
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="pe-7s-diamond icon-gradient bg-warm-flame">
                        </i>
                    </div>
                    <div>ESTADÍSTICAS
                        <div class="page-title-subheading">SECCIÓN PARA VER ESTADÍSTICAS Y RENDIMIENTO
                        </div>
                    </div>
                </div>   
            </div>
        </div>
        <div class="tabs-animation col-md-12">
            <div id="home" class="view_user">
                <div class="mb-3 card">
                    <div class="card-header-tab card-header">
                        <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                            <i class="header-icon lnr-charts icon-gradient bg-happy-green"> </i>
                            RESUMEN DE ACTIVIDAD
                        </div>
                    </div>
                    <div class="no-gutters row">
                        <div class="col-sm-6 col-md-4 col-xl-4">
                            <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
                                <div class="icon-wrapper rounded-circle">
                                    <div class="icon-wrapper-bg opacity-10 bg-success"></div>
                                    <i class="lnr-apartment text-dark opacity-8"></i>
                                </div>
                                <div class="widget-chart-content">
                                    <div class="widget-subheading">CAPITAL ACUMULADO</div>
                                    <div class="widget-numbers text-success"><span id="acumulado"></span></div>
                                        
                                </div>
                            </div>
                            <div class="divider m-0 d-md-none d-sm-block"></div>
                        </div>
                        <div class="col-sm-6 col-md-4 col-xl-4">
                            <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
                                <div class="icon-wrapper rounded-circle">
                                    <div class="icon-wrapper-bg opacity-9 bg-danger"></div>
                                    <i class="lnr-keyboard text-white"></i>
                                </div>
                                <div class="widget-chart-content">
                                    <div class="widget-subheading">PALABRAS POR ENTREGAR</div>
                                    <div class="widget-numbers"><span id="palabras_hoy"></span></div>
                                    <div class="widget-description opacity-8 text-focus">
                                        PEDIDOS:
                                        <span class="text-info pl-1">
                                            <span class="pl-1" id="pedidos_hoy"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="divider m-0 d-md-none d-sm-block"></div>
                        </div>
                        <div class="col-sm-12 col-md-4 col-xl-4">
                            <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
                                <div class="icon-wrapper rounded-circle">
                                    <div class="icon-wrapper-bg opacity-9 bg-success"></div>
                                    <i class="lnr-spell-check text-white"></i></div>
                                    <div class="widget-chart-content">
                                        <div class="widget-subheading">MI NIVEL DE REDACCIÓN</div>
                                        <div class="widget-numbers" id="nivel"></div>
                                        <div class="widget-description opacity-8 text-focus">
                                            <div class="d-inline text-danger pr-1">
                                                <span class="pl-1" id="progress"></span>
                                            </div>
                                            <span id="text"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-center d-block p-3 card-footer">
                            <a href="?name=perfil" class="btn-pill btn-shadow btn-wide fsize-1 btn btn-primary btn-lg">
                                <span class="mr-2 opacity-7">
                                    <i class="icon icon-anim-pulse ion-ios-analytics-outline"></i>
                                </span>
                                <span class="mr-1">VER PERFIL</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-12">
                    <div class="mb-3 card">
                        <div class="card-header-tab card-header">
                            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">PANEL DE GRÁFICAS
                            </div>

                        </div>
                        <div class="p-0 card-body">
                            <div class="p-1 slick-slider-sm mx-auto">
                                <div class="slick-slider">
                                    <div>
                                        <div class="widget-chart widget-chart2 text-left p-0">
                                            <div class="widget-chat-wrapper-outer">
                                                <div class="widget-chart-content widget-chart-content-lg pb-0">
                                                    <div class="widget-chart-flex">
                                                        <div class="widget-title opacity-5 text-muted text-uppercase">CANTIDAD DE PALABRAS ESTE MES</div>
                                                    </div>
                                                   <!-- <div class="widget-numbers">
                                                        <div class="widget-chart-flex">
                                                            <div>
                                                                <small class="opacity-3 pr-1"></small>
                                                                <span>629</span>
                                                                <span class="text-primary pl-3">
                                                                    <i class="fa fa-angle-down"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>-->
                                                </div>
                                                <div class="widget-chart-wrapper he-auto opacity-10 m-0">
                                                    <div id="ingresos" style="width: 410;height: 152;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="widget-chart widget-chart2 text-left p-0">
                                            <div class="widget-chat-wrapper-outer">
                                                <div class="widget-chart-content widget-chart-content-lg pb-0">
                                                    <div class="widget-chart-flex">
                                                        <div class="widget-title opacity-5 text-muted text-uppercase">INGRESOS DIARIOS DE ESTE MES</div>
                                                    </div>
                                                    <div class="widget-numbers">
                                                        <div class="widget-chart-flex">
                                                            <div class="widget-title ml-2 font-size-lg font-weight-normal text-muted">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                                <div class="widget-chart-wrapper he-auto opacity-10 m-0">
                                                    <div id="pagos" style="width: 410;height: 152;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div>
                                        <div class="widget-chart widget-chart2 text-left p-0">
                                            <div class="widget-chat-wrapper-outer">
                                                <div class="widget-chart-content widget-chart-content-lg pb-0">
                                                    <div class="widget-chart-flex">
                                                        <div class="widget-title opacity-5 text-muted text-uppercase">MIS PAGOS EN ESTE AÑO</div>
                                                    </div>
                                                    <div class="widget-numbers">
                                                        <div class="widget-chart-flex">
                                                            
                                                            
                                                        </div>
                                                </div>
                                            </div>
                                                <div class="widget-chart-wrapper he-auto opacity-10 m-0">
                                                    <div id="pagos_list" style="width: 410;height: 152;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
              
<script type="text/javascript" src='stats/js/stats.js'></script>
<script src="add/vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>
<script src="add/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js">
</script>
<script src="add/vendors/bower_components/morris.js/morris.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="add/vendors/toastr/toastr.min.js"></script>
