<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
include('../../../include/coneccion.php');
include('../../../include/funciones.php');

$fechaToday = date('Y-m-d H:i:s');
$z = 1;
$id_logIn  = idUsuario();
$tipo_usuario = tipoUsuario();


$query="SELECT articulo_id, articulo_ref, articulo_descripcion, articulo_redactor, articulo_palabras, articulo_tiempo, articulo_date FROM articulos Where articulo_status = 4 order by articulo_date DESC ";

$res=$conn->query($query);
if($res->num_rows > 0){
    while($fila=$res->fetch_array()){
        $articulo_id       = $fila['articulo_id'];
        $articulo_ref      = $fila['articulo_ref'];
        $articulo_tiempo   = $fila['articulo_tiempo'];
        $articulo_palabras = $fila['articulo_palabras'];
        $articulo_descripcion = $fila['articulo_descripcion'];
        $articulo_date     = diferenciaHoras($fila['articulo_date'],$fechaToday);
        $date     = ConvFecha($fila['articulo_date']);
        $redactor = $fila['articulo_redactor'];
        if ($redactor != 0) {
        	$articulo_redactor = usuarioId($fila['articulo_redactor']);
        	$articulo_redactor = "|| RECHAZADO POR: ".$articulo_redactor;
        }else{
        	$articulo_redactor = "";
        }
        $restante = $articulo_tiempo - $articulo_date;
        if ($restante < 1) {
            $restante = "TIEMPO AGOTADO";
        }else{
            $restante = " RESTA ".$restante." HORAS";
        }
        $select =  selectUsuario2("(idtipousuario = 3) and activo = 1 and status = 1");
       
            $completo = '<div class="panel panel-'.$status_ordenes_color[1].' card-view panel-refresh" >
                            <div class="refresh-container2">
                                <div class="la-anim-1"></div>
                            </div>
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-dark inline-block"><span style="color:blue">REF: '.$articulo_ref.' - </span> '.$articulo_palabras.' PALABRAS <span style="color:#fff"> || </span><span style="color:blue;">'.$articulo_redactor.'</span></h6>
                                    <h6 class="panel-title txt-dark inline-block"><span style="color:red">FECHA '.$date.'</span></h6>
                                </div>
                                <div class="pull-right" >
                                    <a class="pull-left inline-block mr-15"  data-toggle="collapse" href="#collapse_'.$articulo_id.'" aria-expanded="true" >
                                        <i class="zmdi zmdi-chevron-down" style="color:#333;"></i>
                                        <i class="zmdi zmdi-chevron-up" style="color:#333;"></i>
                                    </a>
                                   
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div  id="collapse_'.$articulo_id.'" class="panel-wrapper collapse">
                                <div  class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-wrap">
                                                <form class="form-horizontal" role="form">
                                                    <div class="form-body">
                                                        <h6 class="txt-dark capitalize-font"><i class="zmdi zmdi-account-box mr-10"></i>DETALLES DE LA REDACCIÓN
                                                        </h6>
                                                        <hr class="light-grey-hr"/>
                                                        <div class="row" style="overflow-x:auto;">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <div class="col-md-12">
                                                                        '.$articulo_descripcion.'<br>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div><br><br>
                                                        <div class="col-lg-12 col-md-12 col-xs-12" id="">
                                                            <div class="col-lg-5 col-md-6 col-xs-12"></div>
                                                                <div class="col-lg-4 col-md-6 col-xs-12">
                                                                    <select id="redactor_'.$articulo_id.'" class="form-control ">
                                                                        <option value="0">SELECCIONAR:</option>';    
                                                            $completo .= $select;               
                                                            $completo .='
                                                                    </select>
                                                                </div>
                                                                <div class="col-lg-3 col-md-6 col-xs-12">
                                                                    <a href="javascript:void(0);" class="btn btn-success" style="font-size:14px;text-align:center;text-transform: none;margin-top: 1%;" onclick="comenzarPedidos('.$articulo_id.')">REASIGNAR A ESTE REDACTOR</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pull-left inline-block"><br>    
                                        <ul class="nav nav-pills" style="font-size:12px;">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>';
      
                    $script = "";

            $productos[$articulo_id]= $completo.$script;

        $z++;
    
    }
}

if(empty($productos)){
    echo "<tr><td colspan=7 class='text-center text-muted'>NO SE ENCONTRARON RESULTADOS..</td></tr> ";
}else{

    foreach($productos as $producto)
    {
        echo $producto;
    }
}
?>
