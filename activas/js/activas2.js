function loadOrden(){
    //loaderIn();
    var id = 1;
    var url="activas/queries/activas/load.php";
    $.post(url,{id:id},
    function (response){
        $("#listarOrdenes").html(response);
    });
}
loadOrden();

function comenzarPedidos(id) {
    var redactor = $("#redactor_"+id).val();
    console.log(id+" - "+redactor)
    if (id == ""){
        toastr["error"]("", "DEBE SELECCIONAR ALGÚN ARTÍCULO");
        $("#descripcion_general").focus();
        return false;
    }else if(redactor == "0"){
        toastr["error"]("", "DEBES ASIGNAR A UN REDACTOR");
        $("#redactor").focus();
        return false;
    }else{
        swal({
            title: 'ESTA SEGURO?',
            text: 'QUE DESEA ASIGNAR A ESTE REDACTOR',
            type: 'success',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'SI, ASIGNAR!',
            cancelButtonText: 'NO',
            closeOnConfirm: true,
            closeOnCancel: true
        },function(isConfirm){
            if (isConfirm) {
                $.post("activas/queries/activas/asignar.php",{id:id,redactor:redactor},
                function(data){
                    if(data == 1){
                        toastr["success"]("", "SE HA ASIGNADO CON EXITO Y APARECERÁ EN EL PANEL DEL REDACTOR SELECCIONADO");
                        setTimeout(function(){
                            loadOrden();
                        },300)
                    }else if(data == 3){
                        toastr["warning"]("", "EL TITULO YA SE ENCUNTRA PUBLICADO");
                    }else{
                        toastr["error"]("", "ERROR AL ASIGNAR");
                    }
                });
            }
        });
    }
}