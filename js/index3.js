function login(){
    var username = $('#user').val();
    var password = $('#contraseña').val();
    var captcha = $('#g-recaptcha-response').val();
    if(username == '' || password == ''){
        toastr["warning"]("", "DEBES LLENAR TODOS LOS CAMPOS");
    }else{
        if (captcha === "") {
          toastr["warning"]("", "DEBES MARCAR LA VERIFICACIÓN");  
        } else {
            $.post("login/login.php",
                {username:username,password:password,captcha:captcha},
                function(data){
                    console.log(data);
                    if(data == 1){
                        toastr["success"]("", "CORRECTO! ESTAS INGRESANDO");
                        location.reload();
                    }else if (data == 3){
                        toastr["warning"]("", "HAS ESTADO INACTIVO, COMUNICATE CON NOSOTROS A TRAVÉS DE TELEGRAM");
                    }else if (data == 0){
                        toastr["warning"]("", "NO SE HA VALIDADO LA VERIFICACIÓN");
                    }else{
                        toastr["error"]("", "CREDENCIALES INVALIDAS");
                    }
            });
        }
    }
}
$(function() {
    $(document).keypress(function(e) {
        if(e.which == 13) {
            event.preventDefault();
            login();
        }
    });
});
